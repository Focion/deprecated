package cn.focion.tv.tool.util;

import java.util.List;

/**
 * 操作List的工具 Created by Pirate on 2015/4/21.
 */
public class Lists {
    
    /**
     * 判断List是否无值
     * 
     * @param list
     *            要判断的List
     * @return true 有值 false 无值
     */
    public static boolean isEmpty(List list) {
        return list == null || list.size() == 0;
    }
    
}
