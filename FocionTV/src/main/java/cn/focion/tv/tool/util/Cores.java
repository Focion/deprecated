package cn.focion.tv.tool.util;

import android.os.Environment;

import java.io.File;
import java.io.FileFilter;
import java.util.regex.Pattern;

import cn.focion.tv.model.Season;

/**
 * 系统方法
 * <p/>
 * Created by Pirate on 2015/3/26.
 */
public class Cores {
    
    /**
     * 描述：SD卡是否能用
     *
     * @return true 可用 false 不可用
     */
    public static boolean isCanUseSD() {
        try {
            return Environment.getExternalStorageState()
                              .equals(Environment.MEDIA_MOUNTED);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    
    /**
     * 获取CPU核心数
     *
     * @return CPU核心数
     */
    public static int getNumCores() {
        try {
            // 获取本地CPU信息文件
            File dir = new File("/sys/devices/system/cpu/");
            // 过滤内部文件集
            File[] files = dir.listFiles(new FileFilter() {
                
                @Override
                public boolean accept(File pathname) {
                    // 检查文件名匹配cpu0~cpu9
                    return Pattern.matches("cpu[0-9]", pathname.getName());
                }
                
            });
            // 返回cpu核心数
            return files.length;
        }
        catch (Exception e) {
            // 异常返回1个核心
            return 1;
        }
    }
    
    /**
     * 检查特别季是否有集的方法
     * 
     * @param season
     *            季
     * @return true 无集 false 有集
     */
    public static boolean checkSpecial(Season season) {
        return season.getNumber() == 0 && season.getEpisode_count() == 0;
    }
    
}
