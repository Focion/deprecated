package cn.focion.tv.tool.pool;

/**
 * 任务回调监听器 Created by Pirate on 14-5-29.
 */
public class TaskListener {
    
    /**
     * 任务开始调用
     */
    public void get() {
    }
    
    /**
     * 任务结束调用
     */
    public void update() {
        
    }
    
}
