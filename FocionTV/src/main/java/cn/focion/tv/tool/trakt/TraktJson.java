package cn.focion.tv.tool.trakt;

import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;
import java.util.HashMap;

import cn.focion.tv.model.Episode;
import cn.focion.tv.model.Season;
import cn.focion.tv.model.Show;
import cn.focion.tv.temp.TShow;
import cn.focion.tv.tool.pool.TaskItem;
import cn.focion.tv.tool.pool.TaskListener;
import cn.focion.tv.tool.pool.TaskPool;
import cn.focion.tv.tool.util.Dates;
import cn.focion.tv.tool.util.Strings;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * Trakt返回Json数据的解析 Created by Administrator on 2015/4/18.
 */
public class TraktJson {
    
    // 解析Popular数据JSON完成广播
    public static final String ACTION_FOLLOW_POPULAR_SHOWS = "BOARD_FOLLOW_SHOWS";
    
    // 解析Trend数据JSON完成广播
    public static final String ACTION_FOLLOW_TREND_SHOWS = "BOARD_TREND_SHOWS";
    
    // 解析详情数据的JSON完成广播
    public static final String ACTION_DETAIL_SHOW = "BOARD_DETAIL_SHOW";
    
    // 解析详情数据翻译的JSON完成广播
    public static final String ACTION_DETAIL_SHOW_TRANS = "BOARD_DETAIL_SHOW_TRANS";
    
    // 解析搜索数据
    public static final String ACTION_SEARCH_SHOWS = "BOARD_SEARCH_SHOWS";
    
    // 线程池
    private TaskPool pool;
    
    // 上下文
    private Context mContext;
    
    private static TraktJson mTraktJson;
    
    private TraktJson(Context context) {
        pool = TaskPool.getInstance();
        mContext = context;
    }
    
    /**
     * 单例模式的json解析类
     * 
     * @return Json解析类
     */
    public static TraktJson getInstance(Context context) {
        if (mTraktJson == null)
            mTraktJson = new TraktJson(context);
        return mTraktJson;
    }
    
    /**
     * 获取Json封装模板Shows （Popluar）
     *
     * @param json
     *            json数据
     */
    public void jsonTShows(final String json) {
        final ArrayList<TShow> tShows = new ArrayList<>();
        // 解析线程
        TaskItem item = new TaskItem();
        item.listener = new TaskListener() {
            @Override
            public void get() {
                JSONArray jsonArray = JSON.parseArray(json);
                for (Object obj : jsonArray) {
                    String jsonObj = obj.toString();
                    // 获取最小Show
                    TShow tShow = JSON.parseObject(jsonObj, TShow.class);
                    JSONObject jsonObject = JSON.parseObject(jsonObj);
                    JSONObject images = jsonObject.getJSONObject("images");
                    JSONObject ids = jsonObject.getJSONObject("ids");
                    // id封装
                    tShow.ids_trakt = ids.getIntValue("trakt");
                    tShow.ids_slug = ids.getString("slug");
                    // 图片封装
                    tShow.fanart_thumb = images.getJSONObject("fanart")
                                               .getString("thumb");
                    tShow.poster_thumb = images.getJSONObject("poster")
                                               .getString("thumb");
                    tShow.banner_full = images.getJSONObject("banner")
                                              .getString("full");
                    // 添加集合
                    tShows.add(tShow);
                }
            }
            
            @Override
            public void update() {
                // 发送新加剧的广播
                Intent intent = new Intent();
                intent.setAction(ACTION_FOLLOW_POPULAR_SHOWS);
                intent.putExtra("TShows", tShows);
                // 发送广播更新列表
                mContext.sendBroadcast(intent);
            }
        };
        // 执行线程池
        pool.execute(item);
    }
    
    /**
     * 获取的数据(Trend)
     *
     * @param json
     *            访问的数据
     */
    public void jsonTShows2(final String json) {
        final ArrayList<TShow> tShows = new ArrayList<>();
        // 解析线程
        TaskItem item = new TaskItem();
        item.listener = new TaskListener() {
            @Override
            public void get() {
                JSONArray jsonArray = JSON.parseArray(json);
                for (Object object : jsonArray) {
                    // 获取单个数据的JSONObject
                    JSONObject jsonObj = JSON.parseObject(object.toString());
                    // 获取最小Show
                    TShow tShow = new TShow();
                    // 获取新增观看人数
                    tShow.watchers = jsonObj.getIntValue("watchers");
                    // 获取Show的JSONObject
                    JSONObject showJson = jsonObj.getJSONObject("show");
                    // 获取名字
                    tShow.title = showJson.getString("title");
                    // 获取年份
                    tShow.year = showJson.getIntValue("year");
                    // 图片
                    JSONObject images = showJson.getJSONObject("images");
                    JSONObject ids = showJson.getJSONObject("ids");
                    // id封装
                    tShow.ids_trakt = ids.getIntValue("trakt");
                    tShow.ids_slug = ids.getString("slug");
                    // 图片封装
                    tShow.fanart_thumb = images.getJSONObject("fanart")
                                               .getString("thumb");
                    tShow.poster_thumb = images.getJSONObject("poster")
                                               .getString("thumb");
                    // 添加集合
                    tShows.add(tShow);
                }
            }
            
            @Override
            public void update() {
                // 发送新加剧的广播
                Intent intent = new Intent();
                intent.setAction(ACTION_FOLLOW_TREND_SHOWS);
                intent.putExtra("TShows", tShows);
                // 发送广播更新列表
                mContext.sendBroadcast(intent);
            }
        };
        pool.execute(item);
    }
    
    /**
     * 获取的数据(Search)
     *
     * @param json
     *            访问的数据
     */
    public void jsonTShows3(final String json) {
        final ArrayList<TShow> tShows = new ArrayList<>();
        // 解析线程
        TaskItem item = new TaskItem();
        item.listener = new TaskListener() {
            @Override
            public void get() {
                JSONArray jsonArray = JSON.parseArray(json);
                for (Object object : jsonArray) {
                    // 获取最小Show
                    TShow tShow = new TShow();
                    // 获取Show的JSONObject
                    JSONObject showJson = JSON.parseObject(object.toString())
                                              .getJSONObject("show");
                    // 获取名字
                    tShow.title = showJson.getString("title");
                    // 获取年份
                    tShow.year = showJson.getIntValue("year");
                    // 图片
                    JSONObject images = showJson.getJSONObject("images");
                    JSONObject ids = showJson.getJSONObject("ids");
                    // id封装
                    tShow.ids_trakt = ids.getIntValue("trakt");
                    tShow.ids_slug = ids.getString("slug");
                    // 图片封装
                    tShow.fanart_thumb = images.getJSONObject("fanart")
                                               .getString("thumb");
                    tShow.poster_thumb = images.getJSONObject("poster")
                                               .getString("thumb");
                    // 添加集合
                    tShows.add(tShow);
                }
            }
            
            @Override
            public void update() {
                // 发送新加剧的广播
                Intent intent = new Intent();
                intent.setAction(ACTION_SEARCH_SHOWS);
                intent.putExtra("TShows", tShows);
                // 发送广播更新列表
                mContext.sendBroadcast(intent);
            }
        };
        pool.execute(item);
    }
    
    /**
     * 通过json封装Show对象 (剧集详情)
     * 
     * @param json
     *            json数据
     */
    public void jsonShow(final String json, final TShow tShow) {
        final Intent intent = new Intent();
        // 解析线程
        TaskItem item = new TaskItem();
        item.listener = new TaskListener() {
            @Override
            public void get() {
                // 解析Show
                Show show = JSON.parseObject(json, Show.class);
                // 拼装时间
                JSONObject airs = JSON.parseObject(show.getAirs());
                // 星期、时间和时区
                String day = airs.getString("day");
                String time = airs.getString("time");
                String timezone = airs.getString("timezone");
                if (Strings.isNotEmpty(day) && Strings.isNotEmpty(time))
                {
                    String[] dayTime = Dates.changeAir(timezone, day, time)
                                            .split(" ");
                    time = dayTime[0];
                    day = dayTime[1].replace("星期", "");
                }
                else {
                    day = "";
                    time = "";
                }
                
                show.setTime(time);
                show.setDay(day);
                show.setTimezone(timezone);
                // 剧集状态
                show.setStatus_zh(Strings.status(show.getStatus()));
                // 国家
                show.setCountry_zh(Strings.country(show.getCountry()));
                // 处理分类保存
                JSONArray array = JSON.parseArray(show.getGenres());
                String genres = "";
                // 判断是否为空
                if (array != null && array.size() != 0) {
                    for (Object obj : array) {
                        // 获取单个分类
                        String genre = obj.toString().trim();
                        if (!"".equals(genre)) {
                            String genre_zh = Strings.genre(genre);
                            genres += genre_zh.concat(",  ");
                        }
                    }
                    // 去除最后两个
                    genres = genres.substring(0, genres.length() - 3);
                }
                // 设置分类
                show.setGenres_zh(genres);
                if (tShow == null) {
                    // ID的封装
                    JSONObject ids = JSON.parseObject(show.getIds());
                    show.setSlug(ids.getString("slug"));
                    show.setTrakt(ids.getLongValue("trakt"));
                    // Images的封装
                    JSONObject images = JSON.parseObject(show.getImages());
                    show.setBanner(images.getJSONObject("banner")
                                         .getString("full"));
                }
                else {
                    // 必有字段赋值
                    show.setSlug(tShow.ids_slug);
                    show.setTrakt(tShow.ids_trakt);
                    show.setFanart(tShow.fanart_thumb);
                    show.setPoster(tShow.poster_thumb);
                    // 非搜索直接赋值
                    show.setBanner(tShow.banner_full);
                }
                // 装载Show
                intent.putExtra("Show", show);
            }
            
            @Override
            public void update() {
                // 发送新加剧的广播
                intent.setAction(ACTION_DETAIL_SHOW);
                // 发送广播更新列表
                mContext.sendBroadcast(intent);
            }
        };
        pool.execute(item);
    }
    
    /**
     * 获取观看人数
     *
     * @param json
     *            数据字符串
     * @param show
     *            当前Show
     * @return 有观看人数的Show
     */
    public Show jsonWatchers(String json, Show show) {
        JSONObject jsonObject = JSON.parseObject(json);
        show.setWatchers(jsonObject.getIntValue("watchers"));
        return show;
    }
    
    /**
     * 获取带翻译的Show
     *
     * @param json
     *            数据
     * @param show
     *            需要保存的Show
     */
    public void jsonTranslations(final String json, final Show show) {
        // 解析线程
        TaskItem item = new TaskItem();
        item.listener = new TaskListener() {
            @Override
            public void get() {
                JSONArray jsonArray = JSON.parseArray(json);
                // 获取第一个翻译
                String title_zh = "";
                String overview_zh = "";
                if (jsonArray.size() != 0) {
                    // 获取第一个
                    JSONObject object = jsonArray.getJSONObject(0);
                    // 获取Title和简介
                    title_zh = object.getString("title");
                    overview_zh = object.getString("overview");
                }
                if (Strings.isEmpty(title_zh))
                    title_zh = "暂无中文名";
                if (Strings.isEmpty(overview_zh))
                    overview_zh = "暂无中文简介";
                // 设置Show
                show.setTitle_zh(title_zh);
                show.setOverview_zh(overview_zh);
            }
            
            @Override
            public void update() {
                Intent intent = new Intent();
                intent.setAction(ACTION_DETAIL_SHOW_TRANS);
                mContext.sendBroadcast(intent);
            }
            
        };
        pool.execute(item);
    }
    
    /**
     * 解析Season和Episode对象
     *
     * @param json
     *            json数据字符串
     * @param show_Id
     *            剧集ID
     * @return Map集合
     */
    public static HashMap<String, ArrayList> jsonSeaEpi(String json,
                                                        long show_Id) {
        // 同步季集的数据
        HashMap<String, ArrayList> arrayMap = new HashMap<>();
        ArrayList<Season> seasons = new ArrayList<>();
        ArrayList<Episode> episodes = new ArrayList<>();
        // 判断json是否有值
        JSONArray jsonArray = JSON.parseArray(json);
        // 有数据
        for (Object obj : jsonArray) {
            // 获取解析后的Season对象
            Season season = jsonSeason(obj.toString(), show_Id);
            seasons.add(season);
            // 创建Episode的JSONObject
            String episodeArr = JSON.parseObject(obj.toString())
                                    .getString("episodes");
            // 创建Season对象对应的集集合
            if (Strings.isNotEmpty(episodeArr))
                episodes.addAll(jsonEpisodes(episodeArr,
                                             show_Id,
                                             season.getTrakt()));
        }
        arrayMap.put("seasons", seasons);
        arrayMap.put("episodes", episodes);
        return arrayMap;
    }
    
    /**
     * 解析Season的数据
     * 
     * @param json
     *            json数据字符串
     * @param show_Id
     *            剧集ID
     * @return 季
     */
    private static Season jsonSeason(String json, long show_Id) {
        // 解析对象
        Season season = JSONObject.parseObject(json, Season.class);
        // 解析ID
        JSONObject idsObj = JSON.parseObject(season.getIds());
        season.setTrakt(idsObj.getLongValue("trakt"));
        // 解析Images
        JSONObject imagesObj = JSON.parseObject(season.getImages());
        season.setPoster(imagesObj.getJSONObject("poster").getString("thumb"));
        season.setThumb(imagesObj.getJSONObject("thumb").getString("full"));
        // 设置是否已看
        season.setIsWatch(false);
        // 建立与Show的关系
        season.setShow_id(show_Id);
        // 赋值已看为0
        season.setWatched_episode(0);
        return season;
    }
    
    /**
     * 解析Episode的数据
     *
     * @param json
     *            json数据字符串
     * @param show_id
     *            剧ID
     * @param season_id
     *            季ID
     * @return 集
     */
    private static ArrayList<Episode> jsonEpisodes(String json,
                                                   long show_id,
                                                   long season_id) {
        ArrayList<Episode> episodes = new ArrayList<>();
        JSONArray jsonArray = JSON.parseArray(json);
        for (Object obj : jsonArray) {
            Episode episode = JSON.parseObject(obj.toString(), Episode.class);
            if (episode == null)
                return episodes;
            // 补空
            if (episode.getFirst_aired() == null)
                episode.setFirst_aired("未知");
            // 封装图片和ID
            JSONObject imageJson = JSON.parseObject(episode.getImages());
            JSONObject idsJson = JSON.parseObject(episode.getIds());
            episode.setScreenshot(imageJson.getJSONObject("screenshot")
                                           .getString("thumb"));
            episode.setTrakt(idsJson.getLongValue("trakt"));
            // 设置是否已看
            episode.setIsWatch(false);
            // 设置关联的seasonID
            episode.setSeason_id(season_id);
            // 设置关联的showID
            episode.setShow_id(show_id);
            episodes.add(episode);
        }
        return episodes;
    }
}
