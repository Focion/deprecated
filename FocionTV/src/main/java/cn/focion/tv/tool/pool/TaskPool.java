package cn.focion.tv.tool.pool;

import android.os.Handler;
import android.os.Message;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import cn.focion.tv.tool.util.Cores;

/**
 * 任务执行线程池 Created by Pirate on 14-5-29.
 */
public class TaskPool {
    
    /**
     * 固定线程执行数
     */
    private static int nThreads = 5;
    
    /**
     * 每个程序就只有一个
     */
    private static TaskPool mPool;
    
    /** 线程执行器 */
    private static ExecutorService mExecutor = null;
    
    static {
        // 获取手机核数
        nThreads = Cores.getNumCores();
        // 初始化线程池
        mPool = new TaskPool(nThreads * 5);
        
    }
    
    /**
     * 构造线程池
     * 
     * @param thread
     *            初始线程数
     */
    private TaskPool(int thread) {
        // 创建初始线程数的线程执行器
        mExecutor = Executors.newFixedThreadPool(thread);
    }
    
    /**
     * 单例构造线程池
     * 
     * @return 单例线程池
     */
    public static TaskPool getInstance() {
        return mPool;
    }
    
    /**
     * 更新UI所有Handler
     */
    @SuppressWarnings("ALL")
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            TaskItem item = (TaskItem) msg.obj;
            item.listener.update();
        }
    };
    
    /**
     * 执行任务
     * 
     * @param item
     *            任务对象
     */
    public void execute(final TaskItem item) {
        // 线程执行器执行线程
        mExecutor.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    // 定义了回调
                    if (item.listener != null) {
                        item.listener.get();
                        // 交由UI线程处理
                        Message msg = handler.obtainMessage();
                        msg.obj = item;
                        handler.sendMessage(msg);
                    }
                }
                catch (Exception e) {
                    throw new RuntimeException("线程执行出错");
                }
            }
        });
    }
    
    /**
     * 获取线程池的执行器
     * 
     * @return executorService 线程执行器
     */
    public static ExecutorService getExecutorService() {
        return mExecutor;
    }
    
    /**
     * 描述：立即关闭.
     */
    public void shutdownNow() {
        if (!mExecutor.isTerminated())
            mExecutor.shutdownNow();
        
    }
    
    /**
     * 描述：平滑关闭.
     */
    public void shutdown() {
        if (!mExecutor.isTerminated())
            mExecutor.shutdown();
    }
}
