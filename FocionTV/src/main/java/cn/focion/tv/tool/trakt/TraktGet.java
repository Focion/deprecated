package cn.focion.tv.tool.trakt;

import android.content.Context;
import android.widget.Toast;

import cn.focion.tv.tool.connect.RequestQueue;
import cn.focion.tv.tool.connect.Response;

/**
 * 所有Trakt的访问 Created by Pirate on 2015/3/26.
 */
public class TraktGet {
    
    // Trakt的访问地址
    private static final String TRAKT_URL = "https://api-v2launch.trakt.tv";
    
    // 单例网络访问类
    private static TraktGet mTraktGet;
    
    // 每页条数
    private int limit = 10;
    
    // 访问线程池
    private RequestQueue mRequestQueue;
    
    // 上下文
    private Context mContext;
    
    /**
     * 私有构造方法
     */
    private TraktGet(Context context, RequestQueue requestQueue) {
        // 初始化网络访问客户端
        mRequestQueue = requestQueue;
        mContext = context;
    }
    
    /**
     * 获取网络访问对象
     *
     * @return 单例的网络访问对象
     */
    public static TraktGet getInstance(Context context,
                                       RequestQueue requestQueue) {
        if (mTraktGet == null)
            mTraktGet = new TraktGet(context, requestQueue);
        return mTraktGet;
    }
    
    /*********************************************** Show的分类访问方法 ************************************************/
    
    /**
     * 获取分类 GET https://api-v2launch.trakt.tv/genres/type
     * 
     * @param listener
     *            响应监听
     * @param errorListener
     *            错误监听
     */
    public void genres(Response.Listener<String> listener,
                       Response.ErrorListener errorListener) {
        request(String.format("%s/genres/shows", TRAKT_URL),
                listener,
                errorListener);
    }
    
    /*********************************************** Show的相关访问方法 ************************************************/
    
    /**
     * 获取当下流行剧 GET https://api-v2launch.trakt.tv/shows/popular
     *
     * @param full
     *            是否显示全部信息
     * @param images
     *            是否带图片
     * @param page
     *            页数
     * @param listener
     *            响应监听
     * @param errorListener
     *            错误监听
     */
    public void popularShows(boolean full,
                             boolean images,
                             int page,
                             Response.Listener<String> listener,
                             Response.ErrorListener errorListener) {
        request(extended("/shows/popular", full, images, page, limit),
                listener,
                errorListener);
    }
    
    /**
     * 获取排行榜 GET https://api-v2launch.trakt.tv/shows/trending
     *
     * @param full
     *            是否显示全部信息
     * @param images
     *            是否带图片
     * @param page
     *            页数
     * @param listener
     *            响应监听
     * @param errorListener
     *            错误监听
     */
    public void trendingShows(boolean full,
                              boolean images,
                              int page,
                              Response.Listener<String> listener,
                              Response.ErrorListener errorListener) {
        request(extended("/shows/trending", full, images, page, limit),
                listener,
                errorListener);
    }
    
    /**
     * 获取最近播出剧 GET https://api-v2launch.trakt.tv/shows/updates/start_date
     *
     * @param start_date
     *            开始时间 yyyy-HH-dd (美国时间)
     * @param full
     *            是否显示全部信息
     * @param images
     *            是否带图片
     * @param page
     *            页数
     * @param listener
     *            响应监听
     * @param errorListener
     *            错误监听
     */
    public void recentlyShows(String start_date,
                              boolean full,
                              boolean images,
                              int page,
                              Response.Listener<String> listener,
                              Response.ErrorListener errorListener) {
        request(extended("/shows/updates/".concat(start_date),
                         full,
                         images,
                         page,
                         limit),
                listener,
                errorListener);
    }
    
    /**
     * 获取单个Show信息 GET https://api-v2launch.trakt.tv/shows/id
     *
     * @param ids
     *            通过trakt或者sulg
     * @param full
     *            是否显示全部信息
     * @param images
     *            是否带图片
     * @param listener
     *            响应监听
     * @param errorListener
     *            错误监听
     */
    public void singleShow(Object ids,
                           boolean full,
                           boolean images,
                           Response.Listener<String> listener,
                           Response.ErrorListener errorListener) {
        request(extended("/shows/".concat(ids.toString()), full, images),
                listener,
                errorListener);
    }
    
    /**
     * 翻译 GET https://api-v2launch.trakt.tv/shows/id/translations/language
     *
     * @param ids
     *            通过trakt或者sulg
     * @param listener
     *            响应监听
     * @param errorListener
     *            错误监听
     */
    public void translationsShow(Object ids,
                                 Response.Listener<String> listener,
                                 Response.ErrorListener errorListener) {
        request(String.format("%s/shows/%s/translations/zh",
                              TRAKT_URL,
                              ids.toString()),
                listener,
                errorListener);
    }
    
    /**
     * 获取单个Show演员列表 GET https://api-v2launch.trakt.tv/shows/id/people
     *
     * @param ids
     *            通过trakt或者sulg
     * @param full
     *            是否显示全部信息
     * @param images
     *            是否带图片
     * @param listener
     *            响应监听
     * @param errorListener
     *            错误监听
     */
    public void peopleShow(Object ids,
                           boolean full,
                           boolean images,
                           Response.Listener<String> listener,
                           Response.ErrorListener errorListener) {
        request(extended(String.format("/shows/%s/people", ids.toString()),
                         full,
                         images),
                listener,
                errorListener);
    }
    
    /**
     * 获取相似剧 GET https://api-v2launch.trakt.tv/shows/id/related
     *
     * @param ids
     *            通过trakt或者sulg
     * @param full
     *            是否显示全部信息
     * @param images
     *            是否带图片
     * @param page
     *            页数
     * @param listener
     *            响应监听
     * @param errorListener
     *            错误监听
     */
    public void relatedShows(Object ids,
                             boolean full,
                             boolean images,
                             int page,
                             Response.Listener<String> listener,
                             Response.ErrorListener errorListener) {
        request(extended(String.format("/shows/%s/related", ids.toString()),
                         full,
                         images,
                         page,
                         limit),
                listener,
                errorListener);
    }
    
    /**
     * 获取剧的统计数据 GET https://api-v2launch.trakt.tv/shows/id/stats
     *
     * @param ids
     *            剧id
     * @param listener
     *            响应监听
     * @param errorListener
     *            错误监听
     */
    public void watchersShow(Object ids,
                             Response.Listener<String> listener,
                             Response.ErrorListener errorListener) {
        request(String.format("%s/shows/%s/stats", TRAKT_URL, ids.toString()),
                listener,
                errorListener);
    }
    
    /**
     * 搜索 GET
     * https://api-v2launch.trakt.tv/search?query=batman&type=type&year=2015
     *
     * @param query
     *            查询条件
     * @param currentPage
     *            当前页
     * @param listener
     *            响应监听
     * @param errorListener
     *            错误监听
     */
    public void searchShows(String query,
                            int currentPage,
                            Response.Listener<String> listener,
                            Response.ErrorListener errorListener) {
        request(String.format("%s/search?query=%s&type=show&page=%d&limit=%d",
                              TRAKT_URL,
                              query,
                              currentPage,
                              limit), listener, errorListener);
    }
    
    /*********************************************** Season的相关访问方法 ************************************************/
    
    /**
     * 获取季 GET https://api-v2launch.trakt.tv/shows/id/seasons
     *
     * @param ids
     *            通过trakt或者sulg
     * @param full
     *            是否显示全部信息
     * @param images
     *            是否带图片
     * @param episodes
     *            是否显示集的集合
     * @param listener
     *            响应监听
     * @param errorListener
     *            错误监听
     */
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void seasons(Object ids,
                        boolean full,
                        boolean images,
                        boolean episodes,
                        Response.Listener<String> listener,
                        Response.ErrorListener errorListener) {
        // 获取基本url
        String url = extended(String.format("/shows/%s/seasons", ids.toString()),
                              full,
                              images);
        if (episodes)
            url = url.concat(",episodes");
        request(url, listener, errorListener);
    }
    
    /**
     * 获取单季的信息 GET
     * https://api-v2launch.trakt.tv/shows/game-of-thrones/seasons/season
     *
     * @param ids
     *            通过trakt或者sulg
     * @param season
     *            季
     * @param full
     *            是否显示全部信息
     * @param images
     *            是否带图片
     * @param listener
     *            响应监听
     * @param errorListener
     *            错误监听
     */
    public void singleSeason(Object ids,
                             int season,
                             boolean full,
                             boolean images,
                             Response.Listener<String> listener,
                             Response.ErrorListener errorListener) {
        request(extended(String.format("/shows/%s/seasons/%d",
                                       ids.toString(),
                                       season), full, images),
                listener,
                errorListener);
    }
    
    /*********************************************** Episode的相关访问方法 ************************************************/
    
    /**
     * 获取单集的信息 GET
     * https://api-v2launch.trakt.tv/shows/id/seasons/season/episodes/episode
     *
     * @param ids
     *            通过trakt或者sulg
     * @param season
     *            季
     * @param episode
     *            集
     * @param full
     *            是否显示全部信息
     * @param images
     *            是否带图片
     * @param listener
     *            响应监听
     * @param errorListener
     *            错误监听
     */
    public void singleEpisode(Object ids,
                              int season,
                              int episode,
                              boolean full,
                              boolean images,
                              Response.Listener<String> listener,
                              Response.ErrorListener errorListener) {
        request(extended(String.format("/shows/%s/seasons/%d/episodes/%d",
                                       ids.toString(),
                                       season,
                                       episode),
                         full,
                         images),
                listener,
                errorListener);
    }
    
    /*********************************************** 其他方法 ************************************************/
    
    /**
     * 添加访问参数，不带页数，默认min格式，包括：title, year, ids
     *
     * @param url
     *            地址
     * @param full
     *            是否显示全部数据
     * @param images
     *            是否显示全部图片
     * @return 拼装地址
     */
    @SuppressWarnings("ResultOfMethodCallIgnored")
    private String extended(String url, boolean full, boolean images) {
        // 拼接
        String extended = TRAKT_URL.concat(url);
        // 判断
        if (full && images)
            extended = extended.concat("?extended=full,images");
        else if (full)
            extended = extended.concat("?extended=full");
        else if (images)
            extended = extended.concat("?extended=images");
        return extended;
    }
    
    /**
     * 添加访问参数，不带页数，默认min格式，包括：title, year, ids
     *
     * @param url
     *            地址
     * @param full
     *            是否显示全部数据
     * @param images
     *            是否显示全部图片
     * @param page
     *            页数
     * @param limit
     *            显示条数
     * @return 拼装地址
     */
    @SuppressWarnings("ResultOfMethodCallIgnored")
    private String extended(String url,
                            boolean full,
                            boolean images,
                            int page,
                            int limit) {
        // 获取拼装
        String fullImages = extended(url, full, images);
        // 如果获取最小数据
        if (fullImages.contains("?"))
            fullImages = fullImages.concat("&");
        else
            fullImages = fullImages.concat("?");
        // 拼接
        return String.format("%spage=%d&limit=%d", fullImages, page, limit);
    }
    
    /**
     * Trakt请求
     *
     * @param url
     *            地址
     * @param listener
     *            响应监听
     */
    private void request(String url,
                         Response.Listener<String> listener,
                         Response.ErrorListener errorListener) {
        mRequestQueue.add(new TraktRequest(url, listener, errorListener));
    }
}
