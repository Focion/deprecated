package cn.focion.tv.tool.pool;

/**
 * 任务类 Created by Pirate on 14-5-29.
 */
public class TaskItem {
    
    /** 记录的当前索引 */
    public int position;
    
    /** 执行完成的回调接口 */
    public TaskListener listener;
    
    /** 执行完成的结果 */
    public Object result;
    
}
