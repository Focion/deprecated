package cn.focion.tv.tool.trakt;

import java.util.HashMap;
import java.util.Map;

import cn.focion.tv.tool.connect.Response;
import cn.focion.tv.tool.connect.StringRequest;

/**
 * Trakt Https请求设置 Created by Pirate on 2015/4/3.
 */
public class TraktRequest extends StringRequest {
    
    public TraktRequest(String url,
                        Response.Listener<String> listener,
                        Response.ErrorListener errorListener) {
        super(Method.GET, url, listener, errorListener);
    }
    
    @Override
    public Map<String, String> getHeaders() {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        headers.put("trakt-api-version", "2");
        headers.put("trakt-api-key",
                    "eb9b45763ce3f69322d4f25096d3b9682dad43fde436e646f1fe00eeb7ff186b");
        return headers;
    }
    
}
