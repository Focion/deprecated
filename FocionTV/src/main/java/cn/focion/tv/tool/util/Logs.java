package cn.focion.tv.tool.util;

import android.util.Log;

import cn.focion.tv.BuildConfig;

/**
 * 日志 Created by Pirate on 2015/4/2.
 */
public class Logs {
    
    public static void i(Class<?> clazz, String msg) {
        if (BuildConfig.DEBUG)
            Log.i(String.format("------ %s ------", clazz.getSimpleName()), msg);
    }
    
}
