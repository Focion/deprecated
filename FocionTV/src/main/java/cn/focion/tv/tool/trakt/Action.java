package cn.focion.tv.tool.trakt;

/**
 * 所有广播常量 Created by Pirate on 2015-05-09.
 */
public class Action {
    
    /********** 广播 **********/
    
    public static final String ACTION_EPISODES_TO_SEASONS = "BOARD_EPISODES_TO_ACTIVITY";
    
    public static final String ACTION_EPISODE_TO_WATCHLIST = "BOARD_EPISODE_TO_WATCHLIST";
    
    public static final String ACTION_UPDATE_WATCHLIST = "BOARD_UPDATE_WATCHLIST";
    
    /********** 带参数的请求 **********/
    
    // EpisodeActivity返回参数 结果码
    public static final int EPISODEACTIVITY_WATCHFRAGMENT = 100;
    
}
