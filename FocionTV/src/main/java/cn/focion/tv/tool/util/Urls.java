package cn.focion.tv.tool.util;

import org.apache.http.Header;
import org.apache.http.HttpResponse;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 地址工具类 Created by Pirate on 2015/3/26.
 */
public class Urls {
    
    /**
     * 通过url获取文件名
     *
     * @param url
     *            地址
     * @param response
     *            响应
     * @return 文件名
     */
    public static String getFileNameFromNetUrl(String url, HttpResponse response) {
        if (Strings.isEmpty(url))
            return null;
        String name = null;
        try {
            String suffix = null;
            // 获取后缀
            if (url.lastIndexOf(".") != -1) {
                // 获取后缀
                suffix = url.substring(url.lastIndexOf("."));
                if (suffix.contains("/") || suffix.contains("?")
                    || suffix.contains("&"))
                    suffix = null;
            }
            if (suffix == null) {
                // 获取文件名
                String fileName = "unknow.tmp";
                // Post请求的文件名获取
                Header[] headers = response.getHeaders("content-disposition");
                for (Header header : headers) {
                    // 获取头信息匹配
                    Matcher m = Pattern.compile(".*filename=(.*)")
                                       .matcher(header.getValue());
                    if (m.find())
                        fileName = m.group(1).replace("\"", "");
                }
                if (fileName != null && fileName.lastIndexOf(".") != -1)
                    suffix = fileName.substring(fileName.lastIndexOf("."));
            }
            // 获取加密文件名
            name = MD5.encrypt(url) + suffix;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return name;
    }
}
