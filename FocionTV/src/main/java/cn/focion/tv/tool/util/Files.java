package cn.focion.tv.tool.util;

import android.content.Context;
import android.os.Environment;

import java.io.File;

/**
 * 文件操作工具 Created by Pirate on 2015/4/13.
 */
public class Files {
    
    private static final String cacheDir = "/Android/data/cn.focion.tv";
    
    /**
     * 文件缓存地址
     * 
     * @param context
     *            上下文
     * @param cacheName
     *            缓存文件名
     * @return 缓存文件
     */
    public static File cacheFile(Context context, String cacheName) {
        File fileDir = null;
        // 判断是否有内置SD卡存在
        if (isExistSD())
            fileDir = new File(Environment.getExternalStorageDirectory(),
                               String.format("%s/%s", cacheDir, cacheName));
        // 判断文件夹是否存在
        if (fileDir == null || !fileDir.exists() && !fileDir.mkdirs())
            fileDir = context.getCacheDir();
        
        return fileDir;
    }
    
    /**
     * 获取图片缓存文件
     * 
     * @param context
     *            上下文
     * @return 缓存文件对象
     */
    public static File imageCacheFile(Context context) {
        
        File fileDir = null;
        // 判断是否有内置SD卡存在
        if (isExistSD())
            fileDir = cacheFile(context, "");
        // 判断文件夹是否存在
        if (fileDir == null || !fileDir.exists() && !fileDir.mkdirs())
            fileDir = context.getCacheDir();
        
        return fileDir;
    }
    
    /**
     * 判断SD卡是否存在
     * 
     * @return true 存在 false 不存在
     */
    public static boolean isExistSD() {
        return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
    }
    
}
