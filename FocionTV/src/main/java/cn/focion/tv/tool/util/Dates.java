package cn.focion.tv.tool.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

/**
 * 时间处理 Created by Pirate on 2015/4/9.
 */
public class Dates {
    
    // 中国时区
    private static final String TIMEZONE_CN = "Asia/Shanghai";
    
    private static final Locale LOCALE_CN = Locale.CHINA;
    
    // 返回时间格式
    private static final String ZONE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    
    // 11:11 每周星期日
    private static final String AIR_FORMAT = "HH:mm 每周EEE";
    
    // 2000年 11月11日
    public static final String YYYY_MM_DD = "yyyy年  MM月dd日";
    
    // 11月11日， 2000年 （星期日）
    public static final String WEEK_MM_DD_YYYY = "MM月dd日，  yyyy年 （EEE）";
    
    // 11月 11日
    public static final String MD_CN = "MM月 dd日";
    
    // 2000-11-11
    public static final String YMD = "yyyy-MM-dd";
    
    /**
     * 改变时间时区
     * 
     * @param locale
     *            时间所属时区
     * @param date
     *            时间
     * @param afterFormat
     *            格式化后的格式
     * @return 时间字符串
     */
    public static String changeZone(String locale,
                                    String date,
                                    String afterFormat) {
        if (Strings.isEmpty(date) || date.contains("未知"))
            return "未知";
        return change(locale, date, ZONE_FORMAT, afterFormat);
    }
    
    /**
     * 改变时间时区 HH:mm
     * 
     * @param locale
     *            时间所属时区
     * @param day
     *            星期
     * @param time
     *            时间
     * @return 时间字符串
     */
    public static String changeAir(String locale, String day, String time) {
        return change(locale,
                      String.format("%s %s", time, day),
                      "HH:mm EEE",
                      AIR_FORMAT);
    }
    
    /**
     * 改变时区
     * 
     * @param locale
     *            时间所属时区
     * @param date
     *            时间
     * @param beforFormat
     *            格式
     * @param afterFormat
     *            格式化后的格式
     * @return 时间字符串
     */
    private static String change(String locale,
                                 String date,
                                 String beforFormat,
                                 String afterFormat) {
        // 原时区时间
        SimpleDateFormat format = new SimpleDateFormat(beforFormat, Locale.US);
        // 设置时间时区
        format.setTimeZone(TimeZone.getTimeZone(locale));
        // 本地时区时间
        SimpleDateFormat formatCur = new SimpleDateFormat(afterFormat,
                                                          LOCALE_CN);
        formatCur.setTimeZone(TimeZone.getTimeZone(TIMEZONE_CN));
        // 确定Date
        try {
            return formatCur.format(format.parse(date));
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }
    
    /**
     * 得到当前日期时间的字符串。
     *
     * @param format
     *            日期格式，如yyyy-MM-dd HH:mm:ss
     * @return 当前日期字符串
     */
    public static String currentDate(String format) {
        String currentDate = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format, LOCALE_CN);
            Calendar calendar = Calendar.getInstance();
            currentDate = sdf.format(calendar.getTime());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return currentDate;
    }
    
    /**
     * 获取当前时间的Long型
     *
     * @param format
     *            格式
     * @return 返回long
     */
    public static long currentTime(String format) {
        String currentDate = currentDate(format);
        return dateFormat(currentDate, format).getTime();
    }
    
    /**
     * 将字符串转换成指定格式日期
     *
     * @param dateStr
     *            字符串
     * @param format
     *            格式 如果格式为null，默认格式：yyyy-MM-dd
     * @return 日期
     */
    
    public static Date dateFormat(String dateStr, String format) {
        Date date = null;
        format = format == null ? YMD : format;
        SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat(format,
                                                                  LOCALE_CN);
        try {
            date = mSimpleDateFormat.parse(dateStr);
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
    
    /**
     * 获取milliseconds表示的日期的字符串
     *
     * @param milliseconds
     *            日期的milliseconds
     * @param format
     *            格式化字符串，如"yyyy-MM-dd HH:mm:ss"
     * @return String 日期的字符串表示
     */
    public static String dateFormat(long milliseconds, String format) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format, LOCALE_CN);
            return sdf.format(milliseconds);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
