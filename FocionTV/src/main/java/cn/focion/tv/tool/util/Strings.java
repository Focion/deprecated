package cn.focion.tv.tool.util;

import java.util.StringTokenizer;

import cn.focion.tv.model.Season;
import cn.focion.tv.model.Show;

/**
 * 字符串工具类 Created by Pirate on 2015/3/26.
 */
public class Strings {
    
    /**
     * 判断字符串不为空
     *
     * @param str
     *            字符串
     * @return true 不为空 false 为空
     */
    public static boolean isNotEmpty(String str) {
        boolean result = false;
        if (str != null && !"".equals(str) && !"null".equals(str))
            result = true;
        return result;
    }
    
    /**
     * 判断字符串为空
     *
     * @param str
     *            字符串
     * @return true为空 false不为空
     */
    public static boolean isEmpty(String str) {
        return !isNotEmpty(str);
    }
    
    /**
     * 获取类似S00E00的结构
     *
     * @param season
     *            季数字
     * @param episodeNumber
     *            集数字
     * @return 季集结构
     */
    public static String dealDisplay(long season, long episodeNumber) {
        return String.format("S%02d E%02d", season, episodeNumber);
    }
    
    /**
     * 判断字符串是否为邮箱
     *
     * @param email
     *            邮箱地址
     * @return true 是邮箱 false 不是邮箱
     */
    public static boolean isEmail(String email) {
        return email.matches("^\\w+@\\w+\\.\\w+$");
    }
    
    /**
     * 判断字符串是否是手机
     *
     * @param mobile
     *            手机号
     * @return true 是手机 false 不是手机
     */
    public static boolean isMobilePhone(String mobile) {
        return mobile.matches("^[1][3578]\\d{9}$");
    }
    
    /**
     * 加空格的方法
     *
     * @param text
     *            字符串
     * @return 加空格后的字符串
     */
    public static String space(String text) {
        String result = "";
        // 判断空
        if (isEmpty(text))
            return result;
        // 截取前3个字符
        int length;
        String space3 = text.substring(0, 3).trim();
        // 去除中午空格
        if (space3.contains("　"))
            text = text.substring(2, text.length());
        length = space3.length();
        if (length == 0) {
            // 全为空取4个
            String space4 = text.substring(0, 4).trim().replace("　", "");
            length = space4.length();
        }
        switch (length) {
            case 3:
                result = "        ";
                break;
            case 2:
                result = "      ";
                break;
            case 1:
                result = "    ";
                break;
            case 0:
                result = "  ";
                break;
        }
        return result.concat(text);
    }
    
    /**
     * 通过国家代码返回国家
     *
     * @param country
     *            国家代码
     * @return 国家中文名
     */
    public static String country(String country) {
        switch (country) {
            case "us":
                return "美国";
            case "gb":
                return "英国";
            case "jp":
                return "日本";
            case "kr":
                return "韩国";
            default:
                return "未知";
        }
    }
    
    /**
     * 返回剧集状态
     *
     * @param status
     *            状态
     * @return 中文
     */
    public static String status(String status) {
        switch (status) {
            case "returning series":
                return "已回归";
            case "in production":
                return "未回归";
            case "canceled":
                return "已停播";
            case "ended":
                return "已完结";
            default:
                return "未知";
        }
    }
    
    /**
     * 获取分类中文名
     * 
     * @return 分类中文
     */
    public static String genre(String genre_en) {
        switch (genre_en) {
            case "action":
                return "动作";
            case "adventure":
                return "冒险";
            case "animation":
                return "动画";
            case "biography":
                return "传记";
            case "children":
                return "儿童";
            case "comedy":
                return "喜剧";
            case "disaster":
                return "灾难";
            case "documentary":
                return "纪录";
            case "drama":
                return "歌剧";
            case "eastern":
                return "东部";
            case "fantasy":
                return "科幻";
            case "game-show":
                return "游戏";
            case "history":
                return "历史";
            case "home-and-garden":
                return "家庭&花园";
            case "holiday":
                return "节日";
            case "horror":
                return "恐怖";
            case "mini-series":
                return "迷你剧";
            case "none":
                return "无分类";
            case "news":
                return "新闻";
            case "reality":
                return "真实";
            case "science-fiction":
                return "科幻小说";
            case "soap":
                return "肥皂剧";
            case "special-interest":
                return "特别兴趣";
            case "sports":
                return "体育";
            case "sporting-event":
                return "体育赛事";
            case "talk-show":
                return "脱口秀";
            case "thriller":
                return "惊悚";
            case "western":
                return "西部";
            default:
                return "无分类";
        }
    }
    
    /**
     * 一段字符串首字母大写
     * 
     * @param current
     *            字符串
     * @return 大写后的值
     */
    public static String toUpCase(String current) {
        StringBuilder sb = new StringBuilder();
        StringTokenizer st = new StringTokenizer(current);
        while (st.hasMoreTokens()) {
            String str = st.nextToken();
            char c = (str).charAt(0);
            if (c >= 'a' && c <= 'z') {
                c = (char) ((int) c - 32);
                sb.append(c);
            }
            else
                sb.append(c);
            for (int i = 1; i < (str).length(); i++) {
                char c1 = (str).charAt(i);
                sb.append(c1);
            }
            sb.append(" ");
        }
        return sb.toString();
    }
    
    /**
     * 获取剧的中文名，没有就返回英文
     * 
     * @param show
     *            剧集
     * @return 剧集名
     */
    public static String title(Show show) {
        return isEmpty(show.getTitle_zh()) || show.getTitle_zh().contains("暂无") ? show.getTitle()
                                                                               : show.getTitle_zh();
    }
    
    /**
     * 获取季名字
     */
    public static String title(Season season) {
        return String.format("%s",
                             season.getNumber() == 0 ? "特别季"
                                                    : String.format("第 %d 季",
                                                                    season.getNumber()));
    }
}
