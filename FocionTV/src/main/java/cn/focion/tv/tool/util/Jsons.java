package cn.focion.tv.tool.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import cn.focion.tv.model.Show;
import cn.focion.tv.temp.TShow;

/**
 * JSON工具类 Created by Pirate on 7-8-27.
 */
public class Jsons {
    
    /**
     * 从json字符串转为对应的对象集合
     *
     * @param json
     *            json字符串
     * @param clazz
     *            对象类型
     * @return 对象集合
     */
    public static <T> List<T> jsonToList(String json, Class<T> clazz) {
        return JSON.parseArray(json, clazz);
    }
    
    /**
     * 将json字符串转换成Map对象
     *
     * @param json
     *            json字符串
     * @return 封装的Map集合
     */
    public static HashMap<String, Object> jsonToMap(String json) {
        JSONObject jsonObject = JSONObject.parseObject(json);
        HashMap<String, Object> jsonMap = new HashMap<String, Object>();
        // 循环JsonObj实体
        for (Entry<String, Object> entry : jsonObject.entrySet())
            // 放入Map集合
            jsonMap.put(entry.getKey(), entry.getValue());
        return jsonMap;
    }
}
