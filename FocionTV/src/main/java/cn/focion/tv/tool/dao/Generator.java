package cn.focion.tv.tool.dao;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

/**
 * GreenDao创建器 Created by Pirate on 2015/3/27.
 */
public class Generator {
    
    public static void main(String args[]) throws Exception {
        // 创建生成表的schema
        Schema schema = new Schema(1, "cn.focion.tv.model");
        // 生成所有实例
        trySetupEntity(schema);
        // 生成
        new DaoGenerator().generateAll(schema, "./FocionTV/src/main/java");
    }
    
    /** 公共数据 */
    private static void trySetupCommon(Schema schema) {
        Entity show = schema.addEntity("Genre");
    }
    
    /** 用户数据 */
    private static void trySetupEntity(Schema schema) {
        showEntity(schema);
        seasonEntity(schema);
        episodeEntity(schema);
        genreEntity(schema);
    }
    
    private static void showEntity(Schema schema) {
        /** 剧 */
        Entity show = schema.addEntity("Show");
        // 序列化
        show.implementsSerializable();
        // id
        show.addStringProperty("ids");
        // 数字id
        show.addLongProperty("trakt").primaryKey().unique();
        // 文字id
        show.addStringProperty("slug");
        // 剧名
        show.addStringProperty("title");
        show.addStringProperty("title_zh");
        // 开播时间
        show.addIntProperty("year");
        // 简介
        show.addStringProperty("overview");
        show.addStringProperty("overview_zh");
        // 剧 首播时间
        show.addStringProperty("first_aired");
        // 播出时间
        show.addStringProperty("airs");
        // 星期
        show.addStringProperty("day");
        // 时间
        show.addStringProperty("time");
        // 时区
        show.addStringProperty("timezone");
        // 国家
        show.addStringProperty("country");
        show.addStringProperty("country_zh");
        // 每集时长
        show.addIntProperty("runtime");
        // 播出台
        show.addStringProperty("network");
        // 状态
        show.addStringProperty("status");
        show.addStringProperty("status_zh");
        // 评分
        show.addFloatProperty("rating");
        // 投票
        show.addIntProperty("votes");
        // 最近一次更新时间
        show.addStringProperty("updated_at");
        // 翻译
        show.addStringProperty("available_translations");
        // 类型
        show.addStringProperty("genres");
        show.addStringProperty("genres_zh");
        // 已播出数量
        show.addIntProperty("aired_episodes");
        // 观看人数
        show.addIntProperty("watchers");
        // 图片
        show.addStringProperty("images");
        show.addStringProperty("fanart");
        show.addStringProperty("poster");
        show.addStringProperty("banner");
    }
    
    private static void seasonEntity(Schema schema) {
        /** 季 */
        Entity season = schema.addEntity("Season");
        // 序列化
        season.implementsSerializable();
        // 数字id
        season.addStringProperty("ids");
        season.addLongProperty("trakt").primaryKey().unique();
        // 季
        season.addIntProperty("number");
        // 评分
        season.addFloatProperty("rating");
        // 当季总集数
        season.addIntProperty("episode_count");
        // 已播放集数
        season.addIntProperty("aired_episodes");
        // 简介
        season.addStringProperty("overview");
        // 图片
        season.addStringProperty("images");
        season.addStringProperty("poster");
        season.addStringProperty("thumb");
        // 关系
        season.addLongProperty("show_id");
        // 是否已看
        season.addBooleanProperty("isWatch");
        // 已看集数
        season.addIntProperty("watched_episode");
        // 标记数据是否改变
        season.addBooleanProperty("flag");
    }
    
    private static void episodeEntity(Schema schema) {
        /** 集 */
        Entity episode = schema.addEntity("Episode");
        // 序列化
        episode.implementsSerializable();
        // 数字id
        episode.addStringProperty("ids");
        episode.addLongProperty("trakt").primaryKey().unique();
        // 季
        episode.addIntProperty("season");
        // 集名
        episode.addStringProperty("title");
        // 集
        episode.addIntProperty("number");
        // 简介
        episode.addStringProperty("overview");
        // 评分
        episode.addFloatProperty("rating");
        // 首播
        episode.addStringProperty("first_aired");
        // 当前更新
        episode.addStringProperty("updated_at");
        // 图片
        episode.addStringProperty("images");
        episode.addStringProperty("screenshot");
        // SeasonID
        episode.addLongProperty("season_id");
        // ShowID
        episode.addLongProperty("show_id");
        // 是否已看
        episode.addBooleanProperty("isWatch");
        // 标记数据是否改变
        episode.addBooleanProperty("flag");
    }
    
    private static void peopleEntity(Schema schema) {
        /** 演员表 */
        Entity people = schema.addEntity("People");
        // 序列化
    }
    
    private static void genreEntity(Schema schema) {
        /** 分类 */
        Entity genre = schema.addEntity("Genre");
        // 序列化
        genre.implementsSerializable();
        // 分类名
        genre.addStringProperty("name");
        // 主键
        genre.addStringProperty("slug").primaryKey().unique();
        // 分类中文名
        genre.addStringProperty("name_zh");
    }
}
