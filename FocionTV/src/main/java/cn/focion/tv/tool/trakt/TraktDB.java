package cn.focion.tv.tool.trakt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import cn.focion.tv.BuildConfig;
import cn.focion.tv.model.DaoSession;
import cn.focion.tv.model.Episode;
import cn.focion.tv.model.EpisodeDao;
import cn.focion.tv.model.Season;
import cn.focion.tv.model.SeasonDao;
import cn.focion.tv.model.Show;
import cn.focion.tv.model.ShowDao;
import cn.focion.tv.tool.pool.TaskItem;
import cn.focion.tv.tool.pool.TaskListener;
import cn.focion.tv.tool.pool.TaskPool;
import cn.focion.tv.tool.util.Lists;
import cn.focion.tv.tool.util.Strings;
import cn.focion.tv.ui.dialog.ProgressDialog;
import de.greenrobot.dao.query.QueryBuilder;

/**
 * 保存数据库 Created by Administrator on 2015/4/19.
 */
public class TraktDB {
    
    // 接受添加一个剧的更新广播（添加一个新剧）
    public static final String ACTION_ADD_NEW_SHOW = "BOARD_UPDATE_ADD_NEW_SHOW";
    
    // 更新单个剧的广播（剧的季集集合）
    public static final String ACTION_ADD_NEW_SHOW_DETAIL = "BOARD_UPDATE_ADD_NEW_SHOW_DETAIL";
    
    // 接受获取所有剧集的广播
    public static final String ACTION_ALL_SHOWS_DETAILS = "BOARD_UPDATE_ALL_SHOWS_DETAILS";
    
    // 获取单个Item更新的广播
    public static final String ACTION_WATCHLIST_ITEM_SHOW = "BOARD_UPDATE_WATCHLIST_ITEM_SHOW";
    
    // 获取所有本地Season的广播
    public static final String ACTION_ALL_SEASONS = "BOARD_UPDATE_ALL_SEASONS";
    
    // 获取一季所有本地Episode的广播
    public static final String ACTION_SEASON_ALL_EPISODES = "BOARD_UPDATE_SEASON_ALL_EPISODES";
    
    // 下一集的广播
    public static final String ACTION_NEXT_SEASON_EPISODE = "BOARD_UPDATE_NEXT_SEASON_EPISODE";
    
    // 标记一季所有集已看
    public static final String ACTION_WATCHED_EPISODES = "BOARD_UPDATE_WATCHED_EPISODES";
    
    // 做单利模式
    private static TraktDB mTraktDB;
    
    // 上下文
    private Context mContext;
    
    // 剧集Dao
    private ShowDao mShowDao;
    
    // 季Dao
    private SeasonDao mSeasonDao;
    
    // 集Dao
    private EpisodeDao mEpisodeDao;
    
    // 创建执行线程
    private TaskPool mPool;
    
    private TraktDB(Context context, DaoSession session) {
        mContext = context;
        if (session != null) {
            mShowDao = session.getShowDao();
            mSeasonDao = session.getSeasonDao();
            mEpisodeDao = session.getEpisodeDao();
        }
        mPool = TaskPool.getInstance();
        // 显示日志
        if (BuildConfig.DEBUG) {
            QueryBuilder.LOG_SQL = true;
            QueryBuilder.LOG_VALUES = true;
        }
    }
    
    /**
     * 获取单例的TraktDB
     *
     * @param context
     *            上下文
     * @param session
     *            会话
     * @return 操作访问和数据库的对象
     */
    public static TraktDB getInstance(Context context, DaoSession session) {
        if (mTraktDB == null)
            mTraktDB = new TraktDB(context, session);
        return mTraktDB;
    }
    
    /********** 插入 **********/
    
    /**
     * 添加新的Show信息到数据库中（主要添加季的图片和详情，集的简单信息）
     *
     * @param response
     *            json数据
     * @param show
     *            当前剧集
     * @param dialog
     *            进度框
     */
    public void insertShowDB(final String response,
                             final Show show,
                             final ProgressDialog dialog) {
        // 获取剧名
        final String showName = Strings.title(show);
        // 获取剧集成功
        TaskItem item = new TaskItem();
        item.listener = new TaskListener() {
            
            boolean flag = false;
            
            @SuppressWarnings("unchecked")
            @Override
            public void get() {
                // 获取JSON返回的季集合
                HashMap<String, ArrayList> jsonMap = TraktJson.jsonSeaEpi(response,
                                                                          show.getTrakt());
                try {
                    // 保存数据库
                    mShowDao.insertOrReplaceInTx(show);
                    mSeasonDao.insertOrReplaceInTx(jsonMap.get("seasons"));
                    mEpisodeDao.insertOrReplaceInTx(jsonMap.get("episodes"));
                    flag = true;
                }
                catch (Exception e) {
                    mShowDao.delete(show);
                    mSeasonDao.deleteInTx(jsonMap.get("seasons"));
                    mEpisodeDao.deleteInTx(jsonMap.get("episodes"));
                    flag = false;
                }
            }
            
            @Override
            public void update() {
                if (flag) {
                    Tips(String.format("已添加 %s 到我的剧集", showName));
                    if (dialog.isShowing())
                        dialog.dismiss();
                    // 发送新加剧的广播
                    Intent intent = new Intent();
                    intent.setAction(ACTION_ADD_NEW_SHOW);
                    intent.putExtra("Show", show);
                    // 发送广播更新列表
                    mContext.sendBroadcast(intent);
                }
                else
                    Tips(String.format("%s 保存本地失败，请检查网络后重新添加", showName));
            }
        };
        // 执行线程
        mPool.execute(item);
    }
    
    /********** 更新 **********/
    
    /**
     * 更新单个季
     * 
     * @param season
     *            单个季
     */
    public void updateSeason(final Season season) {
        mSeasonDao.update(season);
    }
    
    /**
     * 更新单个集
     * 
     * @param episode
     *            集
     */
    public void updateEpisode(final Episode episode) {
        mEpisodeDao.update(episode);
    }
    
    /**
     * 更新当前季所有集
     * 
     * @param season
     *            当前季
     * @param isWatch
     *            true 标记全已看 false 全未看
     */
    public void updateEpisodes(final boolean isWatch, final Season season) {
        // 获取剧集成功
        TaskItem item = new TaskItem();
        item.listener = new TaskListener() {
            @SuppressWarnings("unchecked")
            @Override
            public void get() {
                ArrayList<Episode> episodes = new ArrayList<>();
                // 查询所有已看或者未看的集
                episodes.addAll(queryWatchEpisodes(season.getTrakt(), !isWatch));
                // 循环标记
                for (Episode episode : episodes)
                    episode.setIsWatch(isWatch);
                // 更新数据库
                mEpisodeDao.updateInTx(episodes);
            }
            
            @Override
            public void update() {
                Intent intent = new Intent();
                intent.putExtra("Season", season);
                intent.setAction(ACTION_WATCHED_EPISODES);
                mContext.sendBroadcast(intent);
            }
        };
        mPool.execute(item);
    }
    
    /********** 查询 **********/
    
    /**
     * 跟剧分类ID查询Show 获取所有未看剧集
     *
     * @param category_id
     *            分类ID
     */
    public void queryShows(long category_id) {
        final ArrayList<Show> shows = new ArrayList<>();
        final ArrayList<Season> seasons = new ArrayList<>();
        final ArrayList<Episode> episodes = new ArrayList<>();
        // 获取剧集成功
        TaskItem item = new TaskItem();
        item.listener = new TaskListener() {
            @SuppressWarnings("unchecked")
            @Override
            public void get() {
                // TODO 查询所有剧
                shows.addAll(mShowDao.loadAll());
                // 查询当前Show的未看的季
                for (Show show : shows) {
                    // 获取所有未看季的集合，并按number排序，并获取第一个
                    Season season = queryWatchSeason(show.getTrakt());
                    // 获取最小一个
                    if (season == null) {
                        seasons.add(null);
                        episodes.add(null);
                    }
                    else {
                        // 获取最小未看季
                        seasons.add(season);
                        // 获取当前未看季所有未看集集合，并按number排序
                        Episode episode = queryWatchEpisode(season.getTrakt());
                        // 获取最小一个
                        if (episode == null)
                            episodes.add(null);
                        else
                            episodes.add(episode);
                    }
                }
            }
            
            @Override
            public void update() {
                // 发送新加剧的广播
                Intent intent = new Intent();
                intent.setAction(ACTION_ALL_SHOWS_DETAILS);
                intent.putExtra("Shows", shows);
                intent.putExtra("Seasons", seasons);
                intent.putExtra("Episodes", episodes);
                // 发送广播更新列表
                mContext.sendBroadcast(intent);
            }
        };
        mPool.execute(item);
    }
    
    /**
     * 根据Show查询 查询当前未看到的季集
     *
     * @param show
     *            查询的Show
     */
    public void queryShow(final Show show) {
        // 创建广播意图
        final Intent intent = new Intent();
        intent.putExtra("Show", show);
        // 获取剧集成功
        TaskItem item = new TaskItem();
        item.listener = new TaskListener() {
            @SuppressWarnings("unchecked")
            @Override
            public void get() {
                Season season = queryWatchSeason(show.getTrakt());
                // 获取最小一个
                if (season != null) {
                    intent.putExtra("Season", season);
                    intent.putExtra("Episode",
                                    queryWatchEpisode(season.getTrakt()));
                }
            }
            
            @Override
            public void update() {
                // 发送新加剧的广播
                intent.setAction(ACTION_ADD_NEW_SHOW_DETAIL);
                // 发送广播更新列表
                mContext.sendBroadcast(intent);
            }
        };
        mPool.execute(item);
    }
    
    /**
     * 查询所有的Seasons select * from SEASON where show_id = ?
     *
     * @param show_id
     *            剧集ID
     */
    public void querySeasons(final long show_id) {
        final ArrayList<Season> seasons = new ArrayList<>();
        // 获取剧集成功
        TaskItem item = new TaskItem();
        item.listener = new TaskListener() {
            @Override
            public void get() {
                // 查询所有季
                seasons.addAll(mSeasonDao.queryBuilder()
                                         .where(SeasonDao.Properties.Show_id.eq(show_id))
                                         .orderAsc(SeasonDao.Properties.Number)
                                         .list());
            }
            
            @Override
            public void update() {
                Intent intent = new Intent();
                intent.putExtra("Seasons", seasons);
                intent.setAction(ACTION_ALL_SEASONS);
                mContext.sendBroadcast(intent);
            }
        };
        mPool.execute(item);
    }
    
    /**
     * 查询所有的Seasons select * from EPISODE where season_id = ?
     *
     * @param season_id
     *            剧集ID
     */
    public void queryEpisodes(final long season_id) {
        // 集的集合
        final ArrayList<Episode> episodes = new ArrayList<>();
        // 获取剧集成功
        TaskItem item = new TaskItem();
        item.listener = new TaskListener() {
            @Override
            public void get() {
                episodes.addAll(mEpisodeDao.queryBuilder()
                                           .where(EpisodeDao.Properties.Season_id.eq(season_id))
                                           .orderAsc(EpisodeDao.Properties.Number)
                                           .list());
            }
            
            @Override
            public void update() {
                Intent intent = new Intent();
                intent.putExtra("Episodes", episodes);
                intent.setAction(ACTION_SEASON_ALL_EPISODES);
                mContext.sendBroadcast(intent);
            }
        };
        mPool.execute(item);
    }
    
    /**
     * 寻找下一集(未做过Season标记)
     * 
     * @param season
     *            当前季
     * @param episode
     *            当前集，找下一集
     */
    public void queryNext(final Season season, final Episode episode) {
        // 传播Intent
        final Intent intent = new Intent();
        TaskItem item = new TaskItem();
        item.listener = new TaskListener() {
            @Override
            public void get() {
                if (episode != null) {
                    // 设置已看
                    episode.setIsWatch(true);
                    // 更新本集为已看
                    updateEpisode(episode);
                }
                // 获取季
                Season newSeason;
                // 获取当前季
                if (season == null)
                    newSeason = mSeasonDao.load(episode != null ? episode.getSeason_id() : -1);
                else
                    newSeason = season;
                // 特殊情况，特别季没有集数据
                if (episode == null && newSeason.getNumber() == 0)
                    // 标记当前季已看
                    newSeason.setIsWatch(true);
                else {
                    // 已看完的集
                    int watched_episodes = newSeason.getWatched_episode() + 1;
                    // 添加一集已看
                    newSeason.setWatched_episode(watched_episodes);
                    // 判断当季是否已看
                    if (watched_episodes == newSeason.getEpisode_count())
                        newSeason.setIsWatch(true);
                }
                // 更新当前季
                updateSeason(newSeason);
                
                // 判断是否需要新季
                boolean flag = newSeason.getIsWatch();
                // 当前季已看
                if (flag)
                    // 获取最新的季
                    newSeason = queryWatchSeason(episode == null ? newSeason.getShow_id()
                                                                : episode.getShow_id());
                intent.putExtra("Season", newSeason);
                // 获取最新的集
                Episode newEpisode = null;
                if (newSeason != null)
                    newEpisode = queryWatchEpisode(newSeason.getTrakt());
                // 传入集
                intent.putExtra("Episode", newEpisode);
            }
            
            @Override
            public void update() {
                // 更新Item和Episode
                intent.setAction(Action.ACTION_UPDATE_WATCHLIST);
                mContext.sendBroadcast(intent);
            }
        };
        mPool.execute(item);
    }
    
    /**
     * 寻找当前剧的最新季集(已做过Season标记)
     *
     * @param show_id
     *            查询的剧
     */
    public void queryNext(final long show_id) {
        // 传播Intent
        final Intent intent = new Intent();
        TaskItem item = new TaskItem();
        item.listener = new TaskListener() {
            @Override
            public void get() {
                // 获取最新的季
                Season newSeason = queryWatchSeason(show_id);
                intent.putExtra("Season", newSeason);
                Episode newEpisode = null;
                // 获取最新的集
                if (newSeason != null)
                    newEpisode = queryWatchEpisode(newSeason.getTrakt());
                // 传入集
                intent.putExtra("Episode", newEpisode);
            }
            
            @Override
            public void update() {
                intent.setAction(Action.ACTION_UPDATE_WATCHLIST);
                mContext.sendBroadcast(intent);
            }
        };
        mPool.execute(item);
    }
    
    /**
     * 获取未看下一季 select * from SEASON where show_id = ? and isWatch = 0 order by
     * number = ?
     * 
     * @param show_id
     *            剧ID
     * @return 下一季
     */
    private Season queryWatchSeason(long show_id) {
        // 查询当前未看的Season
        List<Season> seasonList = queryWatchSeasons(show_id, false);
        if (Lists.isEmpty(seasonList))
            return null;
        else
            return seasonList.get(0);
    }
    
    /**
     * 获取未看下一集 select * from EPISODE where season_id = ? and isWatch = 0 order
     * by number = ?
     *
     * @param season_id
     *            季ID
     * @return 下一集
     */
    private Episode queryWatchEpisode(long season_id) {
        // 查询当前Season未看集
        List<Episode> episodeList = queryWatchEpisodes(season_id, false);
        // 获取最小一个
        if (Lists.isEmpty(episodeList))
            return null;
        else
            return episodeList.get(0);
    }
    
    /**
     * 获取所有未看的不同剧的不同季
     * 
     * @param show_id
     *            季ID
     * @param isWatch
     *            true 查询已看 false 查询未看
     * @return 所有未看的Season
     */
    private List<Season> queryWatchSeasons(long show_id, boolean isWatch) {
        int watch = 0;
        if (isWatch)
            watch = 1;
        return mSeasonDao.queryBuilder()
                         .where(SeasonDao.Properties.Show_id.eq(show_id),
                                SeasonDao.Properties.IsWatch.eq(watch))
                         .orderAsc(SeasonDao.Properties.Number)
                         .list();
    }
    
    /**
     * 获取所有未看不同季的不同集
     * 
     * @param season_id
     *            季ID
     * @param isWatch
     *            true 查询已看 false 查询未看
     * @return 所有未看集的集合
     */
    private List<Episode> queryWatchEpisodes(long season_id, boolean isWatch) {
        int watch = 0;
        if (isWatch)
            watch = 1;
        return mEpisodeDao.queryBuilder()
                          .where(EpisodeDao.Properties.Season_id.eq(season_id),
                                 EpisodeDao.Properties.IsWatch.eq(watch))
                          .orderAsc(EpisodeDao.Properties.Number)
                          .list();
    }
    
    /**
     * 提示消息
     * 
     * @param msg
     *            消息
     */
    private void Tips(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }
}
