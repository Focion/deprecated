package cn.focion.tv.tool.board;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * 广播 Created by Pirate on 2015/4/21.
 */
public class UpdateBoard extends BroadcastReceiver {
    
    private ReceiveBoard mReceiveBoard;
    
    public UpdateBoard(ReceiveBoard receiveBoard) {
        mReceiveBoard = receiveBoard;
    }
    
    @Override
    public void onReceive(Context context, Intent intent) {
        mReceiveBoard.onReceive(intent);
    }
    
    /**
     * 接受广播的接口
     */
    public interface ReceiveBoard {
        void onReceive(Intent intent);
    }
}
