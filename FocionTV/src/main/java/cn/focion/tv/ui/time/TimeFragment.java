package cn.focion.tv.ui.time;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cn.focion.tv.ui.FocionFragment;

/**
 * 时间Fragment Created by Pirate on 2015-05-15.
 */
public class TimeFragment extends FocionFragment<TimeActivity> {
    
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }
    
    @Override
    public void trySetupData() {
        
    }
}
