package cn.focion.tv.ui.follow;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.graphics.Palette;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.focion.tv.Constants;
import cn.focion.tv.R;
import cn.focion.tv.model.Show;
import cn.focion.tv.temp.TShow;
import cn.focion.tv.tool.connect.Response;
import cn.focion.tv.tool.error.VolleyError;
import cn.focion.tv.tool.trakt.TraktJson;
import cn.focion.tv.tool.util.Dates;
import cn.focion.tv.tool.util.Images;
import cn.focion.tv.tool.util.Strings;
import cn.focion.tv.tool.util.UIs;
import cn.focion.tv.ui.FocionFragment;
import cn.focion.tv.ui.IOC;
import cn.focion.tv.ui.dialog.ProgressDialog;
import cn.focion.tv.widget.CircleButton;
import cn.focion.tv.widget.ObsScrollView;

import com.facebook.drawee.view.SimpleDraweeView;

/**
 * 详情 Created by Pirate on 2015/4/8.
 */
public class DetailFragment extends FocionFragment<DetailActivity> implements
                                                                  ObsScrollView.Callbacks,
                                                                  View.OnClickListener,
                                                                  Palette.PaletteAsyncListener {
    
    @IOC(id = R.id.detail_scroll)
    private ObsScrollView mScrollView;
    
    @IOC(id = R.id.detail_show_fanart)
    private SimpleDraweeView mImageFanart;
    
    @IOC(id = R.id.detail_show_poster)
    private SimpleDraweeView mImagePoster;
    
    @IOC(id = R.id.detail_show_name)
    private TextView showName;
    
    @IOC(id = R.id.detail_show_start_time)
    private TextView showStart;
    
    @IOC(id = R.id.detail_add)
    private CircleButton showAdd;
    
    @IOC(id = R.id.detail_show_rating)
    private TextView showRating;
    
    @IOC(id = R.id.detail_show_overview_en)
    private TextView showOverview;
    
    @IOC(id = R.id.detail_layout)
    private LinearLayout mDetailLayout;
    
    @IOC(id = R.id.detail_show_overview_zh)
    private TextView showOverview_zh;
    
    @IOC(id = R.id.detail_show_name_zh)
    private TextView showName_zh;
    
    @IOC(id = R.id.detail_show_watchers)
    private TextView showWatchers;
    
    @IOC(id = R.id.detail_show_country)
    private TextView showCountry;
    
    @IOC(id = R.id.detail_show_status)
    private TextView showStatus;
    
    @IOC(id = R.id.detail_show_airtime)
    private TextView showAirtime;
    
    private int defaultY = 0;
    
    // 当前显示的Show
    private Show show;
    
    private TraktJson traktJson;
    
    // 加载完成标记
    private boolean detail = false, watchers = false, overview = false;
    
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        setContentView(R.layout.layout_follow_detail);
        return super.onCreateView(inflater, container, savedInstanceState);
    }
    
    @Override
    public void trySetupData() {
        traktJson = TraktJson.getInstance(mActivity);
        final TShow tShow = (TShow) mActivity.getIntent()
                                             .getSerializableExtra("TShow");
        // 获取单个剧的数据
        mFocion.traktGet.singleShow(tShow.ids_trakt,
                                    true,
                                    true,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            detail = true;
                                            // 解析Json数据
                                            traktJson.jsonShow(response, tShow);
                                            // 显示界面
                                            open();
                                        }
                                    },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            Tips(error.getMessage());
                                            setContentShown(true);
                                        }
                                    });
        // 注册监听
        addAction(TraktJson.ACTION_DETAIL_SHOW,
                  TraktJson.ACTION_DETAIL_SHOW_TRANS);
        showAdd.setOnClickListener(this);
        mScrollView.addCallbacks(this);
        // 初始化值
        defaultY = UIs.dipToPx(mActivity, 184);
    }
    
    @Override
    public void onScrollChanged(int scrollX, int scrollY) {
        float alpha;
        // 判断
        if (scrollY < defaultY) {
            alpha = (float) scrollY / defaultY;
            // 设置阴影
            mActivity.setShadowShow(false);
            // 设置图片的高度
            mImageFanart.setTop(-scrollY / 2);
        }
        else {
            alpha = 1;
            // 设置阴影
            mActivity.setShadowShow(true);
        }
        
        // 计算透明度
        mActivity.setBackgroundColor(UIs.colorAlpha(Constants.TITLE_COLOR,
                                                    alpha));
        // 计算字体透明度
        mActivity.setTextColor(UIs.colorAlpha(Color.WHITE, alpha));
    }
    
    @Override
    public void onClick(View v) {
        if (v.getId() != R.id.detail_add)
            return;
        // 判断是否数据加载成功
        if (!detail || !watchers || !overview) {
            Tips("此剧集信息不完整，无法添加，请到检查网络环境后重试");
            return;
        }
        
        // 判断是否重复添加
        Show addShow = mFocion.session.getShowDao().load(show.getTrakt());
        if (addShow != null) {
            Tips("此剧集已在列表中，无需重复添加");
            return;
        }
        final ProgressDialog dialog = new ProgressDialog(mActivity,
                                                         Strings.title(show));
        dialog.show();
        // 获取此剧的季和集
        mFocion.traktGet.seasons(show.getTrakt(),
                                 true,
                                 true,
                                 true,
                                 new Response.Listener<String>() {
                                     @Override
                                     public void onResponse(final String response) {
                                         // 添加剧集到本地数据库
                                         mFocion.traktDB.insertShowDB(response,
                                                                      show,
                                                                      dialog);
                                     }
                                 },
                                 new Response.ErrorListener() {
                                     @Override
                                     public void onErrorResponse(VolleyError error) {
                                         Tips(String.format("添加 %s 失败，请检查网络并重试......",
                                                            Strings.title(show)));
                                     }
                                 });
    }
    
    @Override
    public void onGenerated(Palette palette) {
        mDetailLayout.setBackgroundColor(UIs.rgbDark(palette));
    }
    
    @Override
    public void onReceive(Intent intent) {
        switch (intent.getAction()) {
            case TraktJson.ACTION_DETAIL_SHOW:
                show = (Show) intent.getSerializableExtra("Show");
                // 设置Title
                showName.setText(show.getTitle());
                // 设置ToolBar的Title
                mActivity.setText(show.getTitle());
                mActivity.setTextColor(Color.TRANSPARENT);
                // 图片及颜色
                Images.load(show.getFanart(),
                            mImageFanart,
                            new Images.ImageLoadListener() {
                                @Override
                                public void loadBitmap(Bitmap bitmap) {
                                    Palette.generateAsync(bitmap,
                                                          DetailFragment.this);
                                }
                            });
                // 设置简介
                showOverview.setText(Strings.space(show.getOverview()));
                // 设置图片
                Images.load(show.getPoster(), mImagePoster);
                // 设置首播时间
                showStart.setText(Dates.changeZone(show.getTimezone(),
                                                   show.getFirst_aired(),
                                                   Dates.YYYY_MM_DD));
                // 设置评分
                showRating.setText(String.format("评分：%.1f 分", show.getRating()));
                // 设置剧集状态
                showStatus.setText(show.getStatus_zh());
                // 播出电台
                String network = show.getNetwork();
                if (Strings.isEmpty(network))
                    network = "未知";
                // 设置电台 国家
                showCountry.setText(String.format("%s / %s",
                                                  network,
                                                  show.getCountry_zh()));
                // 设置播出时长
                String format = "%s  %s  ,  %d 分钟 / 集";
                if (Strings.isEmpty(show.getDay()) || Strings.isEmpty(show.getTime()))
                    format = "%s%s%d 分钟 / 集";
                showAirtime.setText(String.format(format,
                                                  show.getDay(),
                                                  show.getTime(),
                                                  show.getRuntime()));
                // 获取Show的ID
                long id = show.getTrakt();
                // 获取关注人数
                mFocion.traktGet.watchersShow(id,
                                              new Response.Listener<String>() {
                                                  @Override
                                                  public void onResponse(String response) {
                                                      watchers = true;
                                                      traktJson.jsonWatchers(response,
                                                                             show);
                                                      showWatchers.setText(String.format("关注人数：%,d 人",
                                                                                         show.getWatchers()));
                                                      // 显示界面
                                                      open();
                                                  }
                                              },
                                              new Response.ErrorListener() {
                                                  @Override
                                                  public void onErrorResponse(VolleyError error) {
                                                      Tips(error.errorMsg());
                                                      setContentShown(true);
                                                  }
                                              });
                // 设置默认显示
                if (!show.getAvailable_translations().contains("zh")) {
                    // 标记中文简介为true
                    overview = true;
                    showOverview_zh.setText(Strings.space("暂无中文简介"));
                    showName_zh.setText("暂无中文名");
                    show.setOverview_zh("暂无中文简介");
                    show.setTitle_zh("暂无中文名");
                    // 显示界面
                    open();
                    return;
                }
                // 获取翻译数据
                mFocion.traktGet.translationsShow(id,
                                                  new Response.Listener<String>() {
                                                      @Override
                                                      public void onResponse(String response) {
                                                          overview = true;
                                                          traktJson.jsonTranslations(response,
                                                                                     show);
                                                          // 显示界面
                                                          open();
                                                      }
                                                  },
                                                  new Response.ErrorListener() {
                                                      @Override
                                                      public void onErrorResponse(VolleyError error) {
                                                          Tips(error.errorMsg());
                                                          setContentShown(true);
                                                      }
                                                  });
                break;
            case TraktJson.ACTION_DETAIL_SHOW_TRANS:
                showName_zh.setText(show.getTitle_zh());
                showOverview_zh.setText(Strings.space(show.getOverview_zh()));
                // 显示界面
                open();
                break;
        }
    }
    
    /**
     * 显示界面的方法
     */
    private void open() {
        if (detail && overview && watchers)
            setContentShown(true);
    }
}
