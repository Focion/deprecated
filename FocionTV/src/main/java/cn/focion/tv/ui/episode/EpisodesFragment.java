package cn.focion.tv.ui.episode;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cn.focion.tv.R;
import cn.focion.tv.adapter.episode.EpisodesAdapter;
import cn.focion.tv.model.Episode;
import cn.focion.tv.model.Season;
import cn.focion.tv.tool.trakt.TraktDB;
import cn.focion.tv.tool.util.UIs;
import cn.focion.tv.ui.FocionFragment;
import cn.focion.tv.ui.IOC;
import cn.focion.tv.widget.RecyclerView;

/**
 * 集列表 Created by Pirate on 2015-05-09.
 */
public class EpisodesFragment extends FocionFragment<EpisodesActivity> implements
                                                                      RecyclerView.ItemClickListener {
    
    @IOC(id = R.id.focion_hide_bar_recycler)
    private RecyclerView mRecyclerView;
    
    private EpisodesAdapter mAdapter;
    
    private ArrayList<Episode> episodes;
    
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        setContentView(R.layout.focion_hide_bar_list);
        return super.onCreateView(inflater, container, savedInstanceState);
    }
    
    @Override
    public void trySetupData() {
        // 滑动隐藏
        mActivity.autoHideBar(mRecyclerView);
        Bundle bundle = getArguments();
        // 获取参数
        String timezone = bundle.getString("timezone");
        Season season = (Season) bundle.getSerializable("Season");
        // 注册监听
        addAction(TraktDB.ACTION_SEASON_ALL_EPISODES);
        // 设置高度
        mRecyclerView.setLayoutManager(mLayoutManager);
        // 设置背景
        mRecyclerView.setBackgroundResource(R.color.white_clr);
        // 初始化参数
        episodes = new ArrayList<>();
        // 设置适配器
        mAdapter = new EpisodesAdapter(mActivity,
                                       timezone,
                                       R.layout.layout_episodes_item,
                                       episodes);
        mAdapter.setItemClickListener(this);
        mRecyclerView.setAdapter(mAdapter);
        // 查询此季的所有集
        mFocion.traktDB.queryEpisodes(season.getTrakt());
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public void onReceive(Intent intent) {
        switch (intent.getAction()) {
            case TraktDB.ACTION_SEASON_ALL_EPISODES:
                episodes.addAll((ArrayList<Episode>) intent.getSerializableExtra("Episodes"));
                mAdapter.notifyDataSetChanged();
                setContentShown(true);
                break;
        }
    }
    
    @Override
    public void setFragmentTopClearance() {
        mRecyclerView.setContentTopClearance(UIs.dipToPx(mActivity, 56));
    }
    
    /**
     * RecyclerView的滚动
     */
    public boolean canSwipeRefreshChildScrollUp() {
        return ViewCompat.canScrollVertically(mRecyclerView, -1);
    }
    
    @Override
    public void onRecyclerItemClick(View v, int position) {
        
    }
}
