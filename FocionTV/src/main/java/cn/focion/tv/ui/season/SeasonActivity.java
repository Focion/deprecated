package cn.focion.tv.ui.season;

//
// import android.graphics.Color;
// import android.os.Bundle;
// import android.support.v4.widget.SwipeRefreshLayout;
// import android.support.v7.widget.Toolbar;
//
// import cn.focion.tv.R;
// import cn.focion.tv.model.Season;
// import cn.focion.tv.tool.util.Strings;
// import cn.focion.tv.tool.util.UIs;
// import cn.focion.tv.ui.FocionActivity;
// import cn.focion.tv.ui.IOC;
// import cn.focion.tv.widget.RefreshLayout;
// import cn.focion.tv.widget.ShadowFrame;
//
// /**
// * 季的Activity Created by Pirate on 2015/4/22.
// */
// public class SeasonActivity extends FocionActivity implements
// SwipeRefreshLayout.OnRefreshListener,
// RefreshLayout.CanChildScrollUpCallback {
//
// @IOC(id = R.id.focion_toolbar)
// private Toolbar mToolBar;
//
// @IOC(id = R.id.focion_content)
// private ShadowFrame mShadowFrame;
//
// @IOC(id = R.id.focion_refresh)
// private RefreshLayout mRefresh;
//
// // 是否显示阴影
// boolean isShadowShow = false;
//
// // 当前点击的季
// private Season season;
//
// private SeasonFragment mSeasonFragment;
//
// @Override
// protected void onCreate(Bundle savedInstanceState) {
// super.onCreate(savedInstanceState);
// setContentView(R.layout.focion_toolbar_refresh);
// // 设置ToolBar
// trySetupToolBar();
// // 初始化Fragment
// trySetupFragment();
// // 初始化刷新
// trySetupRefresh();
// }
//
// /**
// * 设置ToolBar
// */
// private void trySetupToolBar() {
// season = (Season) getIntent().getSerializableExtra("Season");
// // 设置Title和颜色
// mToolBar.setTitle("");
// mToolBar.setBackgroundColor(Color.parseColor("#00000000"));
// // 设置返回键
// setSupportActionBar(mToolBar);
// getSupportActionBar().setDisplayHomeAsUpEnabled(true);
// }
//
// /**
// * 设置Fragment和阴影
// */
// private void trySetupFragment() {
// // 设置阴影FramLayout的高度
// mShadowFrame.setShadowTopOffset(UIs.dipToPx(this, 56));
// // 设置阴影显示
// mShadowFrame.setShadowVisible(isShadowShow, isShadowShow);
// // 替换Fragment
// mSeasonFragment = new SeasonFragment();
// Bundle bundle = new Bundle();
// bundle.putSerializable("Season", season);
// bundle.putSerializable("Timezone",
// getIntent().getStringExtra("Timezone"));
// mSeasonFragment.setArguments(bundle);
// // 设置侧滑栏的布局Fragment
// mFragmentManager.beginTransaction()
// .replace(R.id.focion_frame, mSeasonFragment)
// .commit();
// }
//
// /**
// * 初始化下拉刷新
// */
// private void trySetupRefresh() {
// // 设置颜色
// mRefresh.setColorSchemeResources(R.color.refresh_progress_1,
// R.color.refresh_progress_2,
// R.color.refresh_progress_3);
// // 设置监听
// mRefresh.setCanChildScrollUpCallback(this);
// // 设置下拉监听
// mRefresh.setOnRefreshListener(this);
// if (mRefresh == null)
// return;
// // 顶部距离
// mRefresh.setProgressViewOffset(true,
// -UIs.dipToPx(this, 32),
// UIs.dipToPx(this, 16));
// }
//
// @Override
// public ShadowFrame shadowFrame() {
// return mShadowFrame;
// }
//
// @Override
// public void onRefresh() {
//
// }
//
// @Override
// public boolean canSwipeRefreshChildScrollUp() {
// return mSeasonFragment != null &&
// mSeasonFragment.canSwipeRefreshChildScrollUp();
// }
//
// /**
// * 设置阴影高度
// *
// * @param isShow
// * true 显示 false 不显示
// */
// public void setShadowShow(boolean isShow) {
// if (isShadowShow != isShow) {
// // 设置阴影FramLayout的显示
// mShadowFrame.setShadowVisible(isShow, isShow);
// isShadowShow = isShow;
// }
// }
// }
