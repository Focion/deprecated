package cn.focion.tv.ui.dialog;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import cn.focion.tv.R;
import cn.focion.tv.ui.IOC;

/**
 * 进度文字 Created by Administrator on 2015/4/19.
 */
public class ProgressDialog extends FocionDialog {
    
    @IOC(id = R.id.progress_showName)
    private TextView mShowName;
    
    @IOC(id = R.id.dialog_sure)
    private TextView mSure;
    
    @IOC(id = R.id.dialog_cancel)
    private TextView mCancel;
    
    // 要显示的文字
    private String title;
    
    public ProgressDialog(Context context, String title) {
        super(context);
        this.title = title;
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_progress);
        mShowName.setText(title);
        mSure.setVisibility(View.GONE);
        mCancel.setText("后台运行");
        mCancel.setOnClickListener(this);
    }
    
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.dialog_cancel)
            this.dismiss();
    }
}
