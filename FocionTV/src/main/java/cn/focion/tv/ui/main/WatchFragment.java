package cn.focion.tv.ui.main;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cn.focion.tv.R;
import cn.focion.tv.adapter.main.WatchAdapter;
import cn.focion.tv.model.Episode;
import cn.focion.tv.model.Season;
import cn.focion.tv.model.Show;
import cn.focion.tv.tool.trakt.Action;
import cn.focion.tv.tool.trakt.TraktDB;
import cn.focion.tv.tool.util.UIs;
import cn.focion.tv.ui.FocionFragment;
import cn.focion.tv.ui.IOC;
import cn.focion.tv.widget.RecyclerView;

/**
 * 观看列表 Created by Pirate on 2015/3/28.
 */
public class WatchFragment extends FocionFragment<MainActivity> {
    
    @IOC(id = R.id.focion_hide_bar_recycler)
    private RecyclerView mRecyclerView;
    
    // 适配器
    private WatchAdapter mAdapter;
    
    // 显示的剧
    private ArrayList<Show> shows;
    
    // 显示的季
    private ArrayList<Season> seasons;
    
    // 显示的集
    private ArrayList<Episode> episodes;
    
    // 点击的角标
    private int clickPosition = -1;
    
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        setContentView(R.layout.focion_hide_bar_list);
        return super.onCreateView(inflater, container, savedInstanceState);
    }
    
    @Override
    public void trySetupData() {
        // 滑动隐藏
        mActivity.autoHideBar(mRecyclerView);
        // 设置高度
        mRecyclerView.setLayoutManager(mLayoutManager);
        // 初始化数据
        shows = new ArrayList<>();
        seasons = new ArrayList<>();
        episodes = new ArrayList<>();
        // 初始化适配器
        mAdapter = new WatchAdapter(this,
                                    R.layout.layout_main_watch_item,
                                    shows,
                                    seasons,
                                    episodes);
        // 设置适配器
        mRecyclerView.setAdapter(mAdapter);
        // 注册监听
        addAction(TraktDB.ACTION_ALL_SHOWS_DETAILS,
                  TraktDB.ACTION_ADD_NEW_SHOW,
                  TraktDB.ACTION_ADD_NEW_SHOW_DETAIL,
                  TraktDB.ACTION_WATCHLIST_ITEM_SHOW,
                  Action.ACTION_EPISODE_TO_WATCHLIST,
                  Action.ACTION_UPDATE_WATCHLIST);
        // 执行数据操作
        mFocion.traktDB.queryShows(0);
    }
    
    @Override
    public void setFragmentTopClearance() {
        mRecyclerView.setContentTopClearance(UIs.dipToPx(mActivity, 56));
    }
    
    /**
     * RecyclerView的滚动
     */
    public boolean canSwipeRefreshChildScrollUp() {
        return ViewCompat.canScrollVertically(mRecyclerView, -1);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public void onReceive(Intent intent) {
        switch (intent.getAction()) {
            case TraktDB.ACTION_ALL_SHOWS_DETAILS:
                // 清除原有数据
                shows.clear();
                seasons.clear();
                episodes.clear();
                // 装载新数据
                shows.addAll((ArrayList<Show>) intent.getSerializableExtra("Shows"));
                seasons.addAll((ArrayList<Season>) intent.getSerializableExtra("Seasons"));
                episodes.addAll((ArrayList<Episode>) intent.getSerializableExtra("Episodes"));
                // 通知Adapter更新
                mAdapter.notifyDataSetChanged();
                // 显示界面
                setContentShown(true);
                break;
            case TraktDB.ACTION_ADD_NEW_SHOW:
                Show show = (Show) intent.getSerializableExtra("Show");
                // 查询当前看到的季集
                mFocion.traktDB.queryShow(show);
                break;
            case TraktDB.ACTION_ADD_NEW_SHOW_DETAIL:
                // 获取更新数据
                Show updateShow = (Show) intent.getSerializableExtra("Show");
                Season updateSeason = (Season) intent.getSerializableExtra("Season");
                Episode updateEpisode = (Episode) intent.getSerializableExtra("Episode");
                // 添加更新数据
                shows.add(updateShow);
                seasons.add(updateSeason);
                episodes.add(updateEpisode);
                // 更新适配器
                mAdapter.notifyDataSetChanged();
                break;
            case TraktDB.ACTION_WATCHLIST_ITEM_SHOW:
                // 获取查询的数据
                Season season = (Season) intent.getSerializableExtra("Season");
                Episode episode = (Episode) intent.getSerializableExtra("Episode");
                // 防止出错
                if (clickPosition == -1)
                    break;
                // 防止空
                if (season == null || episode == null)
                    return;
                changeItem(clickPosition, season, episode);
                break;
            case Action.ACTION_EPISODE_TO_WATCHLIST:
                // 获取查询的数据
                Season seasonWatch = (Season) intent.getSerializableExtra("Season");
                Episode episodeWatch = (Episode) intent.getSerializableExtra("Episode");
                // 防止出错
                if (clickPosition == -1)
                    return;
                // 防止空
                if (seasonWatch == null || episodeWatch == null)
                    return;
                changeItem(clickPosition, seasonWatch, episodeWatch);
                break;
            case Action.ACTION_UPDATE_WATCHLIST:
                // 获取更新的参数
                Season newSeason = (Season) intent.getSerializableExtra("Season");
                Episode newEpisode = (Episode) intent.getSerializableExtra("Episode");
                // 改变Item
                changeItem(clickPosition, newSeason, newEpisode);
                break;
        }
    }
    
    /**
     * 更换Item数据
     *
     * @param position
     *            角标
     * @param season
     *            季
     * @param episode
     *            集
     */
    private void changeItem(int position, Season season, Episode episode) {
        // 替换
        seasons.remove(position);
        seasons.add(position, season);
        episodes.remove(position);
        episodes.add(position, episode);
        // 更新
        mAdapter.notifyItemChanged(position);
    }
    
    /**
     * 设置点击的角标
     * 
     * @param clickPosition
     *            角标
     */
    public void setClickPosition(int clickPosition) {
        this.clickPosition = clickPosition;
    }
}
