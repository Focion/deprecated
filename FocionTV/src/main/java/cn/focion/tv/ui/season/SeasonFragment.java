package cn.focion.tv.ui.season;

//
// import java.util.ArrayList;
//
// import android.content.Context;
// import android.content.Intent;
// import android.os.Bundle;
// import android.support.v4.view.ViewCompat;
// import android.support.v7.widget.LinearLayoutManager;
// import android.view.LayoutInflater;
// import android.view.View;
// import android.view.ViewGroup;
// import android.widget.TextView;
//
// import cn.focion.tv.R;
// import cn.focion.tv.adapter.main.SeasonAdapter;
// import cn.focion.tv.model.Episode;
// import cn.focion.tv.model.Season;
// import cn.focion.tv.tool.board.UpdateBoard;
// import cn.focion.tv.tool.trakt.TraktDB;
// import cn.focion.tv.tool.util.Images;
// import cn.focion.tv.tool.util.Lists;
// import cn.focion.tv.tool.util.Strings;
// import cn.focion.tv.tool.util.UIs;
// import cn.focion.tv.ui.FocionFragment;
// import cn.focion.tv.ui.IOC;
// import cn.focion.tv.widget.ObsScrollView;
// import cn.focion.tv.widget.RecyclerView;
//
// import com.facebook.drawee.view.SimpleDraweeView;
//
// /**
// * 季 Created by Pirate on 2015/4/22.
// */
// public class SeasonFragment extends FocionFragment implements
// UpdateBoard.ReceiveBoard,
// ObsScrollView.Callbacks {
//
// // 依赖的Activity
// private SeasonActivity mSeasonActivity;
//
// @IOC(id = R.id.season_scroll)
// private ObsScrollView mScrollView;
//
// @IOC(id = R.id.season_poster)
// private SimpleDraweeView mSeasonPoster;
//
// @IOC(id = R.id.season_fanart)
// private SimpleDraweeView mSeasonFanart;
//
// @IOC(id = R.id.season_rating)
// private TextView mRating;
//
// @IOC(id = R.id.season_count)
// private TextView mAllCount;
//
// @IOC(id = R.id.season_overview)
// private TextView mOverview;
//
// @IOC(id = R.id.season_recycler)
// private RecyclerView mRecyclerView;
//
// @IOC(id = R.id.season_none)
// private TextView mSeasonNone;
//
// // 所有集
// private ArrayList<Episode> episodes;
//
// private SeasonAdapter mAdapter;
//
// // 当前季
// private Season season;
//
// private String timezone;
//
// // 看过的数量
// private int isWatchCount = 0;
//
// @Override
// public View onCreateView(LayoutInflater inflater,
// ViewGroup container,
// Bundle savedInstanceState) {
// setContentView(R.layout.layout_season);
// return super.onCreateView(inflater, container, savedInstanceState);
// }
//
// @Override
// public void trySetupData() {
// mSeasonActivity = (SeasonActivity) mActivity;
// // 获取传过来的Season
// season = (Season) getArguments().getSerializable("Season");
// timezone = getArguments().getString("Timezone");
// // 判断此季是否存在
// if (season == null) {
// setEmptyText("暂无此季信息");
// setContentEmpty(true);
// setContentShown(true);
// return;
// }
// // 注册监听
// addAction(TraktDB.ACTION_ALL_SEASON_EPISODES);
// // 滑动监听
// mScrollView.addCallbacks(this);
// // 加载季的信息
// Images.load(season.getPoster(), mSeasonPoster);
// Images.load(season.getThumb(), mSeasonFanart);
// // 分数
// mRating.setText(String.format("%s (%.1f 分)",
// Strings.title(season),
// season.getRating()));
// // 简介
// String overview = season.getOverview();
// if (Strings.isEmpty(overview))
// overview = "No Introduction";
// mOverview.setText(Strings.space(overview));
// // 设置RecyclerView
// trySetupRecycler();
// // 加载数据
// TraktDB.getInstance(mSeasonActivity, mFocion.session)
// .queryEpisodes(season.getTrakt());
// }
//
// /**
// * 初始化列表
// */
// private void trySetupRecycler() {
// episodes = new ArrayList<>();
// // 设置新的LinearLayoutManager 控制高度
// mRecyclerView.setLayoutManager(new LinearLayoutManager(mSeasonActivity) {
// @Override
// public void onMeasure(android.support.v7.widget.RecyclerView.Recycler
// recycler,
// android.support.v7.widget.RecyclerView.State state,
// int widthSpec,
// int heightSpec) {
// // 控制RecyclerView高度
// View view = recycler.getViewForPosition(0);
// if (view != null) {
// measureChild(view, widthSpec, heightSpec);
// int measuredWidth = View.MeasureSpec.getSize(widthSpec);
// int measuredHeight = view.getMeasuredHeight();
// setMeasuredDimension(measuredWidth,
// measuredHeight * episodes.size());
// }
// }
// });
// // 设置适配器
// mAdapter = new SeasonAdapter(mSeasonActivity,
// R.layout.layout_season_item,
// timezone,
// episodes);
// mRecyclerView.setAdapter(mAdapter);
// }
//
// @SuppressWarnings("unchecked")
// @Override
// public void onReceive(Intent intent) {
// switch (intent.getAction()) {
// case TraktDB.ACTION_ALL_SEASON_EPISODES:
// episodes.addAll((ArrayList<Episode>)
// intent.getSerializableExtra("Episodes"));
// // 更新已看数量
// updateIsWatchCount();
// if (Lists.isEmpty(episodes))
// mSeasonNone.setVisibility(View.VISIBLE);
// else {
// mAdapter.notifyDataSetChanged();
// setContentShown(true);
// }
// break;
// }
// }
//
// @Override
// public void onScrollChanged(int scrollX, int scrollY) {
//
// }
//
// /**
// * RecyclerView的滚动
// */
// public boolean canSwipeRefreshChildScrollUp() {
// return ViewCompat.canScrollVertically(mScrollView, -1);
// }
//
// public void updateIsWatchCount() {
// for (Episode episode : episodes) {
// if (episode.getIsWatch())
// isWatchCount++;
// }
// mAllCount.setText(String.format("%d 集 / %d 集 / %d 集",
// isWatchCount,
// season.getAired_episodes(),
// season.getEpisode_count()));
// }
// }
