package cn.focion.tv.ui.main;

import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cn.focion.tv.Constants;
import cn.focion.tv.R;
import cn.focion.tv.adapter.main.SeasonAdapter;
import cn.focion.tv.model.Season;
import cn.focion.tv.tool.trakt.Action;
import cn.focion.tv.tool.trakt.TraktDB;
import cn.focion.tv.tool.util.Cores;
import cn.focion.tv.tool.util.UIs;
import cn.focion.tv.ui.FocionFragment;
import cn.focion.tv.ui.IOC;
import cn.focion.tv.ui.episode.EpisodesActivity;
import cn.focion.tv.widget.ObsScrollView;
import cn.focion.tv.widget.RecyclerView;

/**
 * 季 Created by Pirate on 2015/4/22.
 */
public class SeasonFragment extends FocionFragment<DetailsActivity> implements
                                                                   ObsScrollView.Callbacks,
                                                                   RecyclerView.ItemClickListener {
    
    @IOC(id = R.id.detail_season_recycler)
    private RecyclerView mRecyclerView;
    
    @IOC(id = R.id.detail_season_scroll)
    private ObsScrollView mScrollView;
    
    private int defaultY;
    
    private SeasonAdapter mAdapter;
    
    private ArrayList<Season> seasons;
    
    private int clickPosition = -1;
    
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        setContentView(R.layout.layout_main_season);
        return super.onCreateView(inflater, container, savedInstanceState);
    }
    
    @Override
    public void trySetupData() {
        // 注册广播
        addAction(TraktDB.ACTION_ALL_SEASONS,
                  TraktDB.ACTION_WATCHED_EPISODES,
                  Action.ACTION_EPISODES_TO_SEASONS);
        defaultY = UIs.dipToPx(mActivity, 203 - 56 - 48);
        seasons = new ArrayList<>();
        // 获取剧集ID
        long show_id = getArguments().getLong("show_id");
        // 设置新的LinearLayoutManager 控制高度
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity) {
            @Override
            public void onMeasure(android.support.v7.widget.RecyclerView.Recycler recycler,
                                  android.support.v7.widget.RecyclerView.State state,
                                  int widthSpec,
                                  int heightSpec) {
                // 控制RecyclerView高度
                View view = recycler.getViewForPosition(0);
                if (view != null) {
                    measureChild(view, widthSpec, heightSpec);
                    int measuredWidth = View.MeasureSpec.getSize(widthSpec);
                    int measuredHeight = view.getMeasuredHeight();
                    setMeasuredDimension(measuredWidth,
                                         measuredHeight * seasons.size());
                }
            }
        });
        // 设置适配器
        mAdapter = new SeasonAdapter(mActivity,
                                     R.layout.layout_main_season_item,
                                     seasons);
        // 设置点击监听
        mAdapter.setItemClickListener(this);
        mRecyclerView.setAdapter(mAdapter);
        // 设置滑动监听
        mScrollView.addCallbacks(this);
        // 获取所有季
        mFocion.traktDB.querySeasons(show_id);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public void onReceive(Intent intent) {
        switch (intent.getAction()) {
            case TraktDB.ACTION_ALL_SEASONS:
                seasons.addAll((ArrayList<Season>) intent.getSerializableExtra("Seasons"));
                mAdapter.notifyDataSetChanged();
                setContentShown(true);
                break;
            case TraktDB.ACTION_WATCHED_EPISODES:
                Season tickSeason = (Season) intent.getSerializableExtra("Season");
                // 做最新的查询
                mFocion.traktDB.queryNext(tickSeason.getShow_id());
                break;
            case Action.ACTION_EPISODES_TO_SEASONS:
                // 传改后的值
                boolean isWatch = intent.getBooleanExtra("isWatch", false);
                // 当前需要改变的季
                Season changeSeason = seasons.get(clickPosition);
                // 获取当前看过集数
                int watch_episode = changeSeason.getWatched_episode();
                if (isWatch)
                    // 如果标看过 +1
                    watch_episode++;
                else
                    // 如果标记未看 -1
                    watch_episode--;
                // 设置新值并更新
                changeSeason.setIsWatch(watch_episode == changeSeason.getEpisode_count());
                // 设置观看数量
                changeSeason.setWatched_episode(watch_episode);
                // 更新数据库
                mFocion.traktDB.updateSeason(changeSeason);
                // 改变Item
                changeItem(changeSeason);
                // 做最新的查询
                mFocion.traktDB.queryNext(changeSeason.getShow_id());
                break;
        }
    }
    
    @Override
    public void onScrollChanged(int scrollX, int scrollY) {
        // 透明度
        float alpha;
        // 颜色
        int color = Color.parseColor(Constants.TITLE_COLOR);
        if (scrollY < defaultY) {
            alpha = (float) scrollY / defaultY;
            // 设置阴影
            mActivity.setShadowShow(false);
            mActivity.setLayoutTop(-scrollY + UIs.dipToPx(mActivity, 203 - 48));
            // 设置颜色
            mActivity.setBackgroundColor(UIs.colorAlpha(color, 0));
        }
        else {
            alpha = 1;
            mActivity.setLayoutTop(UIs.dipToPx(mActivity, 56));
            // 设置阴影
            mActivity.setShadowShow(true);
            // 设置颜色
            mActivity.setBackgroundColor(color);
        }
        mActivity.setImageLayoutTop(-scrollY);
        // 设置背景
        mActivity.setScrollColor(UIs.colorAlpha(color, alpha));
        // 设置字体颜色
        mActivity.setToolTextColor(UIs.colorAlpha(Color.WHITE, alpha));
    }
    
    @Override
    public void onRecyclerItemClick(View v, int position) {
        // 点击的角标
        clickPosition = position;
        // 获取Season
        Season season = seasons.get(position);
        if (Cores.checkSpecial(season)) {
            Tips("此季无集的详情");
            return;
        }
        // 传参数 查看Episode列表
        Intent intent = new Intent(mActivity, EpisodesActivity.class);
        intent.putExtra("Season", season);
        intent.putExtra("title&timezone",
                        getArguments().getString("title&timezone"));
        // 设置带有返回参数的意图
        startActivity(intent);
    }
    
    /**
     * 更新Item
     * 
     * @param season
     *            季
     */
    private void changeItem(Season season) {
        // 更细当前列表
        seasons.remove(clickPosition);
        seasons.add(clickPosition, season);
        mAdapter.notifyItemChanged(clickPosition);
    }
    
    /**
     * 滚动到顶端
     */
    public void setScroll(int scrollY) {
        mScrollView.scrollTo(0, scrollY);
    }
    
    /**
     * 获取移动距离
     */
    public int getScroll() {
        return mScrollView.getScrollY();
    }
    
    /**
     * 获取季的个数
     */
    public int seasonSize() {
        return seasons.size();
    }
}
