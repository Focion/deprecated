package cn.focion.tv.ui.follow;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cn.focion.tv.R;
import cn.focion.tv.ui.FocionFragment;

/**
 * 分类 Created by Pirate on 2015/3/30.
 */
public class GenreFragment extends FocionFragment {
    
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        setContentView(R.layout.item);
        return super.onCreateView(inflater, container, savedInstanceState);
    }
    
    @Override
    public void trySetupData() {
        
    }
    
    @Override
    public void setFragmentTopClearance() {
        
    }
    
    @Override
    public void onReceive(Intent intent) {
        
    }
}
