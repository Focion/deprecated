package cn.focion.tv.ui.search;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import cn.focion.tv.R;
import cn.focion.tv.tool.util.Strings;
import cn.focion.tv.tool.util.UIs;
import cn.focion.tv.ui.FocionActivity;
import cn.focion.tv.ui.IOC;
import cn.focion.tv.widget.ShadowFrame;

/**
 * 搜索Activity Created by Pirate on 2015-05-18.
 */
public class SearchActivity extends FocionActivity implements
                                                  View.OnClickListener,
                                                  TextView.OnEditorActionListener {
    
    @IOC(id = R.id.focion_search_card)
    private CardView mCardView;
    
    @IOC(id = R.id.search_back)
    private ImageView mSearchBack;
    
    @IOC(id = R.id.search_btn)
    private ImageView mSearchBtn;
    
    @IOC(id = R.id.search_key)
    private EditText mSearchKey;
    
    private SearchFragment mSearchFragment;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.focion_search);
        // 初始化数据
        trySetupData();
    }
    
    /**
     * 初始化数据
     */
    private void trySetupData() {
        // 列表布局
        mSearchFragment = new SearchFragment();
        mFragmentManager.beginTransaction()
                        .replace(R.id.focion_frame, mSearchFragment)
                        .commit();
        // 设置点击事件
        mSearchBack.setOnClickListener(this);
        mSearchBtn.setOnClickListener(this);
        mSearchKey.setOnEditorActionListener(this);
    }
    
    @Override
    protected void autoShowOrHide(boolean isShow) {
        // mToolBar的隐藏
        if (isShow) {
            mCardView.animate()
                     .translationY(0)
                     .alpha(1)
                     .setDuration(HEADER_HIDE_ANIM_DURATION)
                     .setInterpolator(new DecelerateInterpolator());
        }
        else {
            mCardView.animate()
                     .translationY(UIs.dipToPx(this, -56))
                     .alpha(0)
                     .setDuration(HEADER_HIDE_ANIM_DURATION)
                     .setInterpolator(new DecelerateInterpolator());
        }
    }
    
    @Override
    public ShadowFrame shadowFrame() {
        return null;
    }
    
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.search_btn: // 点击搜索
                search();
                break;
            case R.id.search_back: // 返回
                onBackPressed();
                break;
        }
    }
    
    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH)
            search();
        return false;
    }
    
    /**
     * 搜索方法
     */
    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void search() {
        // 获取关键字
        String key = mSearchKey.getText().toString().trim();
        // 判断是否数据值
        if (Strings.isEmpty(key)) {
            Tips("请输入有效的关键字");
            return;
        }
        // 判断是否有空格
        if (key.contains(" "))
            key = key.replaceAll(" ", "-");
        // 先隐藏键盘
        // noinspection ConstantConditions
        ((InputMethodManager) mSearchKey.getContext()
                                        .getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                                                                                                                 InputMethodManager.HIDE_NOT_ALWAYS);
        // 存在关键字
        mSearchFragment.updateData(key);
    }
}
