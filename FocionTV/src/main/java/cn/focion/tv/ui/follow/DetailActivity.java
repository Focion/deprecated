package cn.focion.tv.ui.follow;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import cn.focion.tv.Constants;
import cn.focion.tv.R;
import cn.focion.tv.tool.util.UIs;
import cn.focion.tv.ui.FocionActivity;
import cn.focion.tv.ui.IOC;
import cn.focion.tv.widget.ShadowFrame;

/**
 * 添加剧集 Created by Pirate on 14/12/3.
 */
public class DetailActivity extends FocionActivity {
    
    @IOC(id = R.id.focion_toolbar)
    private Toolbar mToolBar;
    
    @IOC(id = R.id.focion_content)
    private ShadowFrame mShadowFrame;
    
    private boolean isShadowShow = false;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.focion_toolbar_normal);
        // 配置ToolBar
        trySetupToolBar();
        // 设置Fragment
        trySetupFragment();
    }
    
    @Override
    public ShadowFrame shadowFrame() {
        return mShadowFrame;
    }
    
    /**
     * 配置ToolBar
     */
    private void trySetupToolBar() {
        mToolBar.setTitle("");
        int colorTrans = Color.TRANSPARENT;
        // 设置字体和背景颜色
        mToolBar.setTitleTextColor(colorTrans);
        mToolBar.setBackgroundColor(colorTrans);
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    
    /**
     * 设置Fragment和阴影
     */
    private void trySetupFragment() {
        // 设置阴影FramLayout的高度
        mShadowFrame.setShadowTopOffset(UIs.dipToPx(this, 56));
        // 设置阴影显示
        mShadowFrame.setShadowVisible(isShadowShow, isShadowShow);
        // 设置侧滑栏的布局Fragment
        mFragmentManager.beginTransaction()
                        .replace(R.id.focion_frame, new DetailFragment())
                        .commit();
    }
    
    /**
     * 设置Title的方法
     */
    public void setTextColor(int color) {
        mToolBar.setTitleTextColor(color);
    }
    
    public void setText(String title) {
        mToolBar.setTitle(title);
    }
    
    /**
     * 设置背景的方法
     *
     * @param color
     *            颜色值
     */
    public void setBackgroundColor(int color) {
        mToolBar.setBackgroundColor(color);
    }
    
    /**
     * 设置阴影高度
     *
     * @param isShow
     *            true 显示 false 不显示
     */
    public void setShadowShow(boolean isShow) {
        if (isShadowShow != isShow) {
            // 设置阴影FramLayout的显示
            mShadowFrame.setShadowVisible(isShow, isShow);
            isShadowShow = isShow;
        }
    }
}
