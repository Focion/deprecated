package cn.focion.tv.ui.main;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;

import cn.focion.tv.Constants;
import cn.focion.tv.R;
import cn.focion.tv.model.Show;
import cn.focion.tv.tool.util.Dates;
import cn.focion.tv.tool.util.Strings;
import cn.focion.tv.tool.util.UIs;
import cn.focion.tv.ui.FocionFragment;
import cn.focion.tv.ui.IOC;
import cn.focion.tv.widget.CircleButton;
import cn.focion.tv.widget.CircleProgress;
import cn.focion.tv.widget.ObsScrollView;

/**
 * 详情 Created by Pirate on 2015/4/8.
 */
public class ShowFragment extends FocionFragment<DetailsActivity> implements
                                                                 ObsScrollView.Callbacks,
                                                                 ObsScrollView.CallbacksDelta {
    
    @IOC(id = R.id.detail_scroll)
    private ObsScrollView mScrollView;
    
    @IOC(id = R.id.detail_score_pro)
    private CircleProgress mProgress;
    
    @IOC(id = R.id.detail_score)
    private TextView mScore;
    
    @IOC(id = R.id.detail_name)
    private TextView mName;
    
    @IOC(id = R.id.detail_show_country)
    private TextView mSignalCount;
    
    @IOC(id = R.id.detail_show_airtime)
    private TextView mLongTime;
    
    @IOC(id = R.id.detail_show_time)
    private TextView mWeek;
    
    @IOC(id = R.id.detail_status)
    private TextView mStatus;
    
    @IOC(id = R.id.detail_first_time)
    private TextView mFirstTime;
    
    @IOC(id = R.id.detail_show_watchers)
    private TextView mWatchers;
    
    @IOC(id = R.id.detail_show_overview_zh)
    private TextView mOverviewZh;
    
    @IOC(id = R.id.detail_show_overview_en)
    private TextView mOverviewEn;
    
    @IOC(id = R.id.detail_genre)
    private TextView mGenre;
    
    @IOC(id = R.id.detail_add)
    private CircleButton mMoreFun;
    
    private int defaultY;
    
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        setContentView(R.layout.layout_main_detail);
        return super.onCreateView(inflater, container, savedInstanceState);
    }
    
    @Override
    public void trySetupData() {
        defaultY = UIs.dipToPx(mActivity, 203 - 56 - 48);
        // 获取Show的信息
        Show show = (Show) getArguments().getSerializable("Show");
        // 设置Show的信息
        mName.setText(Strings.title(show));
        mSignalCount.setText(String.format("%s  %s",
                                           show.getNetwork(),
                                           show.getCountry_zh()));
        mLongTime.setText(String.format("%d分钟", show.getRuntime()));
        String day = show.getDay();
        if (Strings.isEmpty(day))
            day = "未知";
        mWeek.setText(day);
        mStatus.setText(show.getStatus_zh());
        mFirstTime.setText(Dates.changeZone(show.getTimezone(),
                                            show.getFirst_aired(),
                                            Dates.YYYY_MM_DD));
        mWatchers.setText(String.format("%,d", show.getWatchers()));
        mOverviewZh.setText(show.getOverview_zh().replace("　", ""));
        mOverviewEn.setText(show.getOverview());
        // 设置分数
        mScore.setText(String.format("%.1f", show.getRating()));
        mProgress.setMax(10f);
        mProgress.setProgress(show.getRating());
        // 设置剧类型
        mGenre.setText(show.getGenres_zh());
        setContentShown(true);
        // 设置监听
        mScrollView.addCallbacks(this);
        mScrollView.addCallbacksDelta(this);
    }
    
    @Override
    public void onScrollChanged(int scrollX, int scrollY) {
        // 透明度
        float alpha;
        // 颜色
        int color = Color.parseColor(Constants.TITLE_COLOR);
        if (scrollY < defaultY) {
            alpha = (float) scrollY / defaultY;
            // 设置阴影
            mActivity.setShadowShow(false);
            mActivity.setLayoutTop(-scrollY + UIs.dipToPx(mActivity,
                                                                 203 - 48));
            // 设置颜色
            mActivity.setBackgroundColor(UIs.colorAlpha(color, 0));
        }
        else {
            alpha = 1;
            // 设置距离
            mActivity.setLayoutTop(UIs.dipToPx(mActivity, 56));
            // 设置阴影
            mActivity.setShadowShow(true);
            // 设置颜色
            mActivity.setBackgroundColor(color);
        }
        mActivity.setImageLayoutTop(-scrollY);
        // 设置背景
        mActivity.setScrollColor(UIs.colorAlpha(color, alpha));
        // 设置字体颜色
        mActivity.setToolTextColor(UIs.colorAlpha(Color.WHITE, alpha));
    }
    
    @Override
    public void onScrollChangedDelta(int deltaX, int deltaY) {
        if (deltaY >= 0)
            onAutoHideCirBar(false);
        else
            onAutoHideCirBar(true);
    }
    
    /**
     * 隐藏Bar
     */
    private void onAutoHideCirBar(boolean isShow) {
        if (isShow) {
            mMoreFun.animate()
                    .translationY(0)
                    .alpha(1)
                    .setDuration(300)
                    .setInterpolator(new DecelerateInterpolator());
        }
        else { // 隐藏
            mMoreFun.animate()
                    .translationY(UIs.dipToPx(mActivity, 56 + 16))
                    .alpha(0)
                    .setDuration(300)
                    .setInterpolator(new DecelerateInterpolator());
        }
    }
    
    /**
     * 滚动到顶端
     */
    public void setScroll(int scrollY) {
        mScrollView.scrollTo(0, scrollY);
    }
    
    /**
     * 获取移动距离
     */
    public int getScroll() {
        return mScrollView.getScrollY();
    }
}
