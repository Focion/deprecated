package cn.focion.tv.ui.main;

import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.Spinner;

import cn.focion.tv.R;
import cn.focion.tv.adapter.main.SpinnerAdapter;
import cn.focion.tv.tool.util.UIs;
import cn.focion.tv.ui.FocionActivity;
import cn.focion.tv.ui.IOC;
import cn.focion.tv.widget.RefreshLayout;
import cn.focion.tv.widget.ShadowFrame;

/**
 * 主页面 Created by Pirate on 14/11/24.
 */
public class MainActivity extends FocionActivity implements
                                                RefreshLayout.CanChildScrollUpCallback {

    @IOC(id = R.id.focion_toolbar)
    private Toolbar mToolBar;
    
    @IOC(id = R.id.focion_content)
    private ShadowFrame mShadowFrame;
    
    @IOC(id = R.id.focion_drawer_layout)
    private DrawerLayout mDrawerLayout;
    
    @IOC(id = R.id.focion_refresh)
    private RefreshLayout mRefresh;
    
    // ActionBar与Drawer的接合器
    private ActionBarDrawerToggle mDrawerToggle;
    
    // 观看列表
    private WatchFragment mWacthFragment;
    
    private int startMargin;
    
    private int endMargin;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.focion_toolbar_drawer);
        // 设置ToolBar
        trySetupToolBar();
    }
    
    /**
     * 设置ToolBar
     */
    private void trySetupToolBar() {
        setSupportActionBar(mToolBar);
        // 初始化数据
        startMargin = getResources().getDimensionPixelSize(R.dimen.refresh_start_margin);
        endMargin = getResources().getDimensionPixelSize(R.dimen.refresh_end_margin);
        // 初始化侧边栏
        trySetupDrawerParams();
        // 初始化下拉
        trySetupSpinner();
        // 初始化下拉刷新
        trySetupRefresh();
        // 设置阴影FramLayout的高度
        mShadowFrame.setShadowTopOffset(UIs.dipToPx(this, 56));
    }
    
    /**
     * 设置侧滑抽屉的参数
     */
    private void trySetupDrawerParams() {
        mFragmentManager = getSupportFragmentManager();
        // 阴影设置
        mDrawerLayout.setDrawerShadow(R.mipmap.bg_shadow_drawer_left,
                                      GravityCompat.START);
        mDrawerToggle = new ActionBarDrawerToggle(this,
                                                  mDrawerLayout,
                                                  mToolBar,
                                                  R.string.drawer_open,
                                                  R.string.drawer_close);
        // 设置接合器
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        // 设置侧滑栏的布局Fragment
        mFragmentManager.beginTransaction()
                        .replace(R.id.focion_left_drawer, new DrawerFragment())
                        .commit();
        // 列表布局
        mWacthFragment = new WatchFragment();
        mFragmentManager.beginTransaction()
                        .replace(R.id.focion_frame, mWacthFragment)
                        .commit();
    }
    
    /**
     * 初始化Pop
     */
    private void trySetupSpinner() {
        int itemToSelect = -1;
        View spinnerContainer = LayoutInflater.from(this)
                                              .inflate(R.layout.focion_spinner,
                                                       mToolBar,
                                                       false);
        ActionBar.LayoutParams lp = new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                                               ViewGroup.LayoutParams.MATCH_PARENT);
        mToolBar.addView(spinnerContainer, lp);
        Spinner spinner = (Spinner) spinnerContainer.findViewById(R.id.focion_spinner);
        // SDK版本必须大于16
        if (Build.VERSION.SDK_INT > 15)
            spinner.setDropDownVerticalOffset(startMargin);
        
        SpinnerAdapter mAdapter = new SpinnerAdapter(this);
        spinner.setAdapter(mAdapter);
        // 设置点击事件
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> spinner,
                                       View view,
                                       int position,
                                       long itemId) {
                // mWacthFragment.setContentShown(false);
            }
            
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        if (itemToSelect >= 0) {
            spinner.setSelection(itemToSelect);
        }
    }
    
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        
        switch (item.getItemId()) { // 获取点击Menu的ID
        
            case android.R.id.home: { // 点击HOME键
                if (mDrawerLayout.isDrawerOpen(GravityCompat.START))
                    // 关闭Drawer
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                else
                    // 打开Drawer
                    mDrawerLayout.openDrawer(GravityCompat.START);
                break;
            }
        }
        return true;
    }
    
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // 异步改变当前Drawer状态的图标
        mDrawerToggle.syncState();
    }
    
    /**
     * 初始化下拉刷新
     */
    private void trySetupRefresh() {
        // 设置颜色
        mRefresh.setColorSchemeResources(R.color.refresh_progress_1,
                                         R.color.refresh_progress_2,
                                         R.color.refresh_progress_3);
        // 设置监听
        mRefresh.setCanChildScrollUpCallback(this);
        mRefresh.setOnRefreshListener(new RefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Tips("哈哈哈");
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(10000);
                        }
                        catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mRefresh.setRefreshing(false);
                                
                            }
                        });
                    }
                }).start();
            }
        });
        
        if (mRefresh == null)
            return;
        // 顶部距离
        mRefresh.setProgressViewOffset(true, 0, endMargin - startMargin);
    }
    
    @Override
    protected void autoShowOrHide(boolean isShow) {
        // mToolBar的隐藏
        if (isShow) {
            mToolBar.animate()
                    .translationY(0)
                    .alpha(1)
                    .setDuration(HEADER_HIDE_ANIM_DURATION)
                    .setInterpolator(new DecelerateInterpolator());
        }
        else {
            mToolBar.animate()
                    .translationY(startMargin)
                    .alpha(0)
                    .setDuration(HEADER_HIDE_ANIM_DURATION)
                    .setInterpolator(new DecelerateInterpolator());
        }
        // 设置阴影是否显示
        mShadowFrame.setShadowVisible(isShow, isShow);
    }
    
    @Override
    public boolean canSwipeRefreshChildScrollUp() {
        return mWacthFragment != null && mWacthFragment.canSwipeRefreshChildScrollUp();
    }
    
    @Override
    public void onBackPressed() {
        // 判断抽屉是否打开
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            // 关闭Drawer
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
        super.onBackPressed();
    }
    
    @Override
    public ShadowFrame shadowFrame() {
        return mShadowFrame;
    }
}
