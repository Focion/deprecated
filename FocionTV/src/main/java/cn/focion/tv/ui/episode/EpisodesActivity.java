package cn.focion.tv.ui.episode;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.animation.DecelerateInterpolator;

import cn.focion.tv.R;
import cn.focion.tv.model.Season;
import cn.focion.tv.tool.util.Strings;
import cn.focion.tv.tool.util.UIs;
import cn.focion.tv.ui.FocionActivity;
import cn.focion.tv.ui.IOC;
import cn.focion.tv.widget.RefreshLayout;
import cn.focion.tv.widget.ShadowFrame;

/**
 * 集列表 Created by Pirate on 2015-05-08.
 */
public class EpisodesActivity extends FocionActivity implements
                                                    RefreshLayout.CanChildScrollUpCallback {
    
    @IOC(id = R.id.focion_toolbar)
    private Toolbar mToolBar;
    
    @IOC(id = R.id.focion_content)
    private ShadowFrame mShadowFrame;
    
    @IOC(id = R.id.focion_refresh)
    private RefreshLayout mRefresh;
    
    private int startMargin;
    
    private int endMargin;
    
    private EpisodesFragment mEpisodesFragment;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.focion_toolbar_refresh);
        // 设置ToolBar
        trySetupToolBar();
        // 设置下拉刷新
        trySetupRefresh();
    }
    
    /**
     * 设置ToolBar
     */
    private void trySetupToolBar() {
        // 获取参数
        Intent intent = getIntent();
        Season season = (Season) intent.getSerializableExtra("Season");
        // 获取title和timezone并分割
        String[] title_timezone = intent.getStringExtra("title&timezone")
                                        .split("&");
        // 设置Title
        mToolBar.setTitle(title_timezone[0]);
        mToolBar.setSubtitle(Strings.title(season));
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // 初始化数据
        startMargin = getResources().getDimensionPixelSize(R.dimen.refresh_start_margin);
        endMargin = getResources().getDimensionPixelSize(R.dimen.refresh_end_margin);
        // 初始化下拉刷新
        trySetupRefresh();
        // 设置阴影FramLayout的高度
        mShadowFrame.setShadowTopOffset(UIs.dipToPx(this, 56));
        // 列表布局
        mEpisodesFragment = new EpisodesFragment();
        // 传参数
        Bundle bundle = new Bundle();
        // 时区
        bundle.putString("timezone", title_timezone[1]);
        // 季
        bundle.putSerializable("Season", season);
        mEpisodesFragment.setArguments(bundle);
        mFragmentManager.beginTransaction()
                        .replace(R.id.focion_frame, mEpisodesFragment)
                        .commit();
        
    }
    
    /**
     * 初始化下拉刷新
     */
    private void trySetupRefresh() {
        // 设置颜色
        mRefresh.setColorSchemeResources(R.color.refresh_progress_1,
                                         R.color.refresh_progress_2,
                                         R.color.refresh_progress_3);
        // 设置监听
        mRefresh.setCanChildScrollUpCallback(this);
        mRefresh.setOnRefreshListener(new RefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Tips("哈哈哈");
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(10000);
                        }
                        catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mRefresh.setRefreshing(false);
                                
                            }
                        });
                    }
                }).start();
            }
        });
        
        if (mRefresh == null)
            return;
        // 顶部距离
        mRefresh.setProgressViewOffset(true, 0, endMargin - startMargin);
    }
    
    @Override
    protected void autoShowOrHide(boolean isShow) {
        // mToolBar的隐藏
        if (isShow) {
            mToolBar.animate()
                    .translationY(0)
                    .alpha(1)
                    .setDuration(HEADER_HIDE_ANIM_DURATION)
                    .setInterpolator(new DecelerateInterpolator());
        }
        else {
            mToolBar.animate()
                    .translationY(startMargin)
                    .alpha(0)
                    .setDuration(HEADER_HIDE_ANIM_DURATION)
                    .setInterpolator(new DecelerateInterpolator());
        }
        // 设置阴影是否显示
        mShadowFrame.setShadowVisible(isShow, isShow);
    }
    
    @Override
    public ShadowFrame shadowFrame() {
        return mShadowFrame;
    }
    
    @Override
    public boolean canSwipeRefreshChildScrollUp() {
        return mEpisodesFragment != null && mEpisodesFragment.canSwipeRefreshChildScrollUp();
    }
}
