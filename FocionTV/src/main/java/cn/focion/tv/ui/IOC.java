package cn.focion.tv.ui;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 注入标签 Created by Pirate on 14-5-7.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface IOC {
    
    /**
     * View的Id
     */
    public int id();
}
