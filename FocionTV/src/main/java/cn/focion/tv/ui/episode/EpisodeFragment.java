package cn.focion.tv.ui.episode;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cn.focion.tv.Constants;
import cn.focion.tv.R;
import cn.focion.tv.model.Episode;
import cn.focion.tv.tool.util.Strings;
import cn.focion.tv.tool.util.UIs;
import cn.focion.tv.ui.FocionFragment;
import cn.focion.tv.ui.IOC;
import cn.focion.tv.widget.ObsScrollView;

/**
 * 集的页面 Created by Pirate on 2015-04-29.
 */
public class EpisodeFragment extends FocionFragment<EpisodeActivity> implements
                                                                    ObsScrollView.Callbacks {
    
    @IOC(id = R.id.detail_scroll)
    private ObsScrollView mScroll;
    
    @IOC(id = R.id.detail_alarm)
    private TextView mAlarm;
    
    @IOC(id = R.id.detail_overview)
    private TextView mOverview;
    
    @IOC(id = R.id.detail_view)
    private View mView;
    
    private int heightPx;
    
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        setContentView(R.layout.layout_episode_detail);
        return super.onCreateView(inflater, container, savedInstanceState);
    }
    
    @Override
    public void trySetupData() {
        heightPx = UIs.dipToPx(mActivity, 203 - 56);
        // 设置滑动监听
        mScroll.addCallbacks(this);
        // 获取集
        Episode episode = (Episode) getArguments().getSerializable("Episode");
        // 设置值
        updateEpisode(episode);
    }
    
    @Override
    public void onScrollChanged(int scrollX, int scrollY) {
        // 透明度
        float alpha;
        // 颜色值
        int rgb = mActivity.rgb();
        if (scrollY < heightPx) {
            alpha = (float) scrollY / heightPx;
            // 图片滚动
            mActivity.setToolBarLayout(-scrollY);
            mActivity.setDraweeView(scrollY / 2);
            // 设置阴影
            mActivity.setShadowShow(false);
            // 设置ToolBar的背景颜色
            mActivity.setToolBackground(UIs.colorAlpha(rgb, 0));
        }
        else {
            alpha = 1;
            // 设置阴影
            mActivity.setShadowShow(true);
            // 图片滚动
            mActivity.setToolBarLayout(-heightPx);
            // 设置颜色
            mActivity.setBackgroundColor(rgb);
            // 设置ToolBar的背景颜色
            mActivity.setToolBackground(rgb);
        }
        // 背景颜色
        mActivity.setBackgroundColor(UIs.colorAlpha(rgb, alpha));
        // 计算字体透明度
        mActivity.setTextColor(UIs.colorAlpha(Color.WHITE, alpha));
    }
    
    /**
     * 更新当前Episode
     *
     * @param episode
     *            对象
     */
    public void updateEpisode(Episode episode) {
        // 设置值
        mAlarm.setText(episode.getImages());
        // 判断是否有简介
        String overview = episode.getOverview();
        if (Strings.isEmpty(overview))
            overview = "暂无";
        mOverview.setText(overview + overview + overview + overview);
        // 获取高度
        int height = UIs.dipToPx(mActivity, 203 + 16) + mActivity.height();
        // 动态设置高度
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                                                                                   height);
        mView.setLayoutParams(layoutParams);
        // 设置Title并透明
        mActivity.setText(Strings.dealDisplay(episode.getSeason(),
                                              episode.getNumber()));
        setContentShown(true);
        // 滑动到顶端
        mScroll.scrollTo(0, 0);
    }
}
