package cn.focion.tv.ui.category;

import android.os.Bundle;

import cn.focion.tv.R;
import cn.focion.tv.ui.FocionActivity;
import cn.focion.tv.widget.ShadowFrame;

/**
 * 分类 Created by Pirate on 2015-05-21.
 */
public class CategoryActivity extends FocionActivity {
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_category_toolbar);
    }
    
    @Override
    public ShadowFrame shadowFrame() {
        return null;
    }
}
