package cn.focion.tv.ui.follow;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;

import cn.focion.tv.Constants;
import cn.focion.tv.R;
import cn.focion.tv.adapter.follow.FollowAdapter;
import cn.focion.tv.tool.util.UIs;
import cn.focion.tv.ui.FocionActivity;
import cn.focion.tv.ui.FocionFragment;
import cn.focion.tv.ui.IOC;
import cn.focion.tv.ui.search.SearchActivity;
import cn.focion.tv.widget.ShadowFrame;
import cn.focion.tv.widget.SlidingTabStrip;

/**
 * 关注 Created by Pirate on 2015/3/30.
 */
public class FollowActivity extends FocionActivity implements
                                                  ViewPager.OnPageChangeListener {
    
    @IOC(id = R.id.focion_tool_bar_layout)
    private LinearLayout mToolLayout;
    
    @IOC(id = R.id.focion_toolbar)
    private Toolbar mToolBar;
    
    @IOC(id = R.id.focion_content)
    private ShadowFrame mShadowFrame;
    
    @IOC(id = R.id.focion_tab_pager)
    private ViewPager mViewPager;
    
    @IOC(id = R.id.focion_tab)
    private SlidingTabStrip mTabStrip;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.focion_toolbar_tabstrip);
        // 加载ToolBar
        trySetupToolBar();
        // 加载Tab
        trySetupTabStrip();
    }
    
    /**
     * 设置ToolBar
     */
    private void trySetupToolBar() {
        mToolBar.setTitle(R.string.follow_trend);
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // 设置阴影FramLayout的高度
        mShadowFrame.setShadowTopOffset(UIs.dipToPx(this, 56 + 48));
    }
    
    /**
     * Follow中Tab的参数设置
     */
    private void trySetupTabStrip() {
        ArrayList<FocionFragment> fragments = new ArrayList<>();
        TrendFragment trendFragment = new TrendFragment();
        GenreFragment genreFragment = new GenreFragment();
        PopularFragment popularFragment = new PopularFragment();
        // 流行，趋势，分类，搜索
        fragments.add(popularFragment);
        fragments.add(trendFragment);
        fragments.add(genreFragment);
        // 创建适配器
        FollowAdapter pagerAdapter = new FollowAdapter(mFragmentManager,
                                                       fragments);
        // 设置适配器
        mViewPager.setAdapter(pagerAdapter);
        // 设置Fragment缓存数量
        mViewPager.setOffscreenPageLimit(Constants.MAX_CACHE_SIZE);
        // 设置字体颜色和大小
        mTabStrip.setTextColorResource(R.color.white_clr);
        // 绑定ViewPager
        mTabStrip.setViewPager(mViewPager);
        // 设置监听
        mTabStrip.setOnPageChangeListener(this);
        // 初始移动
        ViewCompat.animate(mTabStrip)
                  .translationX(UIs.dipToPx(this, 56))
                  .setDuration(HEADER_HIDE_ANIM_DURATION + 100)
                  .start();
    }
    
    @Override
    protected void autoShowOrHide(boolean isShow) {
        // mToolBar的隐藏
        if (isShow) {
            mToolLayout.animate()
                       .translationY(0)
                       .setDuration(HEADER_HIDE_ANIM_DURATION)
                       .setInterpolator(new DecelerateInterpolator());
            mShadowFrame.setShadowTopOffset(UIs.dipToPx(this, 56 + 48));
        }
        else {
            mToolLayout.animate()
                       .translationY(-mToolBar.getBottom())
                       .setDuration(HEADER_HIDE_ANIM_DURATION)
                       .setInterpolator(new DecelerateInterpolator());
            mShadowFrame.setShadowTopOffset(UIs.dipToPx(this, 48));
        }
        // 设置阴影是否显示
        mShadowFrame.setShadowVisible(true, true);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        // 获取到搜索的Item
        MenuItem searchItem = menu.findItem(R.id.menu_search);
        // 搜索点击监听
        searchItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.menu_search) {
                    Intent intent = new Intent(FollowActivity.this,
                                               SearchActivity.class);
                    startActivity(intent);
                }
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
    
    @Override
    public void onPageScrolled(int position,
                               float positionOffset,
                               int positionOffsetPixels) {
    }
    
    @Override
    public void onPageSelected(int position) {
        pageScrolled(position);
        int anim = 0;
        if (position == 0)
            anim = UIs.dipToPx(this, 56);
        else if (position == 1)
            anim = UIs.dipToPx(this, 36);
        else if (position == 2)
            anim = 0;
        ViewCompat.animate(mTabStrip)
                  .translationX(anim)
                  .setDuration(HEADER_HIDE_ANIM_DURATION + 100)
                  .start();
        // 显示ActionBar
        if (!isShow)
            autoShowOrHide(true);
    }
    
    @Override
    public void onPageScrollStateChanged(int state) {
        
    }
    
    /**
     * 滚动状态改变
     *
     * @param position
     *            角标
     */
    private void pageScrolled(int position) {
        if (position == 0)
            mToolBar.setTitle(R.string.follow_popular);
        else if (position == 1)
            mToolBar.setTitle(R.string.follow_trend);
        else
            mToolBar.setTitle(R.string.follow_genre);
    }
    
    @Override
    public ShadowFrame shadowFrame() {
        return mShadowFrame;
    }
}
