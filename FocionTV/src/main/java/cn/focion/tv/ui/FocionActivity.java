package cn.focion.tv.ui;

import java.lang.reflect.Field;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.widget.Toast;

import cn.focion.tv.Focion;
import cn.focion.tv.tool.board.UpdateBoard;
import cn.focion.tv.tool.pool.TaskItem;
import cn.focion.tv.tool.pool.TaskListener;
import cn.focion.tv.tool.pool.TaskPool;
import cn.focion.tv.widget.HideBarListener;
import cn.focion.tv.widget.ObsScrollView;
import cn.focion.tv.widget.ShadowFrame;

/**
 * 整个Activity的框架类 Created by Pirate on 14-5-7.
 */
public abstract class FocionActivity extends AppCompatActivity implements
                                                              UpdateBoard.ReceiveBoard {
    
    /**
     * 全局变量
     */
    public Focion mFocion;
    
    /**
     * 全局的LayoutInflater对象
     */
    public LayoutInflater mInflater = null;
    
    /**
     * Window 管理器
     */
    public WindowManager mWindowManager = null;
    
    /**
     * Fragment 管理器
     */
    public FragmentManager mFragmentManager = null;
    
    /**
     * 全局的线程执行类
     */
    public TaskPool mPool;
    
    /**
     * 全局任务
     */
    public TaskItem mTask;
    
    /**
     * ActionBar是否显示
     */
    public boolean isShow;
    
    /**
     * 动画速度
     */
    public static final int HEADER_HIDE_ANIM_DURATION = 200;
    
    /** 广播 */
    private UpdateBoard mUpdateBoard;

    /** 意图过滤 */
    private IntentFilter mFilter;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 获取全局
        mFocion = (Focion) getApplication();
        // 初始化全局对象
        mInflater = LayoutInflater.from(this);
        // 初始化管理器
        mWindowManager = getWindowManager();
        // 初始化Fragment管理器
        mFragmentManager = getSupportFragmentManager();
        // 初始化线程
        mPool = TaskPool.getInstance();
        // 初始化全局任务
        mTask = new TaskItem();
        mTask.listener = new TaskListener() {
            @Override
            public void get() {
                runTask();
            }
            
            @Override
            public void update() {
                updateUI();
            }
        };
    }
    
    @Override
    public void setContentView(int layoutId) {
        super.setContentView(layoutId);
        // 初始化标记控件
        trySetupView();
    }
    
    /**
     * /** 初始化所有控件
     */
    private void trySetupView() {
        // 获取所有字段，包括私有字段，但不包括父类字段
        Field[] fields = ((Object) this).getClass().getDeclaredFields();
        if (fields != null && fields.length > 0) { // 判断字段是否为空
            for (Field field : fields) {
                try {
                    // 设置字段可编辑
                    field.setAccessible(true);
                    
                    if (field.get(this) != null)
                        continue;
                    // 获取标记为IOC注解的字段
                    IOC annoView = field.getAnnotation(IOC.class);
                    if (annoView != null) { // 如果不为空
                        // 获取注解的ID
                        int annoId = annoView.id();
                        field.set(this, findViewById(annoId)); // 获取对象并设置
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    /**
     * 执行线程
     */
    public void runTask() {
        
    }
    
    /**
     * 更新UI
     */
    public void updateUI() {
        
    }
    
    /**
     * 添加注册广播
     *
     * @param actions
     *            广播
     */
    protected void addAction(String... actions) {
        // 更新的广播创建
        if (mUpdateBoard == null)
            mUpdateBoard = new UpdateBoard(this);
        // 创建过滤
        if (mFilter == null)
            mFilter = new IntentFilter();
        // 循环添加
        for (String action : actions)
            mFilter.addAction(action);
        // 注册广播
        registerReceiver(mUpdateBoard, mFilter);
    }
    
    /**
     * ActionBar是否能自动隐藏
     *
     * @param recyclerView
     *            滑动的RecyclerView
     */
    public void autoHideBar(final RecyclerView recyclerView) {
        autoHideBar(recyclerView, null);
    }
    
    /**
     * ActionBar是否能自动隐藏
     *
     * @param recyclerView
     *            滑动的RecyclerView
     * @param loadListener
     *            加载更多监听
     */
    public void autoHideBar(final RecyclerView recyclerView,
                            final cn.focion.tv.widget.RecyclerView.LoadMoreListener loadListener) {
        // RecyclerView滑动监听
        recyclerView.addOnScrollListener(new HideBarListener(loadListener) {

            @Override
            public void onShow() {
                isShow = true;
                autoShowOrHide(true);
            }

            @Override
            public void onHide() {
                isShow = false;
                autoShowOrHide(false);
            }
        });
    }

    /**
     * 自动显示或者隐藏ActionBar
     *
     * @param isShow
     *            是否显示
     */
    protected void autoShowOrHide(boolean isShow) {
        
    }
    
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // 设置阴影
        if (shadowFrame() != null) {
            shadowFrame().setAlpha(0);
            shadowFrame().animate()
                         .alpha(1)
                         .setDuration(HEADER_HIDE_ANIM_DURATION);
        }
    }
    
    /**
     * 全局吐司提示
     * 
     * @param msg
     *            提示信息
     */
    public void Tips(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }
    
    @Override
    public void onBackPressed() {
        this.finish();
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mUpdateBoard != null)
            unregisterReceiver(mUpdateBoard);
    }
    
    /**
     * 子类需覆写此方法
     *
     * @return ShadowFrame对象
     */
    public abstract ShadowFrame shadowFrame();
    
    @Override
    public void onReceive(Intent intent) {
        
    }
}
