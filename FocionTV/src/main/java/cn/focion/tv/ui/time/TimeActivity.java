package cn.focion.tv.ui.time;

import android.os.Bundle;

import cn.focion.tv.ui.FocionActivity;
import cn.focion.tv.widget.ShadowFrame;

/**
 * 时间表 Created by Pirate on 2015-05-15.
 */
public class TimeActivity extends FocionActivity {
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    
    @Override
    public ShadowFrame shadowFrame() {
        return null;
    }
}
