package cn.focion.tv.ui.dialog;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import cn.focion.tv.R;
import cn.focion.tv.ui.IOC;

/**
 * 提示Dialog Created by Pirate on 14-8-14.
 */
public class TipsDialog extends FocionDialog {
    
    // 取消按钮
    @IOC(id = R.id.dialog_cancel)
    private TextView mCancel;
    
    // 确定按钮
    @IOC(id = R.id.dialog_sure)
    private TextView mSure;
    
    @IOC(id = R.id.dialog_tips_message)
    private TextView mMessage;
    
    private String message;
    
    private OnClickListener mSureClickListener;
    
    /**
     * TipsDialog对话框
     *
     * @param context
     *            上下文
     * @param message
     *            信息
     * @param clickListener
     *            确定监听
     */
    public TipsDialog(Context context,
                      String message,
                      OnClickListener clickListener) {
        super(context);
        this.message = message;
        this.mSureClickListener = clickListener;
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_tips);
        mMessage.setText(message);
        mCancel.setOnClickListener(this);
        mSure.setOnClickListener(this);
    }
    
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dialog_sure: // 确定
                if (mSureClickListener == null)
                    return;
                mSureClickListener.onClick(TipsDialog.this, -1);
                break;
            case R.id.dialog_cancel: // 取消
                TipsDialog.this.dismiss();
                break;
        }
    }
}
