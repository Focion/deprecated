package cn.focion.tv.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cn.focion.tv.Constants;
import cn.focion.tv.R;
import cn.focion.tv.adapter.main.DrawerAdapter;
import cn.focion.tv.ui.FocionFragment;
import cn.focion.tv.ui.IOC;
import cn.focion.tv.widget.RecyclerView;

/**
 * 测试 Created by Pirate on 14/11/12.
 */
public class DrawerFragment extends FocionFragment<MainActivity> implements
                                                                View.OnClickListener,
                                                                RecyclerView.ItemClickListener {
    
    // 注销按钮
    @IOC(id = R.id.drawer_logout)
    private TextView mLogoutBtn;
    
    // 姓名
    @IOC(id = R.id.drawer_username)
    private TextView mUsername;
    
    // 关注剧的数量
    @IOC(id = R.id.drawer_follow_count)
    private TextView mFollowCount;
    
    // 个人信息点击控件
    @IOC(id = R.id.drawer_profile_layout)
    private RelativeLayout mUserInfoLayout;
    
    // 用户头像
    @IOC(id = R.id.drawer_user_img)
    // private CircleImage userImg;
    private ImageView userImg;
    
    // 初始化功能列表
    @IOC(id = R.id.drawer_fuction_recycler)
    private RecyclerView mFuctions;
    
    // 同步按钮
    @IOC(id = R.id.drawer_sync)
    private TextView mSync;
    
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        setContentView(R.layout.layout_main_drawer);
        return super.onCreateView(inflater, container, savedInstanceState);
    }
    
    @Override
    public void trySetupData() {
        // 显示布局
        setContentShownNoAnimation(true);
        mFuctions.setLayoutManager(mLayoutManager);
        DrawerAdapter adapter = new DrawerAdapter(mActivity,
                                                  R.layout.layout_main_drawer_item);
        // 设置点击监听
        adapter.setItemClickListener(this);
        mFuctions.setAdapter(adapter);
        // 设置数据
        mFollowCount.setText("12,780");
        mUsername.setText("Pirate.D.Zhang");
        // 注册监听
        mUserInfoLayout.setOnClickListener(this);
        mSync.setOnClickListener(this);
        mLogoutBtn.setOnClickListener(this);
    }
    
    @Override
    public void setFragmentTopClearance() {
        
    }
    
    @Override
    public void onClick(View v) {
        Intent intent = new Intent();
        // if (mUser == null) {
        // intent.setClass(mActivity, LoginActivity.class);
        // // 跳转
        // mActivity.startActivity(intent);
        // }
        
        switch (v.getId()) {
            case R.id.drawer_profile_layout:
                // intent.setClass(mActivity, ProfileActivity.class);
                break;
            case R.id.drawer_sync:
                // SyncUtils syncUtils = SyncUtils.getInstance(mActivity,
                // mFocion.user);
                // 无论如何 向上同步
                // syncUtils.syncAllUp();
                // 如果没有 默认true
                // boolean sync = PreUtils.readShareSync(mActivity);
                // if (sync) {
                // syncUtils.syncCategoryDown();
                // syncUtils.syncShowDown();
                // }
                return;
            case R.id.drawer_logout:
                // new TipsDialog(mActivity,
                // "是否注销当前用户？",
                // new DialogInterface.OnClickListener() {
                // @Override
                // public void onClick(DialogInterface dialog,
                // int which) {
                // OAuth.getInstance(mActivity)
                // .clearOAuth();
                // 删除登录的用户
                // dialog.dismiss();
                // 改变首页数据加载
                // Constants.WATCH_LIST_UPDATE = true;
                // Constants.CATEGORY_LIST_UPDATE = true;
                // mActivity.finish();
                // }
                // }).show();
                // return;
                // default:
                break;
        }
        // 跳转
        // mActivity.startActivity(intent);
        
    }
    
    @Override
    public void onRecyclerItemClick(View v, int position) {
        // 获取功能列表对应类
        final Class<?> function = Constants.FUCTIONS[position];
        // 点击跳转
        mActivity.startActivity(new Intent(mActivity, function));
    }
}
