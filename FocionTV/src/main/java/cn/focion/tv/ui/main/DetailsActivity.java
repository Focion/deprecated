package cn.focion.tv.ui.main;

import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import cn.focion.tv.Constants;
import cn.focion.tv.R;
import cn.focion.tv.adapter.main.MainAdapter;
import cn.focion.tv.model.Show;
import cn.focion.tv.tool.util.Images;
import cn.focion.tv.tool.util.Strings;
import cn.focion.tv.tool.util.UIs;
import cn.focion.tv.ui.FocionActivity;
import cn.focion.tv.ui.FocionFragment;
import cn.focion.tv.ui.IOC;
import cn.focion.tv.widget.ShadowFrame;
import cn.focion.tv.widget.SlidingTabStrip;

import com.facebook.drawee.view.SimpleDraweeView;

/**
 * 剧集详情 Created by Pirate on 2015-05-04.
 */
public class DetailsActivity extends FocionActivity implements
                                                   ViewPager.OnPageChangeListener {
    
    @IOC(id = R.id.focion_toolbar)
    private Toolbar mToolBar;
    
    @IOC(id = R.id.focion_content)
    private ShadowFrame mShadowFrame;
    
    @IOC(id = R.id.detail_show_thumb)
    private SimpleDraweeView mDraweeView;
    
    @IOC(id = R.id.focion_tab_pager)
    private ViewPager mViewPager;
    
    @IOC(id = R.id.focion_tab)
    private SlidingTabStrip mTabStrip;
    
    @IOC(id = R.id.detail_tab_layout)
    private RelativeLayout mTabLayout;
    
    @IOC(id = R.id.image_layout)
    private RelativeLayout mImageLayout;
    
    @IOC(id = R.id.tab_background)
    private View mBackgroud;
    
    @IOC(id = R.id.detail_background)
    private View mDetailBackground;
    
    private Show show;
    
    private boolean isShadowShow = false;
    
    // 详情Fragment
    private ShowFragment mShowFragment;
    
    // 季Fragment
    private SeasonFragment mSeasonFragment;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_main_toolbar);
        // 配置ToolBar
        trySetupToolBar();
        // 设置Fragment
        trySetupTabStrip();
    }
    
    /**
     * 配置ToolBar
     */
    private void trySetupToolBar() {
        Intent intent = getIntent();
        // 获取数据
        show = (Show) intent.getSerializableExtra("Show");
        mToolBar.setTitle(Strings.title(show));
        // 透明
        int colorTrans = Color.TRANSPARENT;
        // 设置Title字体和背景
        mToolBar.setTitleTextColor(colorTrans);
        mToolBar.setBackgroundColor(colorTrans);
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // 设置阴影FramLayout的高度
        mShadowFrame.setShadowTopOffset(UIs.dipToPx(this, 56 + 48));
        // 设置阴影显示
        mShadowFrame.setShadowVisible(isShadowShow, isShadowShow);
        // 获取url
        Images.load(show.getFanart(), mDraweeView);
    }
    
    /**
     * Follow中Tab的参数设置
     */
    private void trySetupTabStrip() {
        // 创建详情和季列表Fragment
        ArrayList<FocionFragment> fragments = new ArrayList<>();
        // 创建界面
        mShowFragment = new ShowFragment();
        mSeasonFragment = new SeasonFragment();
        // 创建参数对象
        Bundle bundle = new Bundle();
        // 传参数给ShowFragment
        bundle.putSerializable("Show", show);
        mShowFragment.setArguments(bundle);
        // 新的参数对象
        bundle = new Bundle();
        // 封装SeasonFragment的参数
        bundle.putLong("show_id", show.getTrakt());
        // 标题和时区
        bundle.putString("title&timezone",
                         String.format("%s&%s",
                                       Strings.title(show),
                                       show.getTimezone()));
        mSeasonFragment.setArguments(bundle);
        // 详情和季
        fragments.add(mShowFragment);
        fragments.add(mSeasonFragment);
        // 创建适配器
        MainAdapter pagerAdapter = new MainAdapter(mFragmentManager, fragments);
        // 设置适配器
        mViewPager.setAdapter(pagerAdapter);
        // 设置Fragment缓存数量
        mViewPager.setOffscreenPageLimit(Constants.MAX_CACHE_SIZE);
        // 设置字体颜色和大小
        mTabStrip.setTextColorResource(R.color.white_clr);
        // 绑定ViewPager
        mTabStrip.setViewPager(mViewPager);
        // 设置监听
        mTabStrip.setOnPageChangeListener(this);
        // 初始移动
        ViewCompat.animate(mTabStrip)
                  .translationX(UIs.dipToPx(this, 56))
                  .setDuration(HEADER_HIDE_ANIM_DURATION + 100)
                  .start();
    }
    
    @Override
    public ShadowFrame shadowFrame() {
        return mShadowFrame;
    }
    
    /**
     * 设置Layout的高度
     * 
     * @param top
     *            高度
     */
    public void setLayoutTop(int top) {
        mTabLayout.setTop(top);
    }
    
    /**
     * 设置图片高度
     * 
     * @param top
     *            高度
     */
    public void setImageLayoutTop(int top) {
        mImageLayout.setTop(top);
    }
    
    /**
     * 设置ToolBar颜色
     * 
     * @param color
     *            颜色
     */
    public void setScrollColor(int color) {
        mDetailBackground.setBackgroundColor(color);
    }
    
    /**
     * 设置ToolBar颜色
     *
     * @param color
     *            颜色
     */
    public void setBackgroundColor(int color) {
        mToolBar.setBackgroundColor(color);
        mBackgroud.setBackgroundColor(color);
    }
    
    /**
     * 设置Tool文字
     * 
     * @param color
     *            颜色
     */
    public void setToolTextColor(int color) {
        mToolBar.setTitleTextColor(color);
    }
    
    /**
     * 设置阴影高度
     *
     * @param isShow
     *            true 显示 false 不显示
     */
    public void setShadowShow(boolean isShow) {
        if (isShadowShow != isShow) {
            // 设置阴影FramLayout的显示
            mShadowFrame.setShadowVisible(isShow, isShow);
            isShadowShow = isShow;
        }
    }
    
    @Override
    public void onPageScrolled(int position,
                               float positionOffset,
                               int positionOffsetPixels) {
    }
    
    @Override
    public void onPageSelected(int position) {
        int anim = 0;
        if (position == 0)
            anim = UIs.dipToPx(this, 56);
        else if (position == 1)
            anim = UIs.dipToPx(this, 36);
        else if (position == 2)
            anim = 0;
        ViewCompat.animate(mTabStrip)
                  .translationX(anim)
                  .setDuration(HEADER_HIDE_ANIM_DURATION + 100)
                  .start();
        int defaultSY = UIs.dipToPx(this, 203 - 48 - 56);
        // 设置置顶
        if (position == 0) {
            // 获取当前页面滑动的高度
            int seasonY = mSeasonFragment.getScroll();
            // 判断是否大于默认距离
            mShowFragment.setScroll(seasonY >= defaultSY ? defaultSY : seasonY);
        }
        else if (position == 1) {
            // 获取当前页面滑动的高度
            int showY = mShowFragment.getScroll();
            if (mSeasonFragment.seasonSize() <= 3) {
                // 滑动到顶部
                mSeasonFragment.setScroll(0);
                // 设置图片
                setImageLayoutTop(0);
                // 设置阴影
                setShadowShow(false);
                // 设置Layout高度
                setLayoutTop(UIs.dipToPx(this, 203 - 48));
                // 设置颜色
                setBackgroundColor(Color.TRANSPARENT);
                // 设置Title的颜色
                setScrollColor(Color.TRANSPARENT);
            }
            else
                // 判断是否大于默认距离
                mSeasonFragment.setScroll(showY >= defaultSY ? defaultSY
                                                            : showY);
        }
    }
    
    @Override
    public void onPageScrollStateChanged(int state) {
    }
}
