package cn.focion.tv.ui.search;

import java.util.ArrayList;
import java.util.Calendar;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cn.focion.tv.R;
import cn.focion.tv.adapter.follow.SearchAdapter;
import cn.focion.tv.adapter.follow.TrendAdapter;
import cn.focion.tv.temp.TShow;
import cn.focion.tv.tool.board.UpdateBoard;
import cn.focion.tv.tool.connect.Response;
import cn.focion.tv.tool.error.VolleyError;
import cn.focion.tv.tool.trakt.TraktJson;
import cn.focion.tv.tool.util.UIs;
import cn.focion.tv.ui.FocionFragment;
import cn.focion.tv.ui.IOC;
import cn.focion.tv.ui.follow.DetailActivity;
import cn.focion.tv.widget.RecyclerView;

/**
 * 搜索Fragment Created by Pirate on 2015-05-18.
 */
public class SearchFragment extends FocionFragment<SearchActivity> implements
                                                                  RecyclerView.LoadMoreListener,
                                                                  RecyclerView.ItemClickListener {
    
    @IOC(id = R.id.focion_hide_bar_recycler)
    private RecyclerView mRecyclerView;
    
    private SearchAdapter mAdapter;
    
    private ArrayList<TShow> datas;
    
    private int mCurrentPage;
    
    // 关键字
    private String key;
    
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        setContentView(R.layout.focion_hide_bar_list);
        return super.onCreateView(inflater, container, savedInstanceState);
    }
    
    @Override
    public void trySetupData() {
        // 滑动隐藏
        mActivity.autoHideBar(mRecyclerView, this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        // 数据
        datas = new ArrayList<>();
        mAdapter = new SearchAdapter(mActivity,
                                     R.layout.layout_search_item,
                                     datas);
        // 注册广播
        addAction(TraktJson.ACTION_SEARCH_SHOWS);
        // 设置Adapter
        mRecyclerView.setAdapter(mAdapter);
        // 设置点击监听
        mAdapter.setItemClickListener(this);
        // 无查询结果
        setEmptyText("输入剧集名称进行查询（暂时只支持英文）");
        setContentEmpty(true);
        setContentShown(true);
    }
    
    @Override
    public void setFragmentTopClearance() {
        mRecyclerView.setContentTopClearance(UIs.dipToPx(mActivity, 60));
    }
    
    /**
     * 更新数据
     * 
     * @param key
     *            关键字
     */
    public void updateData(String key) {
        // 首次加载
        this.key = key;
        // 设置页数
        mCurrentPage = 1;
        loadSearch(mCurrentPage);
    }
    
    @Override
    public void onLoadMore() {
        if (mAdapter.isLoad()) {
            mCurrentPage++;
            loadSearch(mCurrentPage);
        }
    }
    
    /**
     * 加载更多搜索结果
     *
     * @param currentPage
     *            当前页
     */
    private void loadSearch(int currentPage) {
        if (currentPage == 1)
            setContentShown(false);
        // 拿着key进行查询
        mFocion.traktGet.searchShows(key,
                                     currentPage,
                                     new Response.Listener<String>() {
                                         @Override
                                         public void onResponse(String response) {
                                             TraktJson.getInstance(mActivity)
                                                      .jsonTShows3(response);
                                         }
                                     },
                                     new Response.ErrorListener() {
                                         @Override
                                         public void onErrorResponse(VolleyError error) {
                                             Tips(error.errorMsg());
                                         }
                                     });
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public void onReceive(Intent intent) {
        switch (intent.getAction()) {
            case TraktJson.ACTION_SEARCH_SHOWS:
                ArrayList<TShow> tShows = (ArrayList<TShow>) intent.getSerializableExtra("TShows");
                // 判断是否有数据
                if (tShows.size() == 0) {
                    mAdapter.loadEnable();
                    Tips("没有更多数据");
                }
                else {
                    // 否则更新数据
                    datas.addAll(tShows);
                    mAdapter.notifyDataSetChanged();
                }
                // 第一次加载
                if (mCurrentPage == 1) {
                    // 关闭空提示
                    setContentEmpty(false);
                    setContentShown(true);
                }
                break;
        }
    }
    
    @Override
    public void onRecyclerItemClick(View v, int position) {
        // 查看详情
        final TShow data = datas.get(position);
        Intent intent = new Intent(mActivity, DetailActivity.class);
        intent.putExtra("TShow", data);
        mActivity.startActivity(intent);
    }
}
