package cn.focion.tv.ui.follow;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cn.focion.tv.R;
import cn.focion.tv.adapter.follow.PopularAdapter;
import cn.focion.tv.temp.TShow;
import cn.focion.tv.tool.connect.Response;
import cn.focion.tv.tool.error.VolleyError;
import cn.focion.tv.tool.trakt.TraktJson;
import cn.focion.tv.tool.util.UIs;
import cn.focion.tv.ui.FocionFragment;
import cn.focion.tv.ui.IOC;
import cn.focion.tv.widget.RecyclerView;

/**
 * 推荐 Created by Pirate on 2015/3/30.
 */
public class PopularFragment extends FocionFragment<FollowActivity> implements
                                                   RecyclerView.LoadMoreListener,
                                                   RecyclerView.ItemClickListener {
    
    @IOC(id = R.id.focion_hide_bar_recycler)
    private RecyclerView mRecyclerView;
    
    private PopularAdapter mAdapter;
    
    private ArrayList<TShow> datas;
    
    private int mCurrentPage = 1;
    
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        setContentView(R.layout.focion_hide_bar_list);
        return super.onCreateView(inflater, container, savedInstanceState);
    }
    
    @Override
    public void trySetupData() {
        // 设置滑动隐藏
        mActivity.autoHideBar(mRecyclerView, this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        datas = new ArrayList<>();
        mAdapter = new PopularAdapter(mActivity,
                                      R.layout.layout_follow_popular_item,
                                      datas);
        // 设置点击监听
        mAdapter.setItemClickListener(this);
        mRecyclerView.setAdapter(mAdapter);
        // 注册监听
        addAction(TraktJson.ACTION_FOLLOW_POPULAR_SHOWS);
        // 首次加载
        loadPopular(mCurrentPage);
    }
    
    @Override
    public void setFragmentTopClearance() {
        mRecyclerView.setContentTopClearance(UIs.dipToPx(mActivity, 56 + 48));
    }
    
    @Override
    public void onLoadMore() {
        if (mAdapter.isLoad()) {
            mCurrentPage++;
            loadPopular(mCurrentPage);
        }
    }
    
    /**
     * 分页加载流行数据
     * 
     * @param currentPage
     *            当前页
     */
    private void loadPopular(int currentPage) {
        mFocion.traktGet.popularShows(false,
                                      true,
                                      currentPage,
                                      new Response.Listener<String>() {
                                          
                                          @Override
                                          public void onResponse(String response) {
                                              // 获取数据
                                              TraktJson.getInstance(mActivity)
                                                       .jsonTShows(response);
                                          }
                                      },
                                      new Response.ErrorListener() {
                                          @Override
                                          public void onErrorResponse(VolleyError error) {
                                              Tips(error.getMessage());
                                              setContentShown(true);
                                          }
                                      });
    }
    
    @Override
    public void onRecyclerItemClick(View v, int position) {
        final TShow data = datas.get(position);
        Intent intent = new Intent(mActivity, DetailActivity.class);
        intent.putExtra("TShow", data);
        mActivity.startActivity(intent);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public void onReceive(Intent intent) {
        switch (intent.getAction()) {
            case TraktJson.ACTION_FOLLOW_POPULAR_SHOWS:
                ArrayList<TShow> tShows = (ArrayList<TShow>) intent.getSerializableExtra("TShows");
                // 判断是否有数据
                if (tShows.size() == 0) {
                    mAdapter.loadEnable();
                    Tips("没有更多数据");
                }
                else {
                    // 否则更新数据
                    datas.addAll(tShows);
                    mAdapter.notifyDataSetChanged();
                }
                // 第一次加载
                if (mCurrentPage == 1)
                    setContentShown(true);
                break;
        }
    }
}
