package cn.focion.tv.ui.episode;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cn.focion.tv.R;
import cn.focion.tv.model.Episode;
import cn.focion.tv.tool.trakt.Action;
import cn.focion.tv.tool.util.Dates;
import cn.focion.tv.tool.util.Images;
import cn.focion.tv.tool.util.Strings;
import cn.focion.tv.tool.util.UIs;
import cn.focion.tv.ui.FocionActivity;
import cn.focion.tv.ui.IOC;
import cn.focion.tv.widget.CircleButton;
import cn.focion.tv.widget.ShadowFrame;

import com.facebook.drawee.view.SimpleDraweeView;

/**
 * 集的Activity Created by Pirate on 2015-04-28.
 */
public class EpisodeActivity extends FocionActivity implements
                                                   Palette.PaletteAsyncListener,
                                                   View.OnClickListener {
    
    @IOC(id = R.id.focion_toolbar)
    private Toolbar mToolBar;
    
    @IOC(id = R.id.focion_content)
    private ShadowFrame mShadowFrame;
    
    @IOC(id = R.id.episode_fanart)
    private SimpleDraweeView mDraweeView;
    
    @IOC(id = R.id.episode_name)
    private TextView mEpisodeName;
    
    @IOC(id = R.id.episode_rating)
    private TextView mEpisodeRating;
    
    @IOC(id = R.id.episode_first_aired)
    private TextView mEpisodeFirst;
    
    @IOC(id = R.id.episode_watched)
    private CircleButton mEpisodeWatched;
    
    @IOC(id = R.id.episode_layout)
    private RelativeLayout mLayout;
    
    @IOC(id = R.id.toolbar_layout)
    private RelativeLayout mToolBarLayout;
    
    @IOC(id = R.id.episode_view)
    private View mEpisodeView;
    
    // 单个集
    private Episode episode;
    
    // 集的Fragment
    private EpisodeFragment mFragment;
    
    // 颜色值
    private int rgb;
    
    // 列表的时区
    private String timezone;
    
    // 判断是否是onCreate
    private boolean flag = true;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_episode_toolbar);
        // 配置ToolBar
        trySetupToolBar();
        // 设置Fragment
        trySetupFragment();
    }
    
    /**
     * 配置ToolBar
     */
    private void trySetupToolBar() {
        // 获取数据
        Intent intent = getIntent();
        // 获取集和时区
        episode = (Episode) intent.getSerializableExtra("Episode");
        timezone = intent.getStringExtra("timezone");
        // 设置标题
        mToolBar.setTitle(Strings.dealDisplay(episode.getSeason(),
                                              episode.getNumber()));
        // 设置颜色
        int colorTrans = Color.TRANSPARENT;
        // 标题字体和背景
        mToolBar.setTitleTextColor(colorTrans);
        mToolBar.setBackgroundColor(colorTrans);
        // 设置ToolBar
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // 添加广播事件
        addAction(Action.ACTION_UPDATE_WATCHLIST);
        // 设置点击监听
        mEpisodeWatched.setOnClickListener(this);
        // 更新数据
        updateData();
    }
    
    /**
     * 设置Fragment和阴影
     */
    private void trySetupFragment() {
        // 设置阴影FramLayout的高度
        mShadowFrame.setShadowTopOffset(height() + UIs.dipToPx(this, 56));
        // 设置阴影显示
        mShadowFrame.setShadowVisible(true, true);
        // Fragmet
        mFragment = new EpisodeFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("Episode", episode);
        mFragment.setArguments(bundle);
        // 设置侧滑栏的布局Fragment
        mFragmentManager.beginTransaction()
                        .replace(R.id.focion_frame, mFragment)
                        .commit();
    }
    
    @Override
    public void onClick(View v) {
        if (v.getId() != R.id.episode_watched)
            return;
        // 点击已看
        mFragment.setContentShown(false);
        // 加载数据
        mFocion.traktDB.queryNext(null, episode);
    }
    
    @Override
    public void onReceive(Intent intent) {
        switch (intent.getAction()) {
            case Action.ACTION_UPDATE_WATCHLIST:
                episode = (Episode) intent.getSerializableExtra("Episode");
                // 更新数据
                updateData();
                // 更新数据
                mFragment.updateEpisode(episode);
                break;
        }
    }
    
    /**
     * 更新数据
     */
    private void updateData() {
        // 图片
        Images.load(episode.getScreenshot(),
                    mDraweeView,
                    new Images.ImageLoadListener() {
                        @Override
                        public void loadBitmap(Bitmap bitmap) {
                            Palette.generateAsync(bitmap, EpisodeActivity.this);
                        }
                    });
        // 判断设置标题
        String episodeTitle = episode.getTitle();
        if (Strings.isEmpty(episodeTitle))
            episodeTitle = "未知";
        // 设置控件的值
        mEpisodeName.setText(episodeTitle);
        mEpisodeRating.setText(String.format("评分：%.1f", episode.getRating()));
        mEpisodeFirst.setText("首播：".concat(Dates.changeZone(timezone,
                                                            episode.getFirst_aired(),
                                                            Dates.WEEK_MM_DD_YYYY)));
    }
    
    /**
     * 设置Title的方法
     */
    public void setTextColor(int color) {
        mToolBar.setTitleTextColor(color);
    }
    
    /**
     * 设置Title
     */
    public void setText(String title) {
        mToolBar.setTitle(title);
    }
    
    /**
     * 获取控件高度
     * 
     * @return 控件高度
     */
    public int height() {
        UIs.measureView(mLayout);
        return mLayout.getMeasuredHeight();
    }
    
    /**
     * 获取图片高度
     */
    public void setDraweeView(int top) {
        mDraweeView.setTop(top);
    }
    
    /**
     * 设置高度
     */
    public void setToolBarLayout(int top) {
        mToolBarLayout.setTop(top);
    }
    
    /**
     * 设置背景的方法
     *
     * @param color
     *            颜色值
     */
    public void setBackgroundColor(int color) {
        mEpisodeView.setBackgroundColor(color);
    }
    
    /**
     * 设置ToolBar的背景色
     * 
     * @param color
     *            颜色
     */
    public void setToolBackground(int color) {
        mToolBar.setBackgroundColor(color);
    }
    
    /**
     * 设置阴影高度
     *
     * @param isShow
     *            true 显示 false 不显示
     */
    public void setShadowShow(boolean isShow) {
        if (isShow)
            mShadowFrame.setVisibility(View.VISIBLE);
        else
            mShadowFrame.setVisibility(View.INVISIBLE);
    }
    
    /**
     * 获取颜色值
     */
    public int rgb() {
        return rgb;
    }
    
    @Override
    public ShadowFrame shadowFrame() {
        return mShadowFrame;
    }
    
    @Override
    public void onGenerated(Palette palette) {
        rgb = UIs.rgbDark(palette);
        mLayout.setBackgroundColor(rgb);
    }
    
}
