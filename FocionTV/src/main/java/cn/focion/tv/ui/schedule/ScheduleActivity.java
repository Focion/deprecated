package cn.focion.tv.ui.schedule;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.DecelerateInterpolator;
import android.widget.DatePicker;
import android.widget.LinearLayout;

import cn.focion.tv.R;
import cn.focion.tv.Constants;
import cn.focion.tv.adapter.schedule.ScheduleAdapter;
import cn.focion.tv.tool.util.UIs;
import cn.focion.tv.ui.FocionActivity;
import cn.focion.tv.ui.IOC;
import cn.focion.tv.widget.ShadowFrame;
import cn.focion.tv.widget.SlidingTabStrip;

/**
 * 我的分类 Created by PS on 14/12/20.
 */
public class ScheduleActivity extends FocionActivity implements
                                                    ViewPager.OnPageChangeListener {
    
    @IOC(id = R.id.focion_tool_bar_layout)
    private LinearLayout mToolLayout;
    
    @IOC(id = R.id.focion_toolbar)
    private Toolbar mToolBar;
    
    @IOC(id = R.id.focion_content)
    private ShadowFrame mShadowFrame;
    
    @IOC(id = R.id.focion_tab_pager)
    private ViewPager mViewPager;
    
    @IOC(id = R.id.focion_tab)
    private SlidingTabStrip mTabStrip;
    
    private ScheduleAdapter mAdapter;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.focion_toolbar_tabstrip);
        // 设置ToolBar
        trySetupToolBar();
        // 初始化Tab
        trySetupTabStrip();
    }
    
    /**
     * 设置ToolBar
     */
    private void trySetupToolBar() {
        mToolBar.setTitle(R.string.drawer_schedule);
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    
    /**
     * Follow中Tab的参数设置
     */
    private void trySetupTabStrip() {
        // 创建适配器
        mAdapter = new ScheduleAdapter(mFragmentManager);
        // 设置适配器
        mViewPager.setAdapter(mAdapter);
        // 设置Fragment缓存数量
        mViewPager.setOffscreenPageLimit(Constants.MAX_CACHE_SIZE);
        // 设置字体颜色和大小
        mTabStrip.setTextColorResource(R.color.white_clr);
        // 计算间距
        int offsetPadding = UIs.dipToPx(this, 16);
        // 设置间距
        mTabStrip.setTabPaddingLeftRight(offsetPadding);
        // 设置滑动
        mViewPager.setCurrentItem(7);
        // 绑定ViewPager
        mTabStrip.setViewPager(mViewPager);
        // 设置监听
        mTabStrip.setOnPageChangeListener(this);
    }
    
    @Override
    public ShadowFrame shadowFrame() {
        return mShadowFrame;
    }
    
    @Override
    protected void autoShowOrHide(boolean isShow) {
        // mToolBar的隐藏
        if (isShow) {
            mToolLayout.animate()
                       .translationY(0)
                       .setDuration(HEADER_HIDE_ANIM_DURATION)
                       .setInterpolator(new DecelerateInterpolator());
            mShadowFrame.setShadowTopOffset(UIs.dipToPx(this, 56 + 48));
        }
        else {
            mToolLayout.animate()
                       .translationY(-mToolBar.getBottom())
                       .setDuration(HEADER_HIDE_ANIM_DURATION)
                       .setInterpolator(new DecelerateInterpolator());
            mShadowFrame.setShadowTopOffset(UIs.dipToPx(this, 48));
        }
        // 设置阴影是否显示
        mShadowFrame.setShadowVisible(true, true);
    }
    
    @Override
    public void onPageScrolled(int position,
                               float positionOffset,
                               int positionOffsetPixels) {
    }
    
    @Override
    public void onPageSelected(int position) {
        int anim = 0;
        if (position == 0)
            anim = UIs.dipToPx(this, 72);
        else if (position == 1)
            anim = UIs.dipToPx(this, 36);
        else if (position == 2)
            anim = 0;
        ViewCompat.animate(mTabStrip)
                  .translationX(anim)
                  .setDuration(HEADER_HIDE_ANIM_DURATION + 100)
                  .start();
    }
    
    @Override
    public void onPageScrollStateChanged(int state) {
        
    }
}
