package cn.focion.tv.ui.dialog;

import java.lang.reflect.Field;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import cn.focion.tv.ui.IOC;

/**
 * 公共弹出Dialog Created by Pirate on 14-6-19.
 */
public class FocionDialog extends Dialog implements View.OnClickListener {
    
    public Context mContext;
    
    public FocionDialog(Context context) {
        super(context);
        mContext = context;
        
    }
    
    public FocionDialog(Context context, int theme) {
        super(context, theme);
    }
    
    protected FocionDialog(Context context,
                           boolean cancelable,
                           OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // 取标题
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
    }
    
    @Override
    public void setContentView(int layoutId) {
        super.setContentView(layoutId);
        // 初始化标记控件
        initView();
    }
    
    /**
     * 初始化所有控件
     */
    private void initView() {
        // 获取所有字段，包括私有字段，但不包括父类字段
        Field[] fields = getClass().getDeclaredFields();
        if (fields != null && fields.length > 0) { // 判断字段是否为空
            for (Field field : fields) {
                try {
                    // 设置字段可编辑
                    field.setAccessible(true);
                    
                    if (field.get(this) != null)
                        continue;
                    // 获取标记为IOC注解的字段
                    IOC annoView = field.getAnnotation(IOC.class);
                    if (annoView != null) { // 如果不为空
                        // 获取注解的ID
                        int annoId = annoView.id();
                        field.set(this, findViewById(annoId)); // 获取对象并设置
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    /**
     * 全局吐司提示
     *
     * @param msg
     *            提示信息
     */
    public void Tips(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }
    
    @Override
    public void onClick(View v) {
        
    }
}
