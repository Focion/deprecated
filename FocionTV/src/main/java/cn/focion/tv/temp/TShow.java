package cn.focion.tv.temp;

import java.io.Serializable;

/**
 * 剧 Created by Pirate on 2015/3/26.
 */
public class TShow implements Serializable {
    
    // trakt的ID
    public long ids_trakt;
    
    // 对应查询名字
    public String ids_slug;
    
    // 剧名字
    public String title;
    
    // 播出时间
    public int year;
    
    // 横图
    public String fanart_thumb;
    
    // 竖图
    public String poster_thumb;
    
    // 横条图
    public String banner_full;
    
    // 观看人数
    public int watchers;
}
