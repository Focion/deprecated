package cn.focion.tv.temp;

/**
 * 集 Created by Pirate on 2015/3/26.
 */
public class Episode {
    
    // trakt的ID
    public long ids_trakt;
    
    // 季
    public int season;
    
    // 集
    public int number;
    
    // 标题
    public String title;
    
    public String number_abs;
    
    // 简介
    public String overview;
    
    // 评分
    public float rating;
    
    // 投票
    public long votes;
    
    // 首播
    public String first_aired;
    
    // 已播出至
    public String updated_at;
    
    // 截图
    public String screenshot_thumb;
}
