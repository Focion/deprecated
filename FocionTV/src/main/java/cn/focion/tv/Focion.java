package cn.focion.tv;

import java.io.File;

import android.app.Application;

import cn.focion.tv.model.DaoMaster;
import cn.focion.tv.model.DaoSession;
import cn.focion.tv.tool.connect.RequestQueue;
import cn.focion.tv.tool.connect.Volley;
import cn.focion.tv.tool.trakt.TraktDB;
import cn.focion.tv.tool.trakt.TraktGet;
import cn.focion.tv.tool.util.Files;

import com.facebook.cache.disk.DiskCacheConfig;
import com.facebook.common.internal.Supplier;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipelineConfig;

/**
 * 全局 Created by Pirate on 2015/3/28.
 */
public class Focion extends Application {
    
    public TraktGet traktGet;
    
    public TraktDB traktDB;
    
    public DaoSession session;
    
    @Override
    public void onCreate() {
        super.onCreate();
        // 创建线程池
        RequestQueue queue = Volley.newRequestQueue(this);
        // Trakts访问
        traktGet = TraktGet.getInstance(this, queue);
        // Fresco加载
        trySetupFresco();
        // 创建数据库Session
        trySetupDaoSession();
    }
    
    /**
     * 创建图片缓存工具
     */
    private void trySetupFresco() {
        // 判断SD卡是否存在
        if (Files.isExistSD()) {
            // 设置Supplier
            Supplier<File> supplier = new Supplier<File>() {
                @Override
                public File get() {
                    // 设置缓存文件位置
                    return Files.imageCacheFile(Focion.this);
                }
            };
            // 磁盘缓存设置
            DiskCacheConfig cacheConfig = DiskCacheConfig.newBuilder(this)
                                                         .setBaseDirectoryName("images")
                                                         .setBaseDirectoryPathSupplier(supplier)
                                                         .setMaxCacheSize(100 * 1024 * 1024)
                                                         .build();
            // 图片配置
            ImagePipelineConfig config = ImagePipelineConfig.newBuilder(this)
                                                            .setMainDiskCacheConfig(cacheConfig)
                                                            .build();
            Fresco.initialize(this, config);
        }
        else
            Fresco.initialize(this);
    }
    
    /**
     * 初始化会话
     */
    private void trySetupDaoSession() {
        DaoMaster.OpenHelper helper = new DaoMaster.DevOpenHelper(this,
                                                                  Constants.DATABASE_NAME,
                                                                  null);
        DaoMaster daoMaster = new DaoMaster(helper.getWritableDatabase());
        session = daoMaster.newSession();
        traktDB = TraktDB.getInstance(this, session);
    }
}
