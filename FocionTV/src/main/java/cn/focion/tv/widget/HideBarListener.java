package cn.focion.tv.widget;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * 控制隐藏ActionBar
 */
public abstract class HideBarListener extends RecyclerView.OnScrollListener {
    
    private static final int HIDE_THRESHOLD = 20;
    
    private int mScrolledDistance = 0;
    
    private boolean mControlsVisible = true;
    
    int lastVisibleItem = 0;
    
    private cn.focion.tv.widget.RecyclerView.LoadMoreListener mListener;
    
    public HideBarListener(cn.focion.tv.widget.RecyclerView.LoadMoreListener listener) {
        mListener = listener;
    }
    
    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        
        super.onScrolled(recyclerView, dx, dy);
        if (mListener != null)
            lastVisibleItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
        
        int firstVisibleItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
        
        if (firstVisibleItem == 0) {
            if (!mControlsVisible) {
                onShow();
                mControlsVisible = true;
            }
        }
        else {
            if (mScrolledDistance > HIDE_THRESHOLD && mControlsVisible) {
                onHide();
                mControlsVisible = false;
                mScrolledDistance = 0;
            }
            else if (mScrolledDistance < -HIDE_THRESHOLD && !mControlsVisible) {
                onShow();
                mControlsVisible = true;
                mScrolledDistance = 0;
            }
        }
        if ((mControlsVisible && dy > 0) || (!mControlsVisible && dy < 0)) {
            mScrolledDistance += dy;
        }
        
    }
    
    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
        if (mListener != null && newState == RecyclerView.SCROLL_STATE_IDLE
            && lastVisibleItem + 1 == recyclerView.getAdapter().getItemCount())
            mListener.onLoadMore();
    }
    
    public abstract void onHide();
    
    public abstract void onShow();
}
