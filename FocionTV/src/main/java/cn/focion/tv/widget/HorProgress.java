package cn.focion.tv.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.View;

import cn.focion.tv.R;

/**
 * 长方形进度View类 Created by Pirate on 14-6-23.
 */
@SuppressLint("ALL")
public class HorProgress extends View {
    
    // 最大
    private float max = 10.0f;
    
    // 进度
    private float progress = 0.0f;
    
    // 绘制底色
    private Paint mPaint = null;
    
    // 底色
    private int rgbColorDown;
    
    // 填充填充
    private Paint mUpPaint = null;
    
    // 填充色
    private int rgbColorUp;
    
    // 宽度
    private int progressWidth = 4;
    
    public HorProgress(Context context) {
        super(context);
    }
    
    public HorProgress(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray attributes = context.obtainStyledAttributes(attrs,
                                                               R.styleable.HoProgress);
        progressWidth = attributes.getDimensionPixelSize(R.styleable.HoProgress_hpWidth,
                                                         R.dimen.hor_progress);
        rgbColorDown = attributes.getColor(R.styleable.HoProgress_hpColorDown,
                                           R.color.bg_clr);
        rgbColorUp = attributes.getColor(R.styleable.HoProgress_hpColorUp,
                                         R.color.red_clr);
        mPaint = new Paint();
        // 设置是否抗锯齿
        mPaint.setAntiAlias(true);
        // 消除锯齿
        mPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        // 设置中空的样式
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setDither(true);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        
        mUpPaint = new Paint();
        // 设置是否抗锯齿
        mUpPaint.setAntiAlias(true);
        // 帮助消除锯齿
        mUpPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        // 设置中空的样式
        mUpPaint.setStyle(Paint.Style.FILL);
        mUpPaint.setDither(true);
        mUpPaint.setStrokeJoin(Paint.Join.ROUND);
        
    }
    
    public HorProgress(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int width = getMeasuredWidth();
        int height = getMeasuredHeight();
        
        // 设置画笔颜色
        mPaint.setColor(rgbColorDown);
        // 设置画笔宽度
        mPaint.setStrokeWidth(progressWidth);
        // 绘制
        canvas.drawRect(0, 0, width, height, mPaint);
        // 线性渲染对象
        LinearGradient linearGradient = new LinearGradient(0,
                                                           0,
                                                           width,
                                                           height,
                                                           rgbColorUp,
                                                           rgbColorUp,
                                                           Shader.TileMode.CLAMP);
        mUpPaint.setShader(linearGradient);
        // 设置线的类型,边是圆的
        mUpPaint.setStrokeCap(Paint.Cap.ROUND);
        // 设置线的宽度
        mUpPaint.setStrokeWidth(progressWidth);
        // 绘制矩形
        canvas.drawRect(0, 0, (progress / max) * width, height, mUpPaint);
    }
    
    /**
     * 设置进度
     * 
     * @param progress
     *            进度数值
     */
    public void setProgress(float progress) {
        this.progress = progress;
        this.invalidate();
    }
    
    /**
     * 设置最大值
     * 
     * @param max
     *            最大值
     */
    public void setMax(float max) {
        this.max = max;
    }
    
    /**
     * 设置进度条宽度
     * 
     * @param progressWidth
     *            宽度
     */
    public void setProgressWidth(int progressWidth) {
        this.progressWidth = progressWidth;
    }
}
