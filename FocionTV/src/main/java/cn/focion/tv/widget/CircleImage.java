package cn.focion.tv.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.widget.ImageView;

import cn.focion.tv.R;
import cn.focion.tv.tool.util.UIs;

/**
 * 圆角ImageView Created by Pirate on 14-8-11.
 */
public class CircleImage extends ImageView {
    
    /**
     * 圆角半径
     */
    private int roundW;
    
    private int roundH;
    
    /**
     * 画笔
     */
    private Paint paintF;
    
    private Paint paintS;
    
    /**
     * 上下角
     */
    private boolean up;
    
    /**
     * ImageView构造器
     * 
     * @param context
     *            上下文
     */
    public CircleImage(Context context) {
        super(context);
        init(context, null);
    }
    
    public CircleImage(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }
    
    public CircleImage(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }
    
    /**
     * 初始化属性
     * 
     * @param context
     *            上下文
     * @param attrs
     *            属性
     */
    private void init(Context context, AttributeSet attrs) {
        
        if (attrs != null) {
            TypedArray array = context.obtainStyledAttributes(attrs,
                                                              R.styleable.CircleImage);
            roundW = array.getDimensionPixelSize(R.styleable.CircleImage_roundW,
                                                 roundW);
            roundH = array.getDimensionPixelSize(R.styleable.CircleImage_roundH,
                                                 roundH);
            up = array.getBoolean(R.styleable.CircleImage_up, true);
        }
        else {
            int roundPx = UIs.dipToPx(context, 2);
            roundW = roundPx;
            roundH = roundPx;
            up = true;
        }
        
        paintF = new Paint();
        paintF.setColor(Color.WHITE);
        paintF.setAntiAlias(true);
        paintF.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
        
        paintS = new Paint();
        paintS.setXfermode(null);
    }
    
    @Override
    public void draw(@NonNull Canvas canvas) {
        Bitmap bitmap = Bitmap.createBitmap(getWidth(),
                                            getHeight(),
                                            Bitmap.Config.ARGB_8888);
        Canvas canvasBitmap = new Canvas(bitmap);
        super.draw(canvasBitmap);
        // 绘制左上角
        drawLeftUp(canvasBitmap);
        // 绘制右下角
        drawRightUp(canvasBitmap);
        if (!up) {
            // 绘制左下角
            drawLeftDown(canvasBitmap);
            // 绘制右下角
            drawRightDown(canvasBitmap);
        }
        // 绘制Bitmap
        canvas.drawBitmap(bitmap, 0, 0, paintS);
        // 回收
        bitmap.recycle();
    }
    
    /**
     * 绘制左上角
     */
    private void drawLeftUp(Canvas canvas) {
        Path path = new Path();
        path.moveTo(0, roundH);
        path.lineTo(0, 0);
        path.lineTo(roundW, 0);
        path.arcTo(new RectF(0, 0, roundW * 2, roundH * 2), -90, -90);
        path.close();
        canvas.drawPath(path, paintF);
    }
    
    /**
     * 绘制左下角
     */
    private void drawLeftDown(Canvas canvas) {
        Path path = new Path();
        path.moveTo(0, getHeight() - roundH);
        path.lineTo(0, getHeight());
        path.lineTo(roundW, getHeight());
        path.arcTo(new RectF(0,
                             getHeight() - roundH * 2,
                             roundW * 2,
                             getHeight()), 90, 90);
        path.close();
        canvas.drawPath(path, paintF);
    }
    
    /**
     * 绘制又下角
     */
    private void drawRightDown(Canvas canvas) {
        Path path = new Path();
        path.moveTo(getWidth() - roundW, getHeight());
        path.lineTo(getWidth(), getHeight());
        path.lineTo(getWidth(), getHeight() - roundH);
        path.arcTo(new RectF(getWidth() - roundW * 2,
                             getHeight() - roundH * 2,
                             getWidth(),
                             getHeight()), 0, 90);
        path.close();
        canvas.drawPath(path, paintF);
    }
    
    /**
     * 绘制右上角
     */
    private void drawRightUp(Canvas canvas) {
        Path path = new Path();
        path.moveTo(getWidth(), roundH);
        path.lineTo(getWidth(), 0);
        path.lineTo(getWidth() - roundW, 0);
        path.arcTo(new RectF(getWidth() - roundW * 2, 0, getWidth(), roundH * 2),
                   -90,
                   90);
        path.close();
        canvas.drawPath(path, paintF);
    }
    
}
