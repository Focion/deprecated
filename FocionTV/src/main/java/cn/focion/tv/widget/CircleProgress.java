package cn.focion.tv.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.SweepGradient;
import android.util.AttributeSet;
import android.view.View;

import cn.focion.tv.R;

/**
 * 环形分数控件类 Created by Pirate on 14-6-19.
 */
public class CircleProgress extends View {
    
    // 默认圆的半径
    private int radius = 0;
    
    // 最大
    private float max = 10.0f;
    
    // 进度
    private float progress = 0.0f;
    
    // 绘制底色
    private Paint mPaint = null;
    
    // 底色
    private int rgbColorDown;
    
    // 填充填充
    private Paint mUpPaint = null;
    
    // 填充色
    private int rgbColorUp;
    
    // 环形宽度
    private int circleWidth = 0;
    
    // 几何图形绘制
    private RectF mRectF;
    
    public CircleProgress(Context context) {
        super(context);
    }
    
    public CircleProgress(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray attributes = context.obtainStyledAttributes(attrs,
                                                               R.styleable.CirProgress);
        radius = attributes.getDimensionPixelSize(R.styleable.CirProgress_cpRadius,
                                                  R.dimen.circle_radius);
        circleWidth = attributes.getDimensionPixelSize(R.styleable.CirProgress_cpCirWidth,
                                                       R.dimen.circle_width);
        rgbColorDown = attributes.getColor(R.styleable.CirProgress_cpColorDown,
                                           R.color.bg_clr);
        rgbColorUp = attributes.getColor(R.styleable.CirProgress_cpColorUp,
                                         R.color.red_clr);
        mPaint = new Paint();
        // 设置是否抗锯齿
        mPaint.setAntiAlias(true);
        // 消除锯齿
        mPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        // 设置中空的样式
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setDither(true);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        
        mUpPaint = new Paint();
        // 设置是否抗锯齿
        mUpPaint.setAntiAlias(true);
        // 帮助消除锯齿
        mUpPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        // 设置中空的样式
        mUpPaint.setStyle(Paint.Style.STROKE);
        mUpPaint.setDither(true);
        mUpPaint.setStrokeJoin(Paint.Join.ROUND);
        
        mRectF = new RectF();
    }
    
    public CircleProgress(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        
    }
    
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int height = MeasureSpec.getSize(heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        setMeasuredDimension(width, height);
    }
    
    @SuppressLint("DrawAllocation")
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // 获取控件宽高
        int width = getMeasuredWidth();
        int height = getMeasuredHeight();
        // 计算内圆半径
        this.radius = getMeasuredWidth() / 2 - circleWidth;
        // 设置画笔颜色和宽度
        mPaint.setColor(rgbColorDown);
        mPaint.setStrokeWidth(circleWidth);
        // 绘制
        canvas.drawCircle(width / 2, height / 2, radius, mPaint);
        // 环形颜色填充
        SweepGradient sweepGradient = new SweepGradient(width / 2,
                                                        height / 2,
                                                        rgbColorUp,
                                                        rgbColorUp);
        // 设置填充色
        mUpPaint.setShader(sweepGradient);
        // 设置线的类型,边是圆的
        mUpPaint.setStrokeCap(Paint.Cap.ROUND);
        // 设置画笔宽度
        mUpPaint.setStrokeWidth(circleWidth);
        // 设置类似于左上角坐标，右下角坐标
        mRectF.set(width / 2 - radius,
                   height / 2 - radius,
                   width / 2 + radius,
                   height / 2 + radius);
        /**
         * 画圆弧 1-绘制圆形参数 2-起始角度 3-跨度（绘制角度） 4-是否空心 true 实心 false 空心 5-画笔
         */
        canvas.drawArc(mRectF, -90, (progress / max) * 360, false, mUpPaint);
    }
    
    /**
     * 设置环形宽度
     * 
     * @param circleWidth
     *            宽度 Px单位
     */
    public void setCircleWidth(int circleWidth) {
        this.circleWidth = circleWidth;
    }
    
    /**
     * 设置圆半径
     * 
     * @param radius
     *            半径 Px单位
     */
    public void setRadius(int radius) {
        this.radius = radius;
    }
    
    /**
     * 设置最大数值
     * 
     * @param max
     *            最大数值
     */
    public void setMax(float max) {
        this.max = max;
    }
    
    /**
     * 设置填充数值
     * 
     * @param progress
     *            填充数值
     */
    public void setProgress(float progress) {
        this.progress = progress;
    }
}
