package cn.focion.tv.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

import java.util.ArrayList;

/**
 * 自定义ScrollView
 */
public class ObsScrollView extends ScrollView {
    
    private ArrayList<Callbacks> mCallbacks = new ArrayList<Callbacks>();
    
    private ArrayList<CallbacksDelta> mCallbacksDelta = new ArrayList<CallbacksDelta>();
    
    private int mContentTopClearance = 0;
    
    public ObsScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    
    public void setContentTopClearance(int clearance) {
        if (mContentTopClearance != clearance) {
            mContentTopClearance = clearance;
            setPadding(getPaddingLeft(),
                       mContentTopClearance,
                       getPaddingRight(),
                       getPaddingBottom());
        }
    }
    
    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        for (Callbacks c : mCallbacks)
            c.onScrollChanged(l, t);
        
        for (CallbacksDelta callbacksDelta : mCallbacksDelta)
            callbacksDelta.onScrollChangedDelta(l - oldl, t - oldt);
    }
    
    @Override
    public int computeVerticalScrollRange() {
        return super.computeVerticalScrollRange();
    }
    
    public void addCallbacks(Callbacks listener) {
        if (!mCallbacks.contains(listener))
            mCallbacks.add(listener);
    }
    
    public void addCallbacksDelta(CallbacksDelta listener) {
        if (!mCallbacksDelta.contains(listener))
            mCallbacksDelta.add(listener);
    }
    
    public interface Callbacks {
        void onScrollChanged(int scrollX, int scrollY);
    }
    
    public interface CallbacksDelta {
        void onScrollChangedDelta(int deltaX, int deltaY);
    }
}
