package cn.focion.tv.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

/**
 * 列表 Created by Pirate on 2015/3/28.
 */
public class RecyclerView extends android.support.v7.widget.RecyclerView {
    
    private int mContentTopClearance = 0;
    
    public RecyclerView(Context context) {
        super(context);
    }
    
    public RecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    
    public RecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    
    public void setContentTopClearance(int clearance) {
        if (mContentTopClearance != clearance) {
            mContentTopClearance = clearance;
            setPadding(getPaddingLeft(),
                       mContentTopClearance,
                       getPaddingRight(),
                       getPaddingBottom());
        }
    }
    
    public interface LoadMoreListener {
        void onLoadMore();
    }
    
    public interface ItemClickListener {
        void onRecyclerItemClick(View v, int position);
    }
}
