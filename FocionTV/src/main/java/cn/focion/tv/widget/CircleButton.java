package cn.focion.tv.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import cn.focion.tv.R;

/**
 * 原型Btn
 */
public class CircleButton extends View {
    
    private static final float SHADOW_RADIUS = 3.0f;
    
    private static final float SHADOW_OFFSET_X = 0.0f;
    
    private static final float SHADOW_OFFSET_Y = 2.0f;
    
    private Context mContext;
    
    private int mButtonWidth;
    
    private int mButtonHeight;
    
    private int mColor;
    
    private int mColorPress;
    
    private Bitmap mIcon;
    
    private Rect mFingerRect;
    
    private boolean mMoveOutside;
    
    private Paint circlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    
    private Paint iconPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    
    public CircleButton(Context context) {
        this(context, null);
    }
    
    public CircleButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }
    
    public CircleButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        TypedArray attributes = context.obtainStyledAttributes(attrs,
                                                               R.styleable.CirButton);
        mColor = attributes.getColor(R.styleable.CirButton_cirColor,
                                     getResources().getColor(R.color.white_clr));
        mColorPress = attributes.getColor(R.styleable.CirButton_cirShadowColorPress,
                                          getResources().getColor(R.color.red_press_clr));
        mButtonWidth = attributes.getDimensionPixelSize(R.styleable.CirButton_cirRadius,
                                                        R.dimen.circle_width);
        mButtonHeight = attributes.getDimensionPixelSize(R.styleable.CirButton_cirRadius,
                                                         R.dimen.circle_height);
        float shadowRadius = attributes.getFloat(R.styleable.CirButton_cirShadowRadius,
                                                 SHADOW_RADIUS);
        float shadowOffsetX = attributes.getFloat(R.styleable.CirButton_cirShadowOffsetX,
                                                  SHADOW_OFFSET_X);
        float shadowOffsetY = attributes.getFloat(R.styleable.CirButton_cirShadowOffsetY,
                                                  SHADOW_OFFSET_Y);
        int shadowColor = attributes.getColor(R.styleable.CirButton_cirShadowColor,
                                              getResources().getColor(R.color.black80_clr));
        Drawable drawable = attributes.getDrawable(R.styleable.CirButton_cirIcon);
        if (drawable != null) {
            mIcon = ((BitmapDrawable) drawable).getBitmap();
        }
        attributes.recycle();
        circlePaint.setColor(mColor);
        circlePaint.setStyle(Paint.Style.FILL);
        circlePaint.setShadowLayer(shadowRadius,
                                   shadowOffsetX,
                                   shadowOffsetY,
                                   shadowColor);
        setWillNotDraw(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
    }
    
    private int darkenColor(int color) {
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        hsv[2] *= 0.5f;
        return Color.HSVToColor(hsv);
    }
    
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int size;
        int widthSpecMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSpecSize = MeasureSpec.getSize(heightMeasureSpec);
        if (widthSpecMode == MeasureSpec.EXACTLY) {
            if (widthSpecSize < mButtonWidth) {
                size = mButtonWidth;
            }
            else {
                size = widthSpecSize;
            }
        }
        else {
            size = mButtonWidth;
        }
        widthMeasureSpec = MeasureSpec.makeMeasureSpec(size,
                                                       MeasureSpec.EXACTLY);
        int heightSpecMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSpecSize = MeasureSpec.getSize(heightMeasureSpec);
        if (heightSpecMode == MeasureSpec.EXACTLY) {
            if (heightSpecSize < mButtonHeight) {
                size = mButtonHeight;
            }
            else {
                size = heightSpecSize;
            }
        }
        else {
            size = mButtonHeight;
        }
        heightMeasureSpec = MeasureSpec.makeMeasureSpec(size,
                                                        MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
    
    @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mMoveOutside = false;
                mFingerRect = new Rect(getLeft(),
                                       getTop(),
                                       getRight(),
                                       getBottom());
                circlePaint.setColor(mColorPress);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                if (!mFingerRect.contains(getLeft() + (int) event.getX(),
                                          getTop() + (int) event.getY())) {
                    mMoveOutside = true;
                    circlePaint.setColor(mColor);
                    invalidate();
                }
                break;
            case MotionEvent.ACTION_UP:
                circlePaint.setColor(mColor);
                invalidate();
                if (!mMoveOutside) {
                    performClick();
                }
                break;
            case MotionEvent.ACTION_CANCEL:
                circlePaint.setColor(mColor);
                invalidate();
                break;
        }
        return true;
    }
    
    @Override
    protected void onDraw(@NonNull Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawCircle(getWidth() / 2,
                          getHeight() / 2,
                          (float) (getWidth() / 2.6),
                          circlePaint);
        if (mIcon != null) {
            float x = (getWidth() - mIcon.getWidth()) / 2;
            float y = (getHeight() - mIcon.getHeight()) / 2;
            canvas.drawBitmap(mIcon, x, y, iconPaint);
        }
    }
    
    /**
     * 设置图标
     * 
     * @param resId
     *            资源ID
     */
    public void setIcon(int resId) {
        Drawable drawable = mContext.getResources().getDrawable(resId);
        if (drawable != null)
            mIcon = ((BitmapDrawable) drawable).getBitmap();
    }
}
