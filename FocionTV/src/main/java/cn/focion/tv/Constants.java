package cn.focion.tv;

import cn.focion.tv.ui.follow.FollowActivity;

/**
 * 公共常量类 Created by Pirate on 14-5-7.
 */
public class Constants {
    
    /**
     * APP名字
     */
    public static final String APP_NAME = "Focion.TV";
    
    /**
     * 最大Fragment缓存数量
     */
    public static final int MAX_CACHE_SIZE = 5;
    
    /**
     * Drawer的功能列表
     */
    public static final int[] FUCTION_LIST = new int[] { R.string.drawer_add,
                                                        R.string.drawer_category,
                                                        R.string.drawer_schedule,
                                                        R.string.drawer_time,
                                                        R.string.drawer_notify,
                                                        R.string.drawer_setting };
    
    /**
     * Drawer的功能对应Class
     */
    public static final Class<?>[] FUCTIONS = new Class[] { FollowActivity.class,
                                                           null,
                                                           null,
                                                           null,
                                                           null,
                                                           null };
    
    /**
     * Drawer的功能图片
     */
    public static final int[] FUCTION_IMG = new int[] { R.mipmap.ic_add_show,
                                                       R.mipmap.ic_action_category,
                                                       R.mipmap.ic_action_alarm,
                                                       R.mipmap.ic_action_schedule,
                                                       R.mipmap.ic_action_notify,
                                                       R.mipmap.ic_action_setting };
    
    /**
     * 数据库名 默认focion
     */
    public static final String DATABASE_NAME = "focion_tv.db";
    
    /**
     * 列表名称改变
     */
    public static String RESULT = "默认";
    
    /**
     * 一些基本颜色
     */
    public static final String TITLE_COLOR = "#2B3740";
}
