package cn.focion.tv.adapter.follow;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;

import cn.focion.tv.R;
import cn.focion.tv.adapter.FocionAdapter;
import cn.focion.tv.temp.TShow;
import cn.focion.tv.ui.follow.DetailActivity;

/**
 * 排行适配器 Created by Pirate on 2015/3/30.
 */
public class PopularAdapter extends FocionAdapter {
    
    private ArrayList<TShow> datas;
    
    /**
     * 父类构造
     *
     * @param context
     *            上下文
     * @param itemLayout
     *            布局
     */
    public PopularAdapter(Context context,
                          int itemLayout,
                          ArrayList<TShow> datas) {
        super(context, itemLayout, true);
        this.datas = datas;
    }
    
    @Override
    public ViewHolder newHolder(View itemView) {
        ViewHolder holder = new ViewHolder(itemView);
        holder.holder(R.id.item_show_img);
        holder.holder(R.id.item_show_name);
        holder.holder(R.id.item_show_year);
        return holder;
    }
    
    @Override
    protected void onBindHolder(ViewHolder holder, int position) {
        final TShow data = datas.get(position);
        SimpleDraweeView imageView = holder.holder(R.id.item_show_img,
                                                   SimpleDraweeView.class);
        Uri uri = Uri.parse(data.fanart_thumb);
        imageView.setImageURI(uri);
        holder.holder(R.id.item_show_name, TextView.class).setText(data.title);
        holder.holder(R.id.item_show_year, TextView.class)
              .setText(String.format("%d", data.year));
    }
    
    @Override
    protected int getCount() {
        return datas.size();
    }
}
