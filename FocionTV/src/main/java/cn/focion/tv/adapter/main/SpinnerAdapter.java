package cn.focion.tv.adapter.main;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import cn.focion.tv.Constants;
import cn.focion.tv.R;

/**
 * Spinner适配器 Created by Pirate on 14/11/28.
 */
public class SpinnerAdapter extends BaseAdapter {
    
    private Context mContext;
    
    public SpinnerAdapter(Context context) {
        mContext = context;
    }
    
    @Override
    public int getCount() {
        return 5;
    }
    
    @Override
    public Object getItem(int position) {
        return null;
    }
    
    @Override
    public long getItemId(int position) {
        return position;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = View.inflate(mContext,
                                       R.layout.focion_spinner_tool_bar,
                                       null);
        TextView categoryNameTv = (TextView) convertView.findViewById(R.id.focion_spinner_tool_text);
        categoryNameTv.setText("默认");
        return convertView;
    }
    
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = View.inflate(mContext,
                                       R.layout.focion_spinner_item,
                                       null);
        TextView categoryNameTv = (TextView) convertView.findViewById(R.id.focion_spinner_text);
        categoryNameTv.setText(Constants.RESULT);
        return convertView;
    }
}
