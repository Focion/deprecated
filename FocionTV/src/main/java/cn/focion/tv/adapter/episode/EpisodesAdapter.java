package cn.focion.tv.adapter.episode;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cn.focion.tv.R;
import cn.focion.tv.adapter.FocionAdapter;
import cn.focion.tv.model.Episode;
import cn.focion.tv.tool.trakt.Action;
import cn.focion.tv.tool.util.Dates;
import cn.focion.tv.tool.util.Strings;

/**
 * 集列表适配器 Created by Pirate on 2015-05-09.
 */
public class EpisodesAdapter extends FocionAdapter {
    
    private ArrayList<Episode> episodes;
    
    private String timezone;
    
    public EpisodesAdapter(Context context,
                           String timezone,
                           int itemLayout,
                           ArrayList<Episode> episodes) {
        super(context, itemLayout, false);
        this.timezone = timezone;
        this.episodes = episodes;
    }
    
    @Override
    protected int getCount() {
        return episodes.size();
    }
    
    @Override
    protected void onBindHolder(ViewHolder holder, int position) {
        // 获取数据
        final Episode episode = episodes.get(position);
        // 设置值
        String title = episode.getTitle();
        if (Strings.isEmpty(title))
            title = "未知";
        holder.holder(R.id.episodes_name, TextView.class).setText(title);
        holder.holder(R.id.episodes_airtime, TextView.class)
              .setText(Dates.changeZone(timezone,
                                        episode.getFirst_aired(),
                                        Dates.WEEK_MM_DD_YYYY));
        holder.holder(R.id.episodes_number, TextView.class)
              .setText(String.valueOf(episode.getNumber()));
        boolean isWatch = episode.getIsWatch();
        int resId;
        if (isWatch)
            resId = R.mipmap.ic_action_tick_red;
        else
            resId = R.mipmap.ic_action_tick;
        // 设置图片
        holder.holder(R.id.episodes_watched, ImageView.class)
              .setImageResource(resId);
        holder.holder(R.id.episodes_watched)
              .setOnClickListener(new ClickListener(position));
    }
    
    @Override
    public ViewHolder newHolder(View itemView) {
        ViewHolder holder = new ViewHolder(itemView);
        holder.holder(R.id.episodes_number);
        holder.holder(R.id.episodes_name);
        holder.holder(R.id.episodes_airtime);
        holder.holder(R.id.episodes_watched);
        return holder;
    }
    
    class ClickListener implements View.OnClickListener {
        
        private int position;
        
        public ClickListener(int position) {
            this.position = position;
        }
        
        @Override
        public void onClick(View v) {
            if (v.getId() != R.id.episodes_watched)
                return;
            // 标记操作
            Episode episode = episodes.get(position);
            boolean isWatch = episode.getIsWatch();
            // 标记相反
            episode.setIsWatch(!isWatch);
            // 更新列表
            notifyItemChanged(position);
            // 更新数据库
            mFocion.traktDB.updateEpisode(episode);
            Intent intent = new Intent();
            // 传改后的值
            intent.putExtra("isWatch", !isWatch);
            intent.setAction(Action.ACTION_EPISODES_TO_SEASONS);
            mContext.sendBroadcast(intent);
        }
    }
}
