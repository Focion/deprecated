package cn.focion.tv.adapter.follow;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;

import cn.focion.tv.R;
import cn.focion.tv.adapter.FocionAdapter;
import cn.focion.tv.temp.TShow;
import cn.focion.tv.tool.util.Images;

/**
 * 搜索适配器 Created by Pirate on 2015-05-18.
 */
public class SearchAdapter extends FocionAdapter {
    
    private ArrayList<TShow> datas;

    /**
     * 父类构造
     *
     * @param context
     *            上下文
     * @param itemLayout
     *            布局
     */
    public SearchAdapter(Context context, int itemLayout, ArrayList<TShow> datas) {
        super(context, itemLayout, true);
        this.datas = datas;
    }
    
    @Override
    protected int getCount() {
        return datas.size();
    }
    
    @Override
    protected void onBindHolder(ViewHolder holder, int position) {
        final TShow data = datas.get(position);
        Images.load(data.poster_thumb,
                    holder.holder(R.id.item_show_img, SimpleDraweeView.class));
        holder.holder(R.id.item_show_name, TextView.class).setText(data.title);
        // 获取年份
        int year = data.year;
        String yearStr = "未知";
        if (year != 0)
            yearStr = String.valueOf(year).concat(" 年");
        holder.holder(R.id.item_show_time, TextView.class)
              .setText(String.format("首播：%s", yearStr));
    }
    
    @Override
    public ViewHolder newHolder(View itemView) {
        ViewHolder holder = new ViewHolder(itemView);
        holder.holder(R.id.item_show_img);
        holder.holder(R.id.item_show_name);
        holder.holder(R.id.item_show_time);
        return holder;
    }
}
