package cn.focion.tv.adapter.main;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.TextView;

import cn.focion.tv.R;
import cn.focion.tv.adapter.FocionAdapter;
import cn.focion.tv.model.Season;
import cn.focion.tv.tool.util.Cores;
import cn.focion.tv.tool.util.Images;
import cn.focion.tv.tool.util.Strings;
import cn.focion.tv.tool.util.UIs;
import cn.focion.tv.widget.HorProgress;

import com.facebook.drawee.view.SimpleDraweeView;

/**
 * Fragment适配器 Created by Pirate on 2015/3/30.
 */
public class SeasonAdapter extends FocionAdapter {
    
    private ArrayList<Season> seasons;
    
    private PopupWindow mPopupWindow;
    
    private TextView mWatched;
    
    private TextView mUnWatched;
    
    public SeasonAdapter(Context context,
                         int itemLayout,
                         ArrayList<Season> seasons) {
        super(context, itemLayout, false);
        this.seasons = seasons;
        // 初始化Pop
        initSlidingPop();
    }
    
    @Override
    protected int getCount() {
        return seasons.size();
    }
    
    @Override
    protected void onBindHolder(ViewHolder holder, int position) {
        final Season season = seasons.get(position);
        HorProgress progress = holder.holder(R.id.item_season_pro,
                                             HorProgress.class);
        // 辨别特别季没有集的情况
        if (Cores.checkSpecial(season)) {
            progress.setMax(1f);
            progress.setProgress(1f);
        }
        else {
            // 设置进度
            progress.setMax(season.getEpisode_count());
            progress.setProgress(season.getWatched_episode());
        }
        // 设置名字
        holder.holder(R.id.item_season_name, TextView.class)
              .setText(Strings.title(season));
        holder.holder(R.id.item_season_watch, TextView.class)
              .setText(String.valueOf(season.getWatched_episode()));
        holder.holder(R.id.item_season_total, TextView.class)
              .setText(String.valueOf(season.getEpisode_count()));
        // 图片
        Images.load(season.getPoster(),
                    holder.holder(R.id.item_season_img, SimpleDraweeView.class));
        // 点击事件
        holder.holder(R.id.item_season_more)
              .setOnClickListener(new ClickListener(position));
    }
    
    @Override
    public ViewHolder newHolder(View itemView) {
        ViewHolder holder = new ViewHolder(itemView);
        holder.holder(R.id.item_season_img);
        holder.holder(R.id.item_season_name);
        holder.holder(R.id.item_season_more);
        holder.holder(R.id.item_season_pro);
        holder.holder(R.id.item_season_watch);
        holder.holder(R.id.item_season_total);
        return holder;
    }
    
    /**
     * 初始化PopWindow
     */
    private void initSlidingPop() {
        // 获取弹出框布局
        View popView = View.inflate(mContext,
                                    R.layout.layout_season_popup,
                                    null);
        UIs.measureView(popView);
        // 布局参数
        mPopupWindow = new PopupWindow(popView,
                                       UIs.dipToPx(mContext, 96),
                                       popView.getMeasuredHeight());
        // 背景色
        mPopupWindow.setBackgroundDrawable(new ColorDrawable(0));
        // 设置监听
        mWatched = (TextView) popView.findViewById(R.id.season_wacthed);
        mUnWatched = (TextView) popView.findViewById(R.id.season_unwatched);
    }
    
    class ClickListener implements View.OnClickListener {
        
        private int position;
        
        public ClickListener(int position) {
            this.position = position;
        }
        
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.item_season_more) {
                // 设置弹出位置
                mPopupWindow.showAsDropDown(v, 0, -UIs.dipToPx(mContext, 36));
                // 获取popwindow焦点
                mPopupWindow.setFocusable(true);
                // 设置popwindow如果点击外面区域，便关闭。
                mPopupWindow.setOutsideTouchable(true);
                // 弹出框更新
                mPopupWindow.update();
                // 设置监听
                ClickListener clickListener = new ClickListener(position);
                mWatched.setOnClickListener(clickListener);
                mUnWatched.setOnClickListener(clickListener);
                return;
            }
            // 获取当前点击的Season
            Season season = seasons.get(position);
            // 处理特别季无集情况
            if (Cores.checkSpecial(season)) {
                Tips("此季无剧集可以标记");
                // 关闭
                mPopupWindow.dismiss();
                return;
            }
            // 标记
            boolean flag = false;
            switch (v.getId()) {
                case R.id.season_wacthed:
                    flag = true;
                    // 设置已看季
                    season.setWatched_episode(season.getEpisode_count());
                    break;
                case R.id.season_unwatched:
                    flag = false;
                    // 设置已看季
                    season.setWatched_episode(0);
                    break;
            }
            // 更新当前Item
            notifyItemChanged(position);
            // 标记
            season.setIsWatch(flag);
            // 更新当前季的数据库
            mFocion.traktDB.updateSeason(season);
            // 标记所有集已看
            mFocion.traktDB.updateEpisodes(flag, season);
            // 关闭
            mPopupWindow.dismiss();
        }
    }
}
