package cn.focion.tv.adapter.main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import java.util.ArrayList;

import cn.focion.tv.ui.FocionFragment;

/**
 * 主页Detail的适配器 Created by Pirate on 2015-05-05.
 */
public class MainAdapter extends FragmentStatePagerAdapter {
    
    private ArrayList<FocionFragment> fragments;
    
    private String[] tabs;
    
    public MainAdapter(FragmentManager fm, ArrayList<FocionFragment> fragments) {
        super(fm);
        this.fragments = fragments;
        this.tabs = new String[] { "剧详情", "所有季" };
    }
    
    @Override
    public int getCount() {
        return fragments.size();
    }

    /**
     * 设置每一个Item
     *
     * @param position
     *            序号
     * @return 子对象
     */
    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }
    
    @Override
    public CharSequence getPageTitle(int position) {
        return tabs[position];
    }
}
