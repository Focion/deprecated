package cn.focion.tv.adapter.schedule;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.view.ViewGroup;

import cn.focion.tv.tool.util.Dates;
import cn.focion.tv.ui.schedule.ScheduleFragment;

/**
 * 我的时间表 Created by Pirate on 2015-05-21.
 */
public class ScheduleAdapter extends FragmentStatePagerAdapter {
    
    private long startTime;
    
    public ScheduleAdapter(FragmentManager fm) {
        super(fm);
        this.startTime = Dates.currentTime(Dates.YMD) - 6 * 86400000;
    }
    
    @Override
    public int getCount() {
        return 15;
    }
    
    /**
     * 设置每一个Item
     *
     * @param position
     *            序号
     * @return 子对象
     */
    @Override
    public Fragment getItem(int position) {
        return new ScheduleFragment();
    }
    
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ScheduleFragment fragment = (ScheduleFragment) super.instantiateItem(container,
                                                                             position);
        // fragment.setStartTime(startTime + (position - 1) * 86400000);
        return fragment;
    }
    
    @Override
    public int getItemPosition(Object object) {
        return PagerAdapter.POSITION_NONE;
    }
    
    @Override
    public CharSequence getPageTitle(int position) {
        return Dates.dateFormat(startTime + (position - 1) * 86400000,
                                Dates.MD_CN);
    }
    
    public void notifyDataSetChanged(long startTime) {
        this.startTime = startTime - 6 * 86400000;
        notifyDataSetChanged();
    }
}
