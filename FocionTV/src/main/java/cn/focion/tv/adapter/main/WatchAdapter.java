package cn.focion.tv.adapter.main;

import java.util.ArrayList;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import cn.focion.tv.R;
import cn.focion.tv.adapter.FocionAdapter;
import cn.focion.tv.model.Episode;
import cn.focion.tv.model.Season;
import cn.focion.tv.model.Show;
import cn.focion.tv.tool.trakt.TraktDB;
import cn.focion.tv.tool.util.Dates;
import cn.focion.tv.tool.util.Images;
import cn.focion.tv.tool.util.Strings;
import cn.focion.tv.ui.episode.EpisodeActivity;
import cn.focion.tv.ui.main.DetailsActivity;
import cn.focion.tv.ui.main.WatchFragment;

import com.facebook.drawee.view.SimpleDraweeView;

/**
 * 主页适配器 Created by Pirate on 2015/4/21.
 */
public class WatchAdapter extends FocionAdapter {
    
    // 显示的剧
    private ArrayList<Show> shows;
    
    // 显示的季
    private ArrayList<Season> seasons;
    
    // 显示的集
    private ArrayList<Episode> episodes;
    
    private WatchFragment watchFragment;
    
    public WatchAdapter(WatchFragment watchFragment,
                        int itemLayout,
                        ArrayList<Show> shows,
                        ArrayList<Season> seasons,
                        ArrayList<Episode> episodes) {
        super(watchFragment.mActivity, itemLayout);
        this.watchFragment = watchFragment;
        this.shows = shows;
        this.seasons = seasons;
        this.episodes = episodes;
    }
    
    @Override
    protected int getCount() {
        return shows.size();
    }
    
    @Override
    protected void onBindHolder(ViewHolder holder, int position) {
        // 获取要绑定的数据
        final Show show = shows.get(position);
        final Season season = seasons.get(position);
        final Episode episode = episodes.get(position);
        // 设置值
        SimpleDraweeView draweeView = holder.holder(R.id.item_show_img,
                                                    SimpleDraweeView.class);
        Images.load(show.getFanart(), draweeView);
        // 剧名
        holder.holder(R.id.item_show_name, TextView.class)
              .setText(Strings.title(show));
        
        // if (episode == null) {
        // // 无集
        // TextView watch = holder.holder(R.id.item_show_watch, TextView.class);
        // watch.setVisibility(View.VISIBLE);
        // // 隐藏加载框
        // holder.holder(R.id.item_progress).setVisibility(View.GONE);
        // // 判断此剧的状态
        // String status = show.getStatus();
        // if (status.contains("ended")) {
        //
        // }
        // else {
        // // 设置不用的按钮
        // watch.setText("点击更新");
        // // 设置更新时间
        // holder.holder(R.id.item_watch_series_episode, TextView.class)
        // .setText("更新时间：");
        // holder.holder(R.id.item_first_air, TextView.class)
        // .setText("未知");
        // holder.holder(R.id.item_episode_name, TextView.class)
        // .setText(Strings.status(status));
        // }
        // return;
        // }
        if (episode != null) { // 当此季有集时
            // 星期? ?月?日 ?年
            holder.holder(R.id.item_first_air, TextView.class)
                  .setText(Dates.changeZone(show.getTimezone(),
                                            episode.getFirst_aired(),
                                            Dates.WEEK_MM_DD_YYYY));
            // 设置单集的标题
            String episodeTitle = episode.getTitle();
            if (Strings.isEmpty(episodeTitle))
                episodeTitle = "未知";
            holder.holder(R.id.item_episode_name, TextView.class)
                  .setText(episodeTitle);
            // S? E?
            holder.holder(R.id.item_watch_series_episode, TextView.class)
                  .setText(Strings.dealDisplay(episode.getSeason(),
                                               episode.getNumber()));
            // 判断是否看过
            if (episode.getIsWatch()) {
                holder.holder(R.id.item_show_watch).setVisibility(View.GONE);
                holder.holder(R.id.item_progress).setVisibility(View.INVISIBLE);
            }
            else {
                holder.holder(R.id.item_show_watch).setVisibility(View.VISIBLE);
                holder.holder(R.id.item_progress).setVisibility(View.INVISIBLE);
            }
        }
        else if (season.getNumber() == 0) { // 无特别季中的集
            holder.holder(R.id.item_episode_name, TextView.class)
                  .setText("特别季无内容，点击标记已看");
            holder.holder(R.id.item_watch_series_episode, TextView.class)
                  .setText("S00 E00");
        }
        // 设置点击监听
        ClickListener positionListener = new ClickListener(position);
        holder.holder(R.id.item_show_watch)
              .setOnClickListener(positionListener);
        holder.holder(R.id.item_episode_layout)
              .setOnClickListener(positionListener);
        draweeView.setOnClickListener(positionListener);
    }
    
    @Override
    public ViewHolder newHolder(View itemView) {
        ViewHolder holder = new ViewHolder(itemView);
        holder.holder(R.id.item_show_img);
        holder.holder(R.id.item_show_name);
        holder.holder(R.id.item_watch_series_episode);
        holder.holder(R.id.item_first_air);
        holder.holder(R.id.item_episode_name);
        holder.holder(R.id.item_show_watch);
        holder.holder(R.id.item_episode_layout);
        holder.holder(R.id.item_progress);
        return holder;
    }
    
    /**
     * 带角标的点击监听
     */
    class ClickListener implements View.OnClickListener {
        
        private int watchPosition;
        
        /**
         * @param watchPosition
         *            当前点击的角标
         */
        public ClickListener(int watchPosition) {
            this.watchPosition = watchPosition;
        }
        
        @Override
        public void onClick(View v) {
            // 设置点击的角标
            watchFragment.setClickPosition(watchPosition);
            Intent intent = new Intent();
            // 获取当前点击的Show,Season,Episode
            final Show show = shows.get(watchPosition);
            final Season season = seasons.get(watchPosition);
            final Episode episode = episodes.get(watchPosition);
            // 设置时间ZONE
            switch (v.getId()) {
                case R.id.item_show_img:
                    // 点击图片跳转Season简介
                    intent.setClass(mContext, DetailsActivity.class);
                    intent.putExtra("Show", show);
                    mContext.startActivity(intent);
                    break;
                case R.id.item_episode_layout:
                    // 无集的情况
                    if (episode == null) {
                        Tips("此集无详情");
                        return;
                    }
                    // 点击图片跳转Episode简介
                    intent.setClass(mContext, EpisodeActivity.class);
                    // 设置ShowId，时区，季，集
                    intent.putExtra("show_id", show.getTrakt());
                    intent.putExtra("timezone", show.getTimezone());
                    intent.putExtra("Season", season);
                    intent.putExtra("Episode", episode);
                    mContext.startActivity(intent);
                    break;
                case R.id.item_show_watch:
                    if (episode != null) {
                        // 点击已看
                        episode.setIsWatch(true);
                        notifyItemChanged(watchPosition);
                    }
                    // 获取下一季或者集
                    mFocion.traktDB.queryNext(season, episode);
                    break;
            
            }
        }
    }
}
