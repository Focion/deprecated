package cn.focion.tv.adapter.follow;

import java.util.ArrayList;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import cn.focion.tv.ui.FocionFragment;

/**
 * Fragment适配器 Created by Pirate on 2015/3/30.
 */
public class FollowAdapter extends FragmentStatePagerAdapter {
    
    private ArrayList<FocionFragment> fragments;
    
    private String[] tabs;
    
    public FollowAdapter(FragmentManager fm, ArrayList<FocionFragment> fragments) {
        super(fm);
        this.fragments = fragments;
        this.tabs = new String[] { "流行", "趋势", "分类" };
    }
    
    @Override
    public int getCount() {
        return fragments.size();
    }
    
    /**
     * 设置每一个Item
     *
     * @param position
     *            序号
     * @return 子对象
     */
    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }
    
    @Override
    public CharSequence getPageTitle(int position) {
        return tabs[position];
    }
}
