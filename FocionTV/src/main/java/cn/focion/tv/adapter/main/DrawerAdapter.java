package cn.focion.tv.adapter.main;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.TextView;

import cn.focion.tv.Constants;
import cn.focion.tv.R;
import cn.focion.tv.adapter.FocionAdapter;

/**
 * 主页Drawer适配器 Created by Pirate on 2015/3/28.
 */
public class DrawerAdapter extends FocionAdapter {
    
    // 功能集合
    private int[] functions;
    
    // 功能图片
    private int[] fuctionImages;
    
    private Resources resources;
    
    /**
     * 父类构造
     *
     * @param context
     *            上下文
     * @param itemLayout
     *            布局
     */
    public DrawerAdapter(Context context, int itemLayout) {
        super(context, itemLayout);
        functions = Constants.FUCTION_LIST;
        fuctionImages = Constants.FUCTION_IMG;
        resources = context.getResources();
    }
    
    @Override
    public ViewHolder newHolder(View itemView) {
        ViewHolder holder = new ViewHolder(itemView);
        holder.holder(R.id.drawer_item_category);
        return holder;
    }
    
    @Override
    protected int getCount() {
        return functions.length;
    }
    
    @Override
    protected void onBindHolder(ViewHolder holder, int position) {
        TextView functionTv = holder.holder(R.id.drawer_item_category,
                                            TextView.class);
        functionTv.setText(functions[position]);
        Drawable drawable = resources.getDrawable(fuctionImages[position]);
        if (drawable != null) {
            drawable.setBounds(0,
                               0,
                               drawable.getMinimumWidth(),
                               drawable.getMinimumHeight());
            functionTv.setCompoundDrawables(drawable, null, null, null);
        }
    }
}
