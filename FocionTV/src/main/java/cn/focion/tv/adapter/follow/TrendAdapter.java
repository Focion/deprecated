package cn.focion.tv.adapter.follow;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import cn.focion.tv.R;
import cn.focion.tv.adapter.FocionAdapter;
import cn.focion.tv.temp.TShow;
import cn.focion.tv.tool.util.Images;
import cn.focion.tv.ui.follow.DetailActivity;

import com.facebook.drawee.view.SimpleDraweeView;

/**
 * 排行适配器 Created by Pirate on 2015/3/30.
 */
public class TrendAdapter extends FocionAdapter {
    
    private ArrayList<TShow> datas;
    
    /**
     * 父类构造
     *
     * @param context
     *            上下文
     * @param itemLayout
     *            布局
     */
    public TrendAdapter(Context context, int itemLayout, ArrayList<TShow> datas) {
        super(context, itemLayout, true);
        this.datas = datas;
    }
    
    @Override
    public ViewHolder newHolder(View itemView) {
        ViewHolder holder = new ViewHolder(itemView);
        holder.holder(R.id.item_show_img);
        holder.holder(R.id.item_show_name);
        holder.holder(R.id.item_show_time);
        holder.holder(R.id.item_show_watchers);
        return holder;
    }
    
    @Override
    protected void onBindHolder(ViewHolder holder, int position) {
        final TShow data = datas.get(position);
        Images.load(data.poster_thumb,
                    holder.holder(R.id.item_show_img, SimpleDraweeView.class));
        holder.holder(R.id.item_show_name, TextView.class).setText(data.title);
        // 获取年份
        int year = data.year;
        String yearStr = "未知";
        if (year != 0)
            yearStr = String.valueOf(year);
        holder.holder(R.id.item_show_time, TextView.class)
              .setText(String.format("首播：%s 年", yearStr));
        holder.holder(R.id.item_show_watchers, TextView.class)
              .setText(String.format("今日浏览：%,d 次", data.watchers));
    }
    
    @Override
    protected int getCount() {
        return datas.size();
    }
}
