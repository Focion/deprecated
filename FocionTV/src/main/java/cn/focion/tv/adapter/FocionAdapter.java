package cn.focion.tv.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import cn.focion.tv.Focion;
import cn.focion.tv.R;
import cn.focion.tv.model.Episode;
import cn.focion.tv.model.Season;
import cn.focion.tv.model.Show;

/**
 * 列表适配器 Created by Pirate on 2015/3/28.
 */
public abstract class FocionAdapter extends
                                   RecyclerView.Adapter<FocionAdapter.ViewHolder> {
    
    // 上下文
    protected Context mContext;
    
    // 布局文件
    protected int mItemLayout;
    
    // 全局变量
    protected Focion mFocion;
    
    // 是否加载更多
    protected boolean isLoad;
    
    // Item
    private final int TYPE_ITEM = 0;
    
    // 最底部
    private final int TYPE_FOOTER = 1;
    
    private cn.focion.tv.widget.RecyclerView.ItemClickListener mItemClickListener;
    
    /**
     * 父类构造
     *
     * @param context
     *            上下文
     * @param itemLayout
     *            布局文件
     */
    public FocionAdapter(Context context, int itemLayout) {
        this(context, itemLayout, false);
    }
    
    /**
     * 父类构造
     *
     * @param context
     *            上下文
     * @param itemLayout
     *            布局文件
     * @param isLoad
     *            是否加载更多
     */
    public FocionAdapter(Context context, int itemLayout, boolean isLoad) {
        mContext = context;
        mItemLayout = itemLayout;
        mFocion = (Focion) context.getApplicationContext();
        this.isLoad = isLoad;
    }
    
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_ITEM:
                return newHolder(View.inflate(parent.getContext(),
                                              mItemLayout,
                                              null));
            case TYPE_FOOTER:
                View view = View.inflate(parent.getContext(),
                                         R.layout.focion_load_more,
                                         null);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                                                             ViewGroup.LayoutParams.WRAP_CONTENT);
                view.setLayoutParams(lp);
                return newHolder(view);
            default:
                return newHolder(View.inflate(parent.getContext(),
                                              mItemLayout,
                                              null));
        }
    }
    
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // 判断Item
        if (position < getItemCount() - 1 || (!isLoad && position == getItemCount() - 1))
            onBindHolder(holder, position);
    }
    
    /**
     * 获取Item数量
     *
     * @return 返回数量包括底部加载
     */
    @Override
    public int getItemCount() {
        if (isLoad)
            return getCount() + 1;
        else
            return getCount();
    }
    
    /**
     * 真实的数据数量
     *
     * @return 数量个数
     */
    protected abstract int getCount();
    
    /**
     * 绑定Holder数据
     *
     * @param holder
     *            容器
     * @param position
     *            角标
     */
    protected abstract void onBindHolder(ViewHolder holder, int position);
    
    /**
     * 返回Item属性
     *
     * @param position
     *            角标
     * @return 属性
     */
    @Override
    public int getItemViewType(int position) {
        if (isLoad && position + 1 == getItemCount())
            return TYPE_FOOTER;
        else
            return TYPE_ITEM;
    }
    
    /**
     * 返回当前加载状态
     *
     * @return true 加载 false 不加载
     */
    public boolean isLoad() {
        return isLoad;
    }
    
    /**
     * 停止加载更多
     */
    public void loadEnable() {
        this.isLoad = false;
        notifyItemRemoved(getItemCount() + 1);
    }
    
    /**
     * 获取ViewHolder
     *
     * @param itemView
     *            布局View
     * @return Holder
     */
    public abstract ViewHolder newHolder(View itemView);
    
    /**
     * ViewHolder做控件缓存
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        
        // 布局View
        private View itemView;
        
        /**
         * 初始化ViewHolder
         *
         * @param itemView
         *            视图
         */
        public ViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            itemView.setBackgroundResource(R.drawable.bg_selector_clr_trans_to_bg);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mItemClickListener.onRecyclerItemClick(v,
                                                           getAdapterPosition());
                }
            });
        }
        
        /**
         * 静态Holder容器
         *
         * @param id
         *            资源Id
         * @param <T>
         *            泛型
         * @return 静态View
         */
        @SuppressWarnings("unchecked")
        public <T extends View> T holder(int id) {
            return holder(id, null);
        }
        
        /**
         * 静态Holder容器
         *
         * @param id
         *            资源Id
         * @param <T>
         *            泛型
         * @param clazz
         *            传入强转类型
         * @return 静态View
         */
        @SuppressWarnings({ "unchecked", "UnusedParameters" })
        public <T extends View> T holder(int id, Class<T> clazz) {
            // 创建容器
            SparseArray<View> holder = (SparseArray<View>) itemView.getTag();
            // 如果容器为空
            if (holder == null) {
                holder = new SparseArray<>();
                itemView.setTag(holder);
            }
            
            // 容器不为空
            View child = holder.get(id);
            if (child == null) {
                // 获取布局控件
                child = itemView.findViewById(id);
                // 放入容器
                holder.put(id, child);
            }
            // 返回控件
            return (T) child;
        }
    }
    
    /**
     * 设置Item点击监听
     * 
     * @param itemClickListener
     *            item点击监听
     */
    public void setItemClickListener(cn.focion.tv.widget.RecyclerView.ItemClickListener itemClickListener) {
        this.mItemClickListener = itemClickListener;
    }
    
    /**
     * 显示消息
     * 
     * @param msg
     *            消息内容
     */
    public void Tips(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }
}
